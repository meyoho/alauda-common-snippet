# Alauda Common Library

## [Document](./projects/alauda-common-lib)

## 现有功能

### 可复用基础设施

- `ConstantsModule` 放置所有模块需要的 InjectionToken
- `ApiGatewayModule` API Gateway 相关（暂无）
- `AuthorizationModule` 提供基于 Alauda Console 的标准认证支持
- `UtilsModule` 可复用的工具类（函数），Angular Pipe, Service
- `CommonLayoutModule` 可复用的全局 Layout 相关组件及服务
- `PermissionModule` 基于标准 k8s API 的权限相关支持
- `Types` 可复用的 typings

### 可复用业务组件

- ImagePull/Push
- PipelineDiagram

## 什么可以加进来

- 所有产品可能都会用到的一致性基础设施
  - ConstantsModule
  - ApiGatewayModule
  - AuthorizationModule
  - CommonLayoutModule
  - PermissionModule
  - 待加入
- 可复用的工具(不再增加新的 Module)
  - UtilsModule
  - Types
- 可复用的业务模块
  - ImagePull/Push
  - PipelineDiagram
  - 待加入

## 原则和风格

### 唯一正确实现

针对同一问题，只提供一种正确的实现或使用方式，如果无法判断最优实现，需要最终确定一种实现，并接受可能的缺陷

### 和现有标准 API 做强绑定（非强绑定，需要项目自行提供上下文）

前端接触的 API 为两种：

- Alauda Console
- API Gateway

Alauda Console 的 API 较少，且不依赖 API_GATEWAY，库中可做强绑定，API Gateway 的 API 是否可以直接在库中使用，还是需要项目提供上下文，待讨论

### 为 RX 的使用做优化

既然大家都喜欢用，库尽量以 Observable interface 为主，但是需要后续推进最佳实践

### 文件结构及命名

- 基础设施按功能职责单独以顶级文件夹加入 lib 目录，需要作为独立的 Module 存在
- 工具相关，加入 UtilsModule
- 类型相关，加入 Types，原则上有业务含义的类型，应包含在相关的业务模块，不应加入 Types
- 可复用业务模块需要明确命名并加入 lib\business 目录，需要作为独立的 Module 存在
- 库不包含内置的静态资源，建议提供接口由项目来提供

## 版本

### 版本类型

- `正式版本` 数字版本: major.minor.patch
- `Feature测试版` 数字版本-[Feature Name]
- `Beta版本` 数字版本-beta[Sequence number]

### 发版规范

- 正常修复 bug，仅修改 patch
- 如需真实项目环境验证，发布 beta 版
- 新增 feature 需修改 minor
- 如有结构性调整，需修改 major

## 新功能加入流程

1. 提出功能加入申请（创建 [RFC](./rfcs/README.md) PR），需包含功能描述和详细设计等
2. 一起讨论功能是否值得入库，如无争议，合并 RFC PR，否则撤销或重新修改 RFC
3. 小范围讨论确定最终实现方式
4. 开发 feature 分支提供 feature 测试版供实际产品验证（仅限一个产品）
5. 修改 RFC 状态，pull request and review
6. 版本发布

## 本地开发调试的方法

第一第二种方式比较适合在本项目中开发新功能时使用
第三种比较适合对 common 进行 debug，无需造数据模拟 bug 场景，可直接在项目中调试

### 一. 本地运行 storybook 调用 common

1. 执行命令：yarn dev, 本地运行 storybook
2. 在 stories 目录下开发 demo，调用 common 代码进行调试

### 二. 运行本地 app 调用 common

1. 执行命令：yarn debug, 运行本项目的 app 下代码
2. 在 app 目录下开发 demo，调用 common 代码进行调试

### 三. 本地 build common 替换目标项目 node_modules 中的 common，后在目标项目中调试 common

1. 在 packages/alauda-common-lib 下创建 ng-package.debug.json, 例如：

   ```
   {
       "$schema": "../../node_modules/ng-packagr/ng-package.schema.json",
       "dest": "/Users/li.youzhi/xxx/underlord/node_modules/@alauda/common-snippet",
       "lib": {
           "entryFile": "src/public-api.ts",
           "umdModuleIds": {
           "lodash-es": "_",
           "jwt-decode": "jwt_decode",
           "@alauda/ui": "alauda.ui"
           }
       },
       "assets": ["assets"]
   }

   ```

   可复制本例子，本例子中的 ‘dest’ 内自行配置本地目标项目路径

2. 执行命令：yarn build:watch，待运行完成、目标项目的 node_modules 里的 common 被替换为本地 common 版本，后可在目标项目进行测试
