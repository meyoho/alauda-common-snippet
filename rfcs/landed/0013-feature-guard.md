- 开始时间: 2020-05-20
- 目标主版本: 2.3
- 相关 issues: https://bitbucket.org/mathildetech/alauda-common-snippet/issues/24/-------
- 实现 PR: (留空)

# 概要

功能开关未启用，直接输入 url 进入，重定向到空页面，提示未启用

# 基本实例

```ts
// 在功能开关模块路由添加guard,data 中传入featureName 功能开关名称。
// 默认跳转路径为feature-guard-error,如需自定义跳转地址可传入guard_url: 'xxxxx'
// 为解决在 underlord 不同平台展示对应名称，添加 `logTitle` 参数，对应路由 `feature-guard-error/:name/:logTitle`
import {FeatureGuard } from '@alauda/common-snippet';
// ...
  RouterModule.forRoot([
    // ...
    {
      path: 'home',
      canActivate: [ FeatureGuard ],
      data: { featureName: 'ace3' },
      loadChildren: () =>
        import('./home/home.module').then(M => M.HomeModule),
    },
    {
      path: 'feature-guard-error/:name',
      component: FeatureGuardErrorComponent,
    },
  ]),
],
// ...
```

# 动机

在某些模块功能开关未启用，当用户收藏了地址，或者直接输入 url 进入，为了限制未开启功能不可见，重定向到空白页，提示未启用

# 详细设计

````typescript
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { FeatureGateService } from './feature-gate.service';
const DEFAULT_ERROR_GUARD_URL = 'feature-guard-error';

/**
 * ```ts
 * data: {
 *  path: 'home',
 *  canActivate: [ FeatureGuard ],
 *  data: { featureName: 'ace3', guard_url: 'xxx', logTitle: 'TKE PaaS'  }
 * }
 * ```
 */
@Injectable({ providedIn: 'root' })
export class FeatureGuard implements CanActivate, CanActivateChild {
  constructor(
    private readonly featureGate: FeatureGateService,
    private readonly router: Router,
  ) {}

  canActivateChild(childRoute: ActivatedRouteSnapshot): Observable<boolean> {
    return this.canActivate(childRoute);
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const data = {
      featureName: '',
      guard_url: DEFAULT_ERROR_GUARD_URL,
      logTitle: '',
      ...route.data,
    };
    return this.featureGate.isEnabled(data.featureName).pipe(
      tap(enabled => {
        if (enabled) return;
        const logTitle = data.logTitle ? `/${data.logTitle}` : '';
        this.router.navigateByUrl(
          `${data.guard_url}/${data.featureName}${logTitle}`,
          { replaceUrl: true },
        );
      }),
    );
  }
}
````

# 缺点

NaN

# 替代方案

NaN

# 实施策略

各产品需要自己添加一个 `feature-guard-error` 组件，内部直接使用 `acl-page-guard` 展示空白页提示(路由需要添加参数用来展示功能开关名称 例：`path: 'feature-guard-error/:name'` )，并在路由中添加守卫在 `data` 中设置 `featureName`, 也可自定义空白页地址 `guard_url`，`logTitle` 参数为空白页 header 中展示平台名称(除了 Underlord 其他平台应该无需此参数)

# 未解决的问题

NaN
