- 开始时间: 2019-09-16
- 目标主版本: 1.4.0
- 相关 issues: #3
- 实现 PR:

# 概要

k8s 资源辅助工具 Service 和 Pipe，统一处理 k8s 资源格式化和获取属性值相关功能。

# 基本实例

```html
<!-- input -->
{{ namespace | aclLabel:'type':'prefix' }}

<!-- transformed -->
{{ namespace.metadata.labels['prefix.alauda.io/type'] }}
```

# 动机

减少样板代码，省去空值判断逻辑，降低 BASE_DOMAIN 相关部分出错的可能。

# 详细设计

基础：K8sUtilService

```ts
class K8sUtilService {
  // 基于外部提供的 TOKEN_BASE_DOMAIN 处理 Annotations 和 Labels
  constructor(@Inject(TOKEN_BASE_DOMAIN) private baseDomain: string) {}

  // reference: http://confluence.alauda.cn/pages/viewpage.action?pageId=27166946
  normalizeType(type: string, prefix?: string) {
    return `${ifExist(prefix, prefix + '.')}${this.baseDomain}/${type}`;
  }

  getAnnotation<T extends KubernetesResource>(
    resource: T,
    type: string,
    prefix?: string,
  ) {
    return get(resource, [
      METADATA,
      ANNOTATIONS,
      this.normalizeType(type, prefix),
    ]);
  }
}
```

基于 K8sUtilService 实现 K8sUtilPipe 减少模板里的样板代码

```ts
@Pipe({ name: 'aluAnnotation' })
export class K8sAnnotationPipe implements PipeTransform {
  constructor(private k8sUtil: K8sUtilService) {}

  transform(
    resource: KubernetesResource,
    type: string,
    prefix?: string,
  ): string {
    return this.k8sUtil.getAnnotation(resource, type, prefix);
  }
}
```

# 缺点

NaN

# 替代方案

NaN

# 实施策略

已有实现的项目 (icarus/underlord) 全局进行替换，其他项目按各自实际需要引入使用。

# 未解决的问题

NaN
