- 开始时间: 2019-09-11
- 目标主版本: 1.3.6
- 相关 issues: https://bitbucket.org/mathildetech/alauda-common-snippet/issues/9/button-disbaled-tooltip
- 实现 PR: (留空)

# 概要

解决 button 在 disabled 时 tooltip 无法生效的问题。

# 基本实例

```html
<!-- template.html -->
<acl-disabled-container [isAllowed]="creatable$ | async">
  <button aui-button="primary" (click)="create()">
    {{ 'create' | translate }}
  </button>
</acl-disabled-container>
```

```html
<!-- template.html -->
<acl-disabled-container
  [isAllowed]="creatable$ | async"
  [isDisabled]="condition"
  [tooltip]="'custom-tooltip'"
>
  <button aui-button="primary" (click)="create()">
    {{ 'create' | translate }}
  </button>
</acl-disabled-container>
```

```html
<!-- template.html -->
<aui-menu #menu>
  <acl-disabled-container
    [isAllowed]="updated$ | async"
    tooltipPosition="start"
  >
    <aui-menu-item (click)="update()">
      {{ 'update' | translate }}
    </aui-menu-item>
  </acl-disabled-container>
  <acl-disabled-container
    [isAllowed]="deleted$ | async"
    tooltipPosition="start"
  >
    <aui-menu-item (click)="delete()">
      {{ 'delete' | translate }}
    </aui-menu-item>
  </acl-disabled-container>
</aui-menu>
```

# 动机

产品需求，当 button 因权限不足而 disabled 时，产品希望使用 tooltip 说明 disabled 原因。[平台权限前端业务处理规则](http://confluence.alauda.cn/pages/viewpage.action?pageId=50832776)
以此需求为基础，本组件主要服务于权限相关情况，同时支持传入自定义判断条件和自定义 tooltip 文本的场景。

# 详细设计

计划通过 ng-content 传入需要 disabled 的元素，组件内部抓取传入元素中的 button 并通过判断条件设置 disabled 属性

```html
<div
  class="acl-disabled-container"
  [auiTooltip]="tooltip || ('forbidden' | translate)"
  [auiTooltipPosition]="tooltipPosition"
  [auiTooltipDisabled]="isAllowed"
>
  <ng-content></ng-content>
</div>
```

支持通过 tooltip 传入自定义文本，通过 tooltipPosition 设置 tooltip 位置，参数与 aui 一致
若场景为权限和其他判断条件共同作用，可除 isAllowed 传入第二个判断参数，规则为权限与判断条件同时不满足，显示无权限的 tooltip

# 缺点

NaN

# 替代方案

NaN

# 实施策略

使用 acl-disabled-container 包在之前的 button 或 aui-menu-item 外层，将之前 \*ngIF 的判断条件 Copy 到 isAllowed 即可
目前只支持 button 和 aui-menu-item， 因为 aui-menu-item 内部也是 button， 可以为 button 设置 disabled 属性。

# 未解决的问题

只能对 button 和 menu-item 生效，dropdown 的适配有待完善
