- 开始时间: 2019-08-29
- 目标主版本: 1.3.0
- 相关 issues: https://bitbucket.org/mathildetech/alauda-common-snippet/issues/7
- 实现 PR: (留空)

# 概要

类似 `K8SResourceList` 的处理通用异步数据的方案，包含负责加载数据 `AsyncDataLoader` 以及 负责展示加载状态 `acl-data-state` 组件。

# 基本实例

```typescript
// component.ts
dataLoader = new AsyncDataLoader({
  params$: this.params$,
  fetcher: params => this.api.someMethod(params),
});
```

```html
<!-- template.html -->
<acl-page-state [adapter]="dataLoader">
  <ng-template let-resource>
    {{ resource | json }}
  </ng-template>
</acl-page-state>
```

# 动机

需要简单方便的方法来加载详情、更新等页面的数据，显示统一的权限、加载、异常等状态。

# 详细设计

通过定义接口的方式解耦数据加载和渲染逻辑，便于以后实现不同的加载或渲染需求。

定义 `DataStateAdapter` 接口，`PageState` 组件通过接口定义的数据展示页面加载状态，并将数据通过 `TemplateContext` 传给 content 中的 `ng-template`。`AsyncDataLoader` 通过实现 `DataStateAdapter` 接口与 `PageState` 组件连接。

```typescript
class AsyncDataLoader<D, P> implements DataStateAdapter<D> {
  snapshot: { data: D; params: P; loading: boolean; error: DataError };
  data$: Observable<D>;
  loading$: Observable<boolean>;
  error$: Observable<DataError>;

  constructor(config: DataLoaderConfig<D, P>) {}

  reload(data?: D) {}
  mapData(mapper: DataMapper<D>) {}
}

class PageStateComponent {
  @Input()
  adapter: DataStateAdapter;
}

interface DataStateAdapter<D extends object> {
  data$: Observable<D>;
  loading$: Observable<boolean>;
  error$: Observable<DataError>;
  reload: () => void;
}

interface DataLoaderConfig<D, P> extends Callbacks<D> {
  fetcher: (params: P) => Observable<D>;
  params$?: Observable<P>;
  params?: P;
}

interface Callbacks<D> {
  onStart?: () => void;
  onLoaded?: (data: D) => void;
  onError?: (err: DataError) => void;
}

type DataError = Partial<HttpErrorResponse & Status>;

type DataMapper<D> = (data: D) => D;
```

# 缺点

NAN

# 替代方案

NAN

# 实施策略

icarus 中已经有不少详情页在用这套方案，api 没有重大变化，替换掉相应组件即可。其他页面在做权限重构时替换。

# 未解决的问题

NAN
