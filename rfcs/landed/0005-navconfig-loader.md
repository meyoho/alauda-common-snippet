- 开始时间: 2019-08-29
- 目标主版本: 1.3.0
- 相关 issues: https://bitbucket.org/mathildetech/alauda-common-snippet/issues/8
- 实现 PR: (留空)

# 概要

定义统一的导航栏配置格式，并提供一整套加载、缓存逻辑，操作树型数据结构的函数，将配置转换 AUI 导航组件配置的方法。

# 基本实例

```typescript
this.navConfig$ = this.navLoader
  .loadNavConfig(custom / navconfig - admin.yaml)
  .pipe(
    this.navLoader.parseYaml(),
    map(config => filterTree(filterFn, config)),
    this.navLoader.mapToAuiNav(),
    publishReplay(1),
    refCount(),
  );
```

# 动机

客户需要[自定义左导航的功能](http://jira.alauda.cn/browse/ACP-502)，为保证各平台实现方案统一开发一套通用的代码封装加载逻辑等。

# 详细设计

项目根模块通过 provider 提供本地缓存使用的 key，如果不提供，导航配置将不会缓存到 localStorage。加载配置时 loader 会分别缓存一份配置到 localStorage 和内存，以免下次打开页面导航栏长时间空白或切换视图时需要重新加载配置。

loader 只提供独立的加载、解析、转换配置格式的方法，业务方根据自己的需求自由选择组合哪些方法。

提供 map, filter, find 等操作树形数据结构的函数。

```typescript
class NavLoaderService {
  loadNavConfig(path: string): Observable<string>;
  parseYaml(): OperatorFunction<string, Tree[]>;
  mapToAuiNavConfig(): OperatorFunction<Tree[], NavItemConfig[]>;
  mapToAuiNavGroupConfig(): OperatorFunction<Tree[], NavGroupConfig[]>;
}

function filterNode(
  nodeKey: string,
  fn: (node: Tree, parents?: Tree[]) => boolean,
): (nodes: Tree[], parents?: NavTree[]) => NavTree[];

function mapNode(
  nodeKey: string,
  fn: (node: Tree, parents?: Tree[]) => Tree,
): (nodes: Tree[], parents?: NavTree[]) => NavTree[];

function findPath(
  nodeKey: string,
  fn: (node: Tree, parents?: Tree[]) => boolean,
): (nodes: Tree[], parents?: NavTree[]) => NavTree[];

function firstPath(nodeKey: string): (nodes: Tree[]) => NavTree[];

interface NavTree {
  [key: string]: any;
  children?: NavTree[];
}

interface NavItem {
  name: string;
  icon?: string;
  label?: string;
  href?: string;
  hidden?: boolean;
  children?: NavItem[];
}

interface NavGroup {
  title?: string;
  items: NavItem[];
}
```

# 缺点

NAN

# 替代方案

NAN

# 实施策略

使用 navLoader 加载配置后，根据自己项目的导航栏渲染方式自由选择后续操作方法。

建议在进入视图首页跳转特定 feature 页面时跳转导航栏第一项，同时处理好配置加载慢的情况。参考：

```typescript
race(
  this.layout.navConfig$.pipe(
    map(firstPath),
    map(nodes => {
      return nodes[nodes.length - 1].href as string;
    }),
    filter(
      href => !(href.startsWith('http://') || href.startsWith('https://')),
    ),
  ),
  of('cluster').pipe(delay(100)),
)
  .pipe(take(1))
  .subscribe(href => {
    this.router.navigate([href], {
      relativeTo: this.activatedRoute,
      replaceUrl: true,
    });
  });
```

在监听导航栏被点击跳转页面时要先判断链接是否是外链。

```typescript
handleActivatedItemChange({ href }: { href: string }) {
  if (href.startsWith('http://') || href.startsWith('https://')) {
    window.open(href, '_blank');
  } else {
    this.router.navigateByUrl(
      this.buildUrl(href, this.activatedRoute.snapshot.paramMap),
    );
  }
}
```

# 未解决的问题

NAN
