- 开始时间: 2020-04-01
- 目标主版本: 2.3
- 相关 issues: https://bitbucket.org/mathildetech/alauda-common-snippet/issues/19/common-header
- 实现 PR: (留空)

# 概要

统一实现头像下拉菜单

# 基本实例

```html
<!-- template.html -->
<acl-account-menu></acl-account-menu>
```

```html
underlord 才需要传 platformPath 为空值
<!-- template.html -->
<acl-account-menu platformPath=""></acl-account-menu>
```

# 动机

目前头像下拉菜单是在各产品代码仓中独立实现的。这块的功能比较统一是可以统一实现，减少各代码仓的维护成本。

# 详细设计

```typescript
@Component({
  selector: 'acl-account-menu',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountMenuComponent {
  // underlord 中设置为空，通过 router.navigate 跳转不会刷新页面
  @Input()
  platformPath = 'console-platform';

  navigateToLink(href: string) {
    if (!this.platformPath) {
      this.router.navigate([href]);
    } else {
      window.open(href);
    }
  }
}
```

# 缺点

NaN

# 替代方案

NaN

# 实施策略

替换各产品不同视图下 layout 中的 <xxx-account-menu>

# 未解决的问题

NaN
