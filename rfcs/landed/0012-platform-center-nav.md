- 开始时间: 2020-04-01
- 目标主版本: 2.3
- 相关 issues: https://bitbucket.org/mathildetech/alauda-common-snippet/issues/19/common-header
- 实现 PR: (留空)

# 概要

统一实现平台中心管理的菜单

# 基本实例

```html
<!-- template.html -->
<acl-platform-center-nav></acl-platform-center-nav>
```

```html
underlord 才需要传 platformPath 为空值
<!-- template.html -->
<acl-platform-center-nav platformPath=""></acl-platform-center-nav>
```

# 动机

目前关于平台的一些管理操作是放在头像的下拉菜单。会给用户带来一些困惑，不能很好的引导用户找到平台管理的入口。
http://confluence.alauda.cn/pages/viewpage.action?pageId=61924083

# 详细设计

```typescript
@Component({
  selector: 'acl-platform-center-nav',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlatformCenterNavComponent {
  // underlord 中设置为空，通过 router.navigate 跳转不会刷新页面
  @Input()
  platformPath = 'console-platform';

  navigateToLink(href: string) {
    if (!this.platformPath) {
      this.router.navigate([href]);
    } else {
      window.open(href);
    }
  }
}
```

# 缺点

NaN

# 替代方案

NaN

# 实施策略

各产品需要在不同视图下 layout 中添加 <acl-platform-center-nav> 到对应的位置

# 未解决的问题

NaN
