- 开始时间: 2020-03-06
- 目标主版本: 2.2
- 相关 issues: N/A
- 实现 PR: [#164](https://bitbucket.org/mathildetech/alauda-common-snippet/pull-requests/164)

# 概要

监听某单个资源的变化，由 K8s 原生提供的 List 监听机制实现

# 基本实例

```ts
class HomeComponent {
  dataLoader = new AsyncDataLoader({
    fetcher: this.fetchCsp.bind(this),
  });

  constructor(private readonly k8sApi: K8sApiService) {}

  fetchCsp() {
    return this.k8sApi.watchGlobalResource({
      definition: {
        type: 'csps',
        apiGroup: 'product.alauda.io',
      },
      name: 'csp',
    });
  }
}
```

# 动机

将数据更新时机从前端转移到后端，如果没有需要更新的 Item，response 是 204 No Content，有需要更新的内容的话返回的是最小更新集，而轮训每次都是全量集。

# 详细设计

原生 K8s 列表接口正常情况返回 `KubernetesResourceList`, 当添加 query 参数 `watch: true` 时将变成长连接，在指定时间段内将期间变更的所有信息通过 [`JSONStream`](https://en.wikipedia.org/wiki/JSON_streaming) 的形式一次性传递回来，当列表数据没有任何变更时返回 Empty Response。
对应的 `JSONStream` 的基本结构是：

```jsonschema
{
  "type": "added",
  "object": {}
}
{
  "type": "modified",
  "object": {}
}
```

因此需要自定义 `response` 解析功能。

功能上，第一次请求不应该使用 `watch` 因为只需要获取最新数据，之后进入正常的 `watch` 流程，并传入已获取数据中返回的 `resourceVersion` 以排除不必要的变更内容。对于 `error` 的响应，应该转化为延时的空响应，避免一直循环 `error`。

对比 `K8sApiService` 中的其他方法，虽然都是返回 `Observable`，但实际上其他方法返回的请求是一次性的，即收到响应后 `Observable` 即进入 `complete` 状态。而 `watchResource` 则不同，收到响应后还需要继续进行下一轮 `watch`，因此需要自定义一个 `Observable`，只返回正常有效的 `resource` 数据。

# 缺点

N/A

# 替代方案

由开发者自行实现轮询，不使用 `watch` 机制。

```ts
resource$ = interval(15 * 1000).pipe(
  concatMap(() =>
    this.k8sApi.getResource({
      definition: {
        type: 'csps',
        apiGroup: 'product.alauda.io',
      },
      name: 'csp',
    }),
  ),
);
```

普通的轮询可以实现类似功能，但轮询的问题是每次获取的数据都是全量的，资源未发生变更时也会返回，即会发生很多无意义的响应。

另外，`WebSocket` 是比长连接 `watch` 更好的变更通知机制。

# 实施策略

所有资源详情页都可以考虑迁移到 `watch` API，当然这需要跟各自产品再确认一下。

# 未解决的问题

N/A
