- 开始时间: 2020-03-27
- 目标主版本: 2.2
- 相关 issues: (如果有，填上已存在的相关 issues)
- 实现 PR: (留空)

# 概要

通过 feature gate 之外的条件（通常是通过特定 api）判断功能可用性时的页面反馈组件。

# 基本实例

```html
<acl-page-guard
  [status]="isNetworkSupport$ | async"
  [title]="'subnet_not_support_title' | translate"
  [description]="'subnet_not_support_content' | translate"
>
  <div *aclPageGuardContent>
    ...
  </div>
  <div *aclPageGuardOperation>
    <button aui-button="primary">available</button>
  </div>
</acl-page-guard>

<acl-page-guard
  [status]="isNetworkSupport$ | async"
  [operation]="operationTemplate"
  [reason]="Reason.NotSupported"
  [title]="'title'"
>
  <div *aclPageGuardContent>
    ...
  </div>
  <div *aclPageGuardDescription>
    <p>1. 集群 master 节点有变更，请重新生成 license。如果不做 license 变更</p>
  </div>

  <div *aclPageGuardOperation>
    <button aui-button="primary" (click)="status=guardStatus.Available">
      available
    </button>
  </div>
</acl-page-guard>
```

# 动机

目前设计规范规定只有通过 feature gate 禁用功能时从导航栏隐藏链接，其他情况允许打开页面，并在功能不可用时显示说明（不支持、无权限、未启用未开启未部署等等）。因此需要实现一个通用的 UI 组件。

# 详细设计

使用类似 `acl-page-state` 组件的方式控制页面内容的显示。

```typescript
export enum Reason {
  // 不支持
  NotSupported = 'notSupported',
  // 无权限
  NoPermission = 'noPermission'
  // 未部署
  NotDeployed = 'notDeployed'
}
const REASON_MAP = {
  [Reason.NotSupported]: {
    src: 'images/not-installed.svg',
    title: 'not_support',
    description: 'not_support_description'
  },
  [Reason.NoPermission]: {
    src: 'images/not-permission.svg',
    title: 'no_permission',
    description: 'no_permission_description'
  },
  [Reason.NotDeployed]: {
    src: 'images/not-deployed.svg',
    title: 'not-deployed',
    description: 'no_deployed_description',
  },
}
const DEFAULT_REASON = REASON_MAP[Reason.NotSupported];
@Component({
  selector: 'acl-page-guard',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageGuardComponent {
   @Input() status: GuardStatus = GuardStatus.Pending;

  @Input()
  set reason(reason: Reason) {
    if (reason) {
      this.reasonModel = REASON_MAP[reason] || DEFAULT_REASON;
    }
  }

  @Input() title = '';

  @Input() description: string;

  @ContentChild(DescriptionDirective, { read: TemplateRef })
  descriptionTpl: TemplateRef<unknown>;

  @ContentChild(OperationDirective, { read: TemplateRef })
  operation: TemplateRef<unknown>;

  @ContentChild(ContentDirective, { read: TemplateRef })
  template: TemplateRef<unknown>;

  reasonModel = DEFAULT_REASON;

  isNonEmptyString(value: {}): boolean {
    return typeof value === 'string' && value !== '';
  }

  isUndefined(value: string) {
    return typeof value === 'undefined';
  }

  isTemplateRef(value: {}): boolean {
    return value instanceof TemplateRef;
  }
}

```

# 缺点

N/A

# 替代方案

N/A

# 实施策略

有需要的新功能直接使用 `acl-page-guard`，已自行实现功能的页面自行安排时间替换。
并需要在 `angular.json assets` 中添加 `assets`

```
 "assets": [
   // ....
    {
      "glob": "**/*",
      "input": "node_modules/@alauda/common-snippet/assets",
      "output": "/"
    },
  // ....
  ],
```

# 未解决的问题

UI 设计上卡片是全屏，组件放到 common 里不好实现。
