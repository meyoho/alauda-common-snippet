- 开始时间: 2019-08-27
- 目标主版本: 1.2.0
- 相关 issues: https://bitbucket.org/mathildetech/alauda-common-snippet/issues/2/k8sresourcelist-translateservice-common
- 实现 PR: (留空)

# 概要

封装原生 K8S 资源列表的加载和显示逻辑。

# 基本实例

```typescript
// component.ts
class Component {
  list = new K8SResourceList({
    fetcher: queryParams => this.api.someMethod(queryParams),
    activatedRoute: this.activatedRoute,
  });
}
```

```html
<!-- template.html -->
<aui-table [dataSource]="list.items$ | async">
  ...
</aui-table>
<acl-k8s-resource-list-footer [list]="list"></acl-k8s-resource-list-footer>
```

# 动机

K8S 资源列表 api 加载逻辑和状态都比较复杂，所以希望能将其封装进黑盒，降低开发成本。

只支持加载 K8S 资源列表，包含原生 api 和高级搜索。

通过 low level、high level 组件结合使用的方式，预期能大大简化资源列表页的开发成本，同时又有足够的扩展性。

我们为什么要这么做？它支持哪些用例？预期的结果是什么？

# 详细设计

```typescript
export class K8SResourceList {
  public rawResponse$;
  public items$;
  public hasMore$;
  public loading$;
  public loadingError$;

  constructor(options: OptionWithRoute | OptionWithParams) {}

  public reload(resource: K8SResource) {}
  public loadMore() {}
  public scanItems(fn) {}
  public create(resource: K8SResource) {}
  public update(resource: K8SResource) {}
  public delete(resource: K8SResource) {}
}

export interface OptionWithRoute<R> extends BaseOption {
  fetcher: (
    params: ResourceListParams,
  ) => Observable<KubernetesResourceList<R>>;
  activatedRoute: ActivatedRoute;
}

export interface OptionWithParams<R, P> extends BaseOption {
  fetcher: (
    params: ResourceListParams,
  ) => Observable<KubernetesResourceList<R>>;
  fetchParams$: Observable<P>;
}

@Component({
  selector: 'acl-k8s-resource-list',
})
export class K8SResourceListComponent {
  @Input() list: ResourceList;
  @Input() autoLoad = true;
  @Input() backTop = true;
  @Input() bottomDistance = 60;
}
```

# 缺点

NAN

# 替代方案

主要考虑了基类继承、Service 封装、AsyncData 几种方式，但是因为扩展性不够或使用太复杂等原因放弃。

# 实施策略

目前所有用到 K8S 资源列表的项目都已经通过复制代码的方式开始使用。替换时需要移除项目内的实现，接口没有变化通过搜索组件名来全局替换组件即可。如果使用「方案一」，在替换 `rc-resource-list-footer` 时会麻烦一点。预计编译期即可发现所有错误，如果其他项目自己对原有的 ResourceList 做了定制需要自行解决问题。

# 未解决的问题

NAN
