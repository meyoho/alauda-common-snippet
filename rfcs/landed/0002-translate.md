- 开始时间: 2019-08-23
- 目标主版本: 1.x
- 相关 issues: https://bitbucket.org/mathildetech/alauda-common-snippet/issues/2 https://bitbucket.org/mathildetech/alauda-common-snippet/issues/3
- 实现 PR: https://bitbucket.org/mathildetech/alauda-common-snippet/pull-requests/86

# 概要

实现一个精简地、符合我们业务需求的翻译服务替代 `NgxTranslate` 及每个子项目中继续包装的 `TranslateService`

# 基本实例

```ts
@NgModule({
  imports: [
    TranslateModule.forRoot({
      defaultLocale: Locale.ZH,
      locales: [Locale.EN, Locale.ZH],
      translations: {},
      loose: true, // 宽松模式，即将所有 `zh-*` -> `zh`，`en-*` -> `en`,
      remoteUrl: url, // 远程 JSON 文件，最高优先级
    }),
  ],
})
class AppModule {}

@NgModule({
  imports: [
    TranslateModule.forChild({
      translations: {},
    }),
  ],
})
class HomeModule {}

@Component()
class HomeComponent {
  locale$ = this.translate.locale$;

  constructor(private translate: TranslateService) {}

  changeLocale() {
    this.translate.changeLocale(Locale.EN);
  }

  toggleLocale() {
    this.translate.toggleLocale();
  }
}
```

```html
<div>{{ 'hello' | translate:'world' }}</div>
```

# 动机

`NgxTranslate` 功能大而全，但是我们只使用其最基本的功能，没有必要，因此考虑实现一个精简地、符合我们业务需求的翻译服务。

# 详细设计

1. 提供 `Locale`, `DefaultLocale`, `Locales`, `remoteUrl` 等四个 Token 注入相关信息，不传入 `Locale` 时自动解析 `BrowserLanguage` 并存储到 `localStorage` 中。
2. 针对未提供翻译项直接传入的问题，开发环境选择性进行提示缺失翻译。
3. 当前使用的 `locale` 和 默认回退的 `defaultLocale` 都可以动态修改，并支持 `Observable`
4. 异步加载的模块支持动态新增翻译，但不支持取消，如果翻译项有重复，开发环境进行提醒
5. 支持嵌套命名空间翻译 key 和动态参数，可以减少翻译文件中重复的前缀，并且可以在编辑器中折叠不相关的 block，添加新翻译项也更容易查找
6. 提供服务端返回的包含翻译信息的数据的翻译支持。类似 `{{ { en: 'test', 'zh-CN': '测试' } | translate }}`

# 缺点

暂无

# 替代方案

暂无

# 实施策略

1. 部分核心翻译即各个项目中完全通用的翻译直接由 common 提供，子项目去除 `NgxTranslate` 依赖和自己实现的 `TranslateService`，在 `AppModule` 中使用 `TranslateModule.forRoot` 兼容之前的单个翻译文件，
   并在开发环境根据 console 提示去除与核心翻译重复的翻译项，验证通过后合入 master 。
2. 虽然可以支持异步加载并注入翻译，但仍建议项目仅使用一份翻译文件
3. 建议翻译 key 命名严格符合实际语义，而不是上下文上的功能名称，能有效减少歧义
