- 开始时间: 2020-03-17
- 目标主版本: 2.2
- 相关 issues: (如果有，填上已存在的相关 issues)
- 实现 PR: (留空)

# 概要

在 `K8sUtilService` 中增加一个 `updateResource`, `updateGlobalResource` 方法用于更新 k8s 资源，当因资源 `resourceVersion` 过期导致更新失败可以由用户确认后自动重试。

# 基本实例

```typescript
export class TAppUpdateComponent {

  constructor(private k8sUtil: K8sUtilService) {}

  submit() {
    const params = {
      type: RESOURCE_TYPES.TAPP,
      cluster: this.baseParams.cluster,
      resource: this.resource,
    };

    this.k8sUtil.updateResource(params).subscribe(
      () => {
        ...
      },
      () => {
        ...
      },
    );
  }
}
```

# 动机

用户在资源更新页提交表单可能因各种原因发生当前 `resourceVersion` 过期而导致的更新失败的情况。因此提供一个方法，在因 `resourceVersion` 过期导致更新失败时弹出一个对话框，由用户决定是否自动使用最新的 `resourceVersion` 重试更新操作。

# 详细设计

见[代码](../../packages/alauda-common-lib/src/utils/k8s-util.service.ts) `updateResource` 部分。

# 缺点

N/A

# 替代方案

在更新时使用 `patch` 方法更新完整资源不会产生 version 冲突，但是使用 `patch` 方法完整更新资源会导致生成的日志语义混乱，不易区分部分字段更新操作和完整更新操作。

# 实施策略

非 breaking change，且方法接受的参数与 `k8sApiService.putResource` 基本相同，各项目根据自己的工作排期逐步替换即可。

# 未解决的问题

N/A
