- 开始时间: 2019-07-15
- 目标主版本: 1.0
- 相关 issues: 无
- 实现 PR:

请直接使用 `*ngIf` 或尝试配合 `ngTemplateOutlet` 使用。

---

# 概要

处理模板中使用临时变量问题的指令，适合多次使用的复杂表达式、需要在事件回调传入的 `Observable` 或 `Promise` 参数、需要自动过滤 `null` 的其他输入值，以此简化或移除 js 代码对临时变量/快照(`Snapshot`)的处理，支持自动解构。

# 基本实例

```html
<ng-container *aclVar="let content from content$; nullable: false">
  <div (click)="onClick(content)">{{ content.text }}</div>
</ng-container>

<aui-menu #menu size="small">
  <a
    *rcVar="
              let endpoint;
              from: cluster
                | aclAnnotation: 'k8s-dashboard-endpoint':'infrastructure'
            "
    [href]="endpoint || 'javascript:;' | rcTrustUrl"
    target="_blank"
    rel="noopener noreferrer"
    stop
  >
    <aui-menu-item [disabled]="!endpoint">
      {{ 'cluster_management' | translate }}
    </aui-menu-item>
  </a>
  <aui-menu-item
    *rcVar="let resourceRatioFeature; from: resourceRatioFeature$"
    (click)="updateResourceRatio(resourceRatioFeature)"
  >
    {{ 'update_over_commit' | translate }}
  </aui-menu-item>
</aui-menu>
```

# 动机

在模板中我们经常会遇到使用临时变量的问题，比如我们有一个 `Observable` 对象 `observable$`，如果我们需要在事件回调中传回这个对象的当前值时，一般我们会在 js 中定义一个 snapshot:

```ts
export class Demo {
  observable$ = this.getObservable().pipe(
    tap(snapshot => {
      this.snapshot = snapshot;
    }),
  );
}
```

然后我们就可以在事件回调中使用 snapshot 传参或者直接调用 `this.snapshot` 了。

但是很显然，`snapshot` 的作用只是作为一个中间变量，增加了逻辑的复杂度。

而对于模板中多次使用到的较复杂的表达式，如果有修改需求，很可能会漏改（列表循环中出现相对较复杂的表达式，用脚本临时变量解决也很麻烦）。

因此，我们需要一个直接在模板中定义临时变量的方式来解决这些小问题。

# 详细设计

针对具体的使用场景，我们的指令会支持 `Observable`, `Promise` 和其他普通值三种输入，并支持根据传入 `Input` `nullable: false` 自动将 `null/undefined` 过滤掉。

我们应该将这三种输入自动转化为一个通用的 `Observable`，然后读取 `TemplateRef` 进行渲染，当 `input` 发生变化时自动响应并重新渲染。

此外，最好能支持对象解构，省去为临时变量命名的烦恼，同时也可以减少后续使用临时变量时的复杂度。

# 缺点

1. 和其他指令配合时可能需要注意嵌套顺序，例如在 `aui-tab` 中使用时如果用下面的方式 `tab` 将无法正确渲染：

   ```html
   <aui-tab>
     <ng-container *aclVar="let value from value$">
       <div *auiTabContent></div>
     </ng-container>
   </aui-tab>
   ```

   因为 `ViewChild` 是无法读取到嵌套的 `TemplateRef` 的，这可能对使用者来说增加了学习成本。

2. 当发现还有其他地方需要共享这个临时变量时，要么需要声明两次，要么需要把 `aclVar` 的声明层级不断上提，找到可以共用的作用域。

# 替代方案

暂无

# 实施策略

原有代码不会受到任何影响，调用者需要使用时将此指令加入 `SharedModule` 中后即可按使用说明编写代码。

# 未解决的问题

自动拷贝 `Input` 的属性到 `context` 中是否是一个好的设计。`ngFor` 没有这样的功能，因为上下文中还有其他属性(`index`, `first`, `last` 等)，而 `aclVar` 中没有其他值可能造成冲突。
