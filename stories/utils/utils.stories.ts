import { CommonModule } from '@angular/common';
import { storiesOf } from '@storybook/angular';

import { DemoComponent } from './demo.component';
import { ObservableInputComponent } from './observable-input.component';

storiesOf('Alauda Common Snippet Library', module).add(
  'decorator ObservableInput',
  () => ({
    moduleMetadata: {
      imports: [CommonModule],
      declarations: [ObservableInputComponent, DemoComponent],
    },
    component: DemoComponent,
  }),
);
