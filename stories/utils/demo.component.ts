import { Component } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  template: `
    <acl-demo-observable-input [num]="num$ | async"></acl-demo-observable-input>
  `,
})
export class DemoComponent {
  num$ = interval(1000);
}
