import { ObservableInput, ValueHook } from '@alauda/common-snippet';
import { Component, Input } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'acl-demo-observable-input',
  template: `
    num: {{ num$$ | async }} - {{ num }}
  `,
})
export class ObservableInputComponent {
  @ObservableInput(true)
  num$$: Subject<number>;

  @ValueHook(console.log)
  @Input()
  num: number;
}
