// tslint:disable-next-line: no-namespace
declare namespace placeholder {
  type PlaceholderOptions = Partial<{
    size: string;
    bgcolor: string;
    color: string;
    text: string;
    fstyle: string;
    fweight: string;
    fsize: string;
    ffamily: string;
  }>;

  interface Placeholder {
    getCanvas(options?: PlaceholderOptions): HTMLCanvasElement;
    getData(options?: PlaceholderOptions): string;
    render(force?: string): void;
  }
}

declare const placeholder: placeholder.Placeholder;

export = placeholder;

export as namespace placeholder;
