import { PipelineNode, Stage } from '@alauda/common-snippet';
import { Component } from '@angular/core';

import { stages1 } from './examples/1';

@Component({
  template: `
    <button aui-button="primary" type="button" (click)="fetchData()">
      fetchData
    </button>
    <acl-pipeline-status-diagram
      [stages]="stages"
      [selected]="selected"
      (selectedChange)="onSelect($event)"
    ></acl-pipeline-status-diagram>
  `,
})
export class DiagramWithSelectionComponent {
  selected: PipelineNode<Stage> = null;

  stages = stages1;

  onSelect(node: PipelineNode<Stage>) {
    this.selected = node;
  }

  fetchData() {
    console.log('previous stages:', this.stages, 'selected:', this.selected);
    this.stages = this.stages.map((stage: Stage) => {
      return {
        ...stage,
        durationInMillis: Math.random(),
      };
    });
    console.log('after fetch stages:', this.stages, 'selected:', this.selected);
  }
}
