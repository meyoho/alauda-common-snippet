import { PipelineDiagramModule } from '@alauda/common-snippet';
import { ButtonModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { storiesOf } from '@storybook/angular';

import { DiagramWithSelectionComponent } from './diagram-with-selection.component';
import { stages1 } from './examples/1';
import { stages2 } from './examples/2';
import { stages3 } from './examples/3';
import { stages4 } from './examples/4';
import { stages5 } from './examples/5';
import { skipAll } from './examples/skip-all';

const imports = [CommonModule, PipelineDiagramModule, ButtonModule];

storiesOf('Pipeline Status Diagrams', module)
  .add('default', () => {
    return {
      moduleMetadata: {
        imports,
      },
      template: `
      <acl-pipeline-status-diagram
        [stages]="stages"
      ></acl-pipeline-status-diagram>
    `,
      props: {
        stages: stages1,
      },
    };
  })
  .add('with skipped stages', () => {
    return {
      moduleMetadata: {
        imports,
      },
      template: `
      <acl-pipeline-status-diagram
        [stages]="stages"
      ></acl-pipeline-status-diagram>
    `,
      props: {
        stages: stages2,
      },
    };
  })
  .add('with skipped stages near by forks', () => {
    return {
      moduleMetadata: {
        imports,
      },
      template: `
      <acl-pipeline-status-diagram
        [stages]="stages"
      ></acl-pipeline-status-diagram>
    `,
      props: {
        stages: stages3,
      },
    };
  })
  .add('skip parallels', () => {
    return {
      moduleMetadata: {
        imports,
      },
      template: `
      <acl-pipeline-status-diagram
        [stages]="stages"
      ></acl-pipeline-status-diagram>
    `,
      props: {
        stages: stages4,
      },
    };
  })
  .add('fork from start', () => {
    return {
      moduleMetadata: {
        imports,
      },
      template: `
      <acl-pipeline-status-diagram
        [stages]="stages"
      ></acl-pipeline-status-diagram>
    `,
      props: {
        stages: stages5,
      },
    };
  })
  .add('with selection', () => {
    return {
      moduleMetadata: {
        imports,
        declarations: [DiagramWithSelectionComponent],
      },
      component: DiagramWithSelectionComponent,
    };
  })
  .add('skip all', () => {
    return {
      moduleMetadata: {
        imports,
      },
      template: `
      <acl-pipeline-status-diagram
        [stages]="stages"
      ></acl-pipeline-status-diagram>
    `,
      props: {
        stages: skipAll,
      },
    };
  });

storiesOf('Pipeline Preview Diagrams', module).add('default', () => {
  return {
    moduleMetadata: {
      imports,
    },
    template: `
    <acl-pipeline-preview-diagram
      [stages]="stages"
      [hideStatus]="true"
    ></acl-pipeline-preview-diagram>
      <acl-pipeline-preview-diagram
        [stages]="stages"
        [nodeRadius]="12"
        [endNodeRadius]="6"
        [gridX]="104"
        [endOffset]="36"
      ></acl-pipeline-preview-diagram>
      <acl-pipeline-preview-diagram
        [stages]="stages"
      ></acl-pipeline-preview-diagram>
    `,
    props: {
      stages: stages1,
    },
  };
});
