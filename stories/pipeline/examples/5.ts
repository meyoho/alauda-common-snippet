import { Stage } from '@alauda/common-snippet';

export const stages5: Stage[] = [
  {
    name: 'StartFork',
    durationInMillis: 34,
    id: '6',
    result: 'SUCCESS',
    startTime: '2018-07-04T09:02:56.485+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '10',
        type: 'PARALLEL',
      },
      {
        id: '11',
        type: 'PARALLEL',
      },
      {
        id: '12',
        type: 'PARALLEL',
      },
    ],
  },
  {
    name: 'First',
    durationInMillis: 25,
    id: '10',
    result: 'SUCCESS',
    startTime: '2018-07-04T09:02:56.494+0000',
    status: 'FINISHED',
    type: 'PARALLEL',
    edges: [
      {
        id: '35',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Second',
    durationInMillis: 24,
    id: '11',
    result: 'SUCCESS',
    startTime: '2018-07-04T09:02:56.495+0000',
    status: 'FINISHED',
    type: 'PARALLEL',
    edges: [
      {
        id: '35',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Third',
    durationInMillis: 23,
    id: '12',
    result: 'SUCCESS',
    startTime: '2018-07-04T09:02:56.496+0000',
    status: 'FINISHED',
    type: 'PARALLEL',
    edges: [
      {
        id: '35',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Next',
    durationInMillis: 19,
    id: '35',
    result: 'SUCCESS',
    startTime: '2018-07-04T09:02:57.011+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [],
  },
];
