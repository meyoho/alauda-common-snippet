import { Stage } from '@alauda/common-snippet';

export const stages4: Stage[] = [
  {
    name: 'Checkout',
    durationInMillis: 266,
    id: '8',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:50:32.116+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '13',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Skipable',
    durationInMillis: 508,
    id: '13',
    result: 'NOT_BUILT',
    startTime: '2018-06-27T09:50:32.872+0000',
    status: 'SKIPPED',
    type: 'STAGE',
    edges: [
      {
        id: '39',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Build',
    durationInMillis: 473,
    id: '17',
    result: 'NOT_BUILT',
    startTime: '2018-06-27T09:50:32.907+0000',
    status: 'NOT_BUILT',
    type: 'PARALLEL',
    edges: [
      {
        id: '39',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Code Scan',
    durationInMillis: 438,
    id: '18',
    result: 'NOT_BUILT',
    startTime: '2018-06-27T09:50:32.942+0000',
    status: 'NOT_BUILT',
    type: 'PARALLEL',
    edges: [
      {
        id: '39',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Test',
    durationInMillis: 401,
    id: '19',
    result: 'NOT_BUILT',
    startTime: '2018-06-27T09:50:32.979+0000',
    status: 'NOT_BUILT',
    type: 'PARALLEL',
    edges: [
      {
        id: '39',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Deploying',
    durationInMillis: 163,
    id: '39',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:50:35.387+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '44',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Manual Test',
    durationInMillis: 200,
    id: '44',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:50:35.804+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '49',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Promoting',
    durationInMillis: 195,
    id: '49',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:50:36.208+0000',
    status: 'FINISHED',
    type: 'STAGE',
  },
];
