import { Stage } from '@alauda/common-snippet';

export const skipAll: Stage[] = [
  {
    name: 'Checkout',
    durationInMillis: 266,
    id: '8',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:50:32.116+0000',
    status: 'SKIPPED',
    type: 'STAGE',
    edges: [
      {
        id: '13',
        type: 'STAGE',
      },
    ],
  },
];
