import { Stage } from '@alauda/common-snippet';

export const stages2: Stage[] = [
  {
    name: 'Checkout',
    durationInMillis: 31171,
    id: '15',
    result: 'SUCCESS',
    startTime: '2018-06-29T02:20:08.392+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '34',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'CI',
    durationInMillis: 359,
    id: '34',
    result: 'SUCCESS',
    startTime: '2018-06-29T02:20:39.858+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '38',
        type: 'PARALLEL',
      },
      {
        id: '39',
        type: 'PARALLEL',
      },
      {
        id: '40',
        type: 'PARALLEL',
      },
    ],
  },
  {
    name: 'Build',
    durationInMillis: 333,
    id: '38',
    result: 'SUCCESS',
    startTime: '2018-06-29T02:20:39.884+0000',
    status: 'FINISHED',
    type: 'PARALLEL',
    edges: [
      {
        id: '167',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Code Scan',
    durationInMillis: 304,
    id: '39',
    result: 'SUCCESS',
    startTime: '2018-06-29T02:20:39.913+0000',
    status: 'FINISHED',
    type: 'PARALLEL',
    edges: [
      {
        id: '167',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Test',
    durationInMillis: 243,
    id: '40',
    result: 'SUCCESS',
    startTime: '2018-06-29T02:20:39.974+0000',
    status: 'FINISHED',
    type: 'PARALLEL',
    edges: [
      {
        id: '167',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Deploying',
    durationInMillis: 46849,
    id: '167',
    result: 'SUCCESS',
    startTime: '2018-06-29T02:42:01.351+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '240',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Manual test',
    durationInMillis: 0,
    id: '240',
    result: 'NOT_BUILT',
    startTime: null,
    status: 'SKIPPED',
    type: 'STAGE',
    edges: [
      {
        id: '244',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Promoting',
    durationInMillis: 57614,
    id: '244',
    result: 'SUCCESS',
    startTime: '2018-06-29T02:42:49.841+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [],
  },
];
