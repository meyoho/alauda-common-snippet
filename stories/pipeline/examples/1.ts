import { Stage } from '@alauda/common-snippet';

export const stages1: Stage[] = [
  {
    name: 'Checkout',
    durationInMillis: 14146,
    id: '15',
    result: 'SUCCESS',
    startTime: '2018-06-26T09:14:16.627+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '32',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'CI',
    durationInMillis: 306,
    id: '32',
    result: 'SUCCESS',
    startTime: '2018-06-26T09:14:31.079+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '36',
        type: 'PARALLEL',
      },
      {
        id: '37',
        type: 'PARALLEL',
      },
      {
        id: '38',
        type: 'PARALLEL',
      },
      {
        id: '39',
        type: 'PARALLEL',
      },
    ],
  },
  {
    name: 'Build',
    durationInMillis: 263,
    id: '36',
    result: 'SUCCESS',
    startTime: '2018-06-26T09:14:31.122+0000',
    status: 'FINISHED',
    type: 'PARALLEL',
    edges: [
      {
        id: '171',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Code Scan',
    durationInMillis: 243,
    id: '37',
    result: 'UNKNOWN',
    startTime: '2018-06-26T09:14:31.142+0000',
    status: 'RUNNING',
    type: 'PARALLEL',
    edges: [
      {
        id: '171',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Test',
    durationInMillis: 163,
    id: '38',
    result: 'FAILURE',
    startTime: '2018-06-26T09:14:31.222+0000',
    status: 'FINISHED',
    type: 'PARALLEL',
    edges: [
      {
        id: '171',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Waiting Input',
    durationInMillis: 81,
    id: '39',
    result: 'UNKNOWN',
    startTime: '2019-07-16T17:23:57.978+0800',
    status: 'PAUSED',
    type: 'PARALLEL',
    edges: [
      {
        id: '171',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Deploying',
    durationInMillis: 0,
    id: '171',
    result: 'ABORTED',
    startTime: null,
    status: 'ABORTED',
    type: 'STAGE',
    edges: [
      {
        id: '175',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Manual test',
    durationInMillis: 0,
    id: '175',
    result: 'NOT_BUILT',
    startTime: null,
    status: 'NOT_BUILT',
    type: 'STAGE',
    edges: [
      {
        id: '179',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Promoting',
    durationInMillis: 0,
    id: '179',
    result: 'NOT_BUILT',
    startTime: null,
    status: 'NOT_BUILT',
    type: 'STAGE',
    edges: [],
  },
];
