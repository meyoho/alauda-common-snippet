import { Stage } from '@alauda/common-snippet';

export const stages3: Stage[] = [
  {
    name: 'Checkout a long text name here just for test',
    durationInMillis: 275,
    id: '8',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:52:54.039+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '13',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Skipable',
    durationInMillis: 402,
    id: '13',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:52:54.577+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [
      {
        id: '17',
        type: 'PARALLEL',
      },
      {
        id: '18',
        type: 'PARALLEL',
      },
      {
        id: '19',
        type: 'PARALLEL',
      },
    ],
  },
  {
    name: 'Build',
    durationInMillis: 357,
    id: '17',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:52:54.622+0000',
    status: 'FINISHED',
    type: 'PARALLEL',
    edges: [
      {
        id: '42',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Code Scan',
    durationInMillis: 314,
    id: '18',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:52:54.665+0000',
    status: 'FINISHED',
    type: 'PARALLEL',

    edges: [
      {
        id: '42',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Test',
    durationInMillis: 280,
    id: '19',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:52:54.699+0000',
    status: 'FINISHED',
    type: 'PARALLEL',

    edges: [
      {
        id: '42',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Deploying',
    durationInMillis: 0,
    id: '42',
    result: 'NOT_BUILT',
    startTime: null,
    status: 'SKIPPED',
    type: 'STAGE',
    edges: [
      {
        id: '46',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Manual Test',
    durationInMillis: 0,
    id: '46',
    result: 'NOT_BUILT',
    startTime: null,
    status: 'SKIPPED',
    type: 'STAGE',

    edges: [
      {
        id: '50',
        type: 'STAGE',
      },
    ],
  },
  {
    name: 'Promoting',
    durationInMillis: 286,
    id: '50',
    result: 'SUCCESS',
    startTime: '2018-06-27T09:52:59.800+0000',
    status: 'FINISHED',
    type: 'STAGE',
    edges: [],
  },
];
