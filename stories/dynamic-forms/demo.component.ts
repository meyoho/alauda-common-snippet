import { FieldDefine, GroupDefine } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { mockControlMapper, mockTemplate } from './mock-data';

@Component({
  templateUrl: 'demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DemoComponent {
  title = 'app';
  @ViewChild('ngForm', { static: false })
  ngForm: NgForm;

  model = {
    abc: '123',
  };

  template = mockTemplate;

  controlMapper = mockControlMapper;

  groupTracker(_: number, group: GroupDefine) {
    return group.displayName.en;
  }

  fieldTracker(_: number, field: FieldDefine) {
    return field.name;
  }

  save() {
    //
  }
}
