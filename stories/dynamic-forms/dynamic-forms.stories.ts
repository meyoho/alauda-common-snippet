import { DynamicFormsModule } from '@alauda/common-snippet';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { storiesOf } from '@storybook/angular';

import { DemoComponent } from './demo.component';

const imports = [
  CommonModule,
  HttpClientModule,
  ReactiveFormsModule,
  DynamicFormsModule,
];

storiesOf('Alauda Common Snippet Library', module).add('Dynamic Forms', () => {
  return {
    moduleMetadata: {
      imports,
      declarations: [DemoComponent],
    },
    component: DemoComponent,
  };
});
