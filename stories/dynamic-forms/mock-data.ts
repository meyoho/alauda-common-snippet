import { ControlMapper, GroupDefine } from '@alauda/common-snippet';
import { of } from 'rxjs';

export const mockTemplate: GroupDefine[] = [
  {
    displayName: {
      en: 'abc',
      'zh-CN': '第一组',
    },
    relation: null,
    items: [
      {
        name: 'abc',
        schema: {
          type: 'string',
        },
        required: false,
        validation: {},
        default: null,
        display: {
          type: 'text',
          name: {
            en: 'field1',
            'zh-CN': '字段1',
          },
          description: {
            en: 'field one',
            'zh-CN': '这是第一个字段',
          },
        },
        relation: null,
      },
      {
        name: 'select1',
        schema: {
          type: 'string',
        },
        required: false,
        validation: {},
        default: null,
        display: {
          type: 'resource1',
          name: {
            en: 'field2',
            'zh-CN': '字段2',
          },
          description: {
            en: 'field two',
            'zh-CN': '这是第二个字段',
          },
        },
        relation: null,
      },
    ],
  },
  {
    displayName: {
      en: 'group2',
      'zh-CN': '第二组',
    },
    relation: [
      {
        when: { name: 'abc', value: '123' },
        action: 'hidden',
      },
    ],
    items: [
      {
        name: 'select2',
        schema: {
          type: 'string',
        },
        required: false,
        validation: {},
        default: null,
        display: {
          type: 'resource2',
          name: {
            en: 'field3',
            'zh-CN': '字段3',
          },
          description: {
            en: 'field one',
            'zh-CN': '这是第一个字段',
          },
          related: 'select1',
        },
        relation: null,
      },
      {
        name: 'field4',
        schema: {
          type: 'string',
        },
        required: false,
        validation: {},
        default: null,
        display: {
          type: 'text',
          name: {
            en: 'field4',
            'zh-CN': '字段4',
          },
          description: {
            en: 'field two',
            'zh-CN': '这是第二个字段',
          },
        },
        relation: [
          {
            when: { name: 'select1', value: 2 },
            action: 'hidden',
          },
        ],
      },
      {
        name: 'select3',
        schema: {
          type: 'string',
        },
        required: false,
        validation: {},
        default: null,
        display: {
          type: 'resource3',
          name: {
            en: 'field4',
            'zh-CN': '字段4',
          },
          description: {
            en: 'field two',
            'zh-CN': '这是第二个字段',
          },
          related: 'select2',
        },
        relation: null,
      },
    ],
  },
];

export const mockControlMapper: ControlMapper = {
  resource1: {
    optionsResolver: () => {
      return of([1, 2, 3]);
    },
  },

  resource2: {
    optionsResolver: related =>
      related ? of([1, 2, 3].map(item => item * 10 ** related)) : of([]),
  },

  resource3: {
    optionsResolver: related =>
      related ? of([1, 2, 3].map(item => item + related)) : of([]),
  },
};
