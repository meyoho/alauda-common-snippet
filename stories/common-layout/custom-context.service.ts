import { CommonLayoutContextService } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { delay, map } from 'rxjs/operators';

import { clusters, projects } from './mock-data';

@Injectable()
export class CustomContextService extends CommonLayoutContextService {
  getProjects() {
    return of(projects).pipe(delay(1000));
  }

  getProducts() {
    return of([
      {
        name: 'console-acp',
        displayName: 'Container Platform',
        url: '/console-acp/',
        index: 3,
      },
      {
        name: 'console-devops',
        displayName: 'DevOps',
        url: '/console-devops/',
        index: 1,
      },
      {
        name: 'console-asm',
        displayName: 'Service Mesh',
        url: '/console-asm/',
        index: 2,
      },
      {
        name: 'console-platform',
        displayName: 'Platform',
        url: '/console-platform/',
        hide: true,
        index: 100,
      },
    ]);
  }

  getClusterNamespaces(_projectName: string, clusterName: string) {
    return of(clusters.find(item => item.name === clusterName)).pipe(
      map(cluster => cluster.namespaces),
      delay(1000),
    );
  }
}
