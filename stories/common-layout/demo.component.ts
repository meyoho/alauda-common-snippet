import {
  CommonLayoutStoreService,
  NamespaceIdentity,
} from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  template: `
    <div class="container">
      <acl-product-select current="console-devops"></acl-product-select>
      <acl-project-select
        [selected]="projectName$ | async"
        (selectedChange)="onSelectedProjectChange($event)"
      ></acl-project-select>
      <acl-namespace-select
        class="namespace-select"
        [projectName]="projectName$ | async"
        [selected]="namespace$ | async"
        (selectedChange)="onSelectedNamespaceChange($event)"
      ></acl-namespace-select>
    </div>
  `,
  styles: [
    `
      .container {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
        background-color: #000;
      }
      .container > * {
        margin: 16px;
      }
      .namespace-select {
        width: 200px;
      }
    `,
  ],
})
export class DemoComponent {
  projectName$ = new BehaviorSubject('bar');

  namespace$ = new BehaviorSubject<NamespaceIdentity>(null);

  project$ = this.projectName$.pipe(
    switchMap(name => this.projectStore.selectProjectByName(name)),
  );

  constructor(private readonly projectStore: CommonLayoutStoreService) {}

  onSelectedProjectChange(name: string) {
    this.projectName$.next(name);
    this.namespace$.next(null);
  }

  onSelectedNamespaceChange(namespace: NamespaceIdentity) {
    this.namespace$.next(namespace);
  }
}
