export const projects = [
  {
    name: 'foo',
    displayName: 'foo display name',
    clusters: ['cluster a1', 'cluster a2', 'cluster a3'],
  },
  {
    name: 'bar',
    displayName: 'bar display name long enough for test',
    clusters: ['cluster b1', 'cluster b2', 'cluster b3'],
  },
  {
    name: 'baz-name-looooooooooooooooooooong-enough-for-test',
    displayName: 'baz display name',
    clusters: ['cluster c1', 'cluster c2', 'cluster c3'],
  },
  { name: 'project1' },
  { name: 'project2' },
  { name: 'project3' },
  { name: 'project4' },
  { name: 'project5' },
  { name: 'project6' },
  { name: 'project7' },
  { name: 'project8' },
  { name: 'project9' },
  { name: 'project10' },
  { name: 'project11' },
  { name: 'project12' },
  { name: 'project13' },
].map(({ name, displayName, clusters: _clusters }) => ({
  metadata: {
    name,
    annotations: {
      'alauda.io/display-name': displayName,
    },
    labels: {
      'alauda.io/cluster': name,
    },
  },
  spec: {
    clusters: _clusters && _clusters.map(_name => ({ name: _name })),
  },
}));

export const clusters = projects.flatMap(item =>
  (item.spec.clusters || []).map(({ name }) => ({
    name,
    displayName: `${name} display name`,
    namespaces: new Array(5).fill(null).map((_, index) => ({
      metadata: {
        name: `namespace${index}`,
        annotations: {
          'alauda.io/display-name': `namespace${index} display name`,
        },
        labels: {
          'alauda.io/cluster': name,
        },
      },
    })),
  })),
);
