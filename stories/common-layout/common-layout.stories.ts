import {
  ApiGatewayInterceptor,
  AuthorizationInterceptorService,
  CommonLayoutContextService,
  CommonLayoutModule,
  TranslateModule,
} from '@alauda/common-snippet';
import { DialogModule, MessageModule } from '@alauda/ui';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { storiesOf } from '@storybook/angular';

import { CustomContextService } from './custom-context.service';
import { DemoComponent } from './demo.component';

const imports = [
  CommonLayoutModule,
  DialogModule,
  HttpClientModule,
  MessageModule,
  TranslateModule.forRoot(),
];

storiesOf('Alauda Common Snippet Library', module).add(
  'all common layout components',
  () => {
    return {
      moduleMetadata: {
        imports,
        declarations: [DemoComponent],
        providers: [
          {
            provide: HTTP_INTERCEPTORS,
            useClass: ApiGatewayInterceptor,
            multi: true,
          },
          {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthorizationInterceptorService,
            multi: true,
          },
          {
            provide: CommonLayoutContextService,
            useClass: CustomContextService,
          },
        ],
      },
      component: DemoComponent,
    };
  },
);
