import {
  AllPermissions,
  K8sPermissionService,
  K8sResourceAction,
  READABLE_ACTIONS,
  WRITABLE_ACTIONS,
  WritablePermissions,
  publishRef,
} from '@alauda/common-snippet';
import { Component, OnInit } from '@angular/core';
import { Observable, interval } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';

import { namespace } from './mock';

export interface MenuDefinition {
  name: string;
  icon: string;
  link: string;
  type?: string;
  allowed?: boolean;
}

export const MENU_DEFINITIONS: MenuDefinition[] = [
  {
    name: 'project_management',
    icon: 'project_s',
    link: 'home/project',
    type: 'projectview',
  },
  {
    name: 'platform_management',
    icon: 'setting_home',
    link: 'manage',
    type: 'platformview',
  },
  {
    name: 'my_profile',
    icon: 'user_s',
    link: 'home/personal-info',
    allowed: true,
  },
];

const CHECK_PERMISSION_TYPES = MENU_DEFINITIONS.reduce<string[]>(
  (acc, { allowed, type }) => {
    if (!allowed) {
      acc.push(type);
    }
    return acc;
  },
  [],
);

@Component({
  template: `
    <button aui-button="primary" [auiDropdown]="menu">Account</button>
    <aui-menu #menu>
      <ng-container *ngFor="let menu of accountMenus$ | async; index as i">
        <a
          *ngIf="menu.allowed"
          [href]="'/console-platform/#/' + menu.link"
          target="_blank"
          rel="nofollow me noopener noreferrer"
        >
          <aui-menu-item>
            <aui-icon [icon]="'basic:' + menu.icon"></aui-icon>
            {{ menu.name }}
          </aui-menu-item>
        </a>
      </ng-container>
    </aui-menu>
    <div>readable: {{ readablePermissions$ | async | json }}</div>
    <div>writable: {{ writablePermissions$ | async | json }}</div>
    <div>all: {{ allPermissions$ | async | json }}</div>
    <div>views: {{ viewsPermission$ | async | json }}</div>
    <div>advanced permission: {{ advancedPermission$ | async | json }}</div>
    <div>can update: {{ canUpdate$ | async }}</div>
    <div>Namespace permissions: {{ namespacePermissions$ | async | json }}</div>
  `,
})
export class DemoComponent implements OnInit {
  constructor(private readonly k8sPermission: K8sPermissionService) {}
  accountMenus$: Observable<MenuDefinition[]> = this.k8sPermission
    .isAllowed({
      type: 'views',
      name: CHECK_PERMISSION_TYPES,
    })
    .pipe(
      map(statuses =>
        MENU_DEFINITIONS.map((menu, index) => ({
          ...menu,
          allowed: menu.allowed || statuses[index],
        })),
      ),
      publishRef(),
    );

  readablePermissions$: Observable<boolean[]>;
  writablePermissions$: Observable<WritablePermissions>;
  allPermissions$: Observable<AllPermissions>;

  viewsPermission$ = this.k8sPermission.getAccess({
    type: 'views',
    // name: 'acp_manageview',
    // action: K8sResourceAction.UPDATE,
  });

  advancedPermission$ = this.k8sPermission.getAccess({
    type: 'views',
    action: K8sResourceAction.GET,
    advanced: true,
  });

  canUpdate$: Observable<boolean>;

  namespacePermissions$ = interval(5000).pipe(
    take(2),
    switchMap(() =>
      this.k8sPermission.isAllowed({
        resource: namespace,
        action: WRITABLE_ACTIONS,
        cluster: 'global',
      }),
    ),
  );

  ngOnInit() {
    this.readablePermissions$ = this.k8sPermission.isAllowed({
      type: 'views',
      action: READABLE_ACTIONS,
      transform: false,
    });

    this.writablePermissions$ = this.k8sPermission.isAllowed({
      type: 'views',
      action: WRITABLE_ACTIONS,
    });

    this.allPermissions$ = this.k8sPermission.isAllowed({
      type: 'views',
      action: K8sResourceAction.ALL,
    });

    this.canUpdate$ = this.k8sPermission.isAllowed({
      type: 'views',
      action: K8sResourceAction.UPDATE,
    });
  }
}
