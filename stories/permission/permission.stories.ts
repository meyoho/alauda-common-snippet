import { storiesOf } from '@storybook/angular';

import { DemoComponent } from './demo.component';
import { DemoModule } from './demo.module';

storiesOf('Alauda Common Snippet Library', module).add(
  'check resource access permission (need proxy and open new page)',
  () => ({
    moduleMetadata: {
      imports: [DemoModule],
      declarations: [DemoComponent],
    },
    component: DemoComponent,
  }),
);
