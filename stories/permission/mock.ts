import { KubernetesResource } from '@alauda/common-snippet';

export const namespace: KubernetesResource = {
  kind: 'Namespace',
  apiVersion: 'v1',
  metadata: {
    name: 'demo',
    selfLink: '/api/v1/namespaces/demo',
    uid: 'c80e6c67-90e5-11e9-a03a-525400ec6688',
    resourceVersion: '6877939',
    creationTimestamp: '2019-06-17T09:53:38Z',
    labels: {
      'alauda.io/project': 'demo',
      'alauda.io/serviceMesh': 'enabled',
      'istio-injection': 'enabled',
    },
    annotations: {
      AnnotationsKeyServiceMeshInjection: 'alauda.io/serviceMesh',
      'alauda.io/serviceMesh': 'true',
      'istio-injection': 'enabled',
    },
    ownerReferences: [
      {
        apiVersion: 'auth.alauda.io/v1',
        kind: 'Project',
        name: 'demo',
        uid: 'c80d2442-90e5-11e9-8ff1-525400cb432d',
        controller: true,
        blockOwnerDeletion: true,
      },
    ],
  },
  spec: {
    finalizers: ['kubernetes'],
  },
  status: {
    phase: 'Active',
  },
};
