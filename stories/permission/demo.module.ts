import {
  ApiGatewayInterceptor,
  AuthorizationInterceptorService,
} from '@alauda/common-snippet';
import { ButtonModule, DropdownModule, IconModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

export const EXPORTABLES = [
  CommonModule,
  HttpClientModule,
  ButtonModule,
  DropdownModule,
  IconModule,
];

@NgModule({
  imports: EXPORTABLES,
  exports: EXPORTABLES,
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiGatewayInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptorService,
      multi: true,
    },
  ],
})
export class DemoModule {}
