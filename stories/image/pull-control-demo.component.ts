import { ImageRepositoryValue } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { MockDataContext } from './mock-data-context';

@Component({
  selector: 'acl-pull-control-demo',
  template: `
    <acl-image-pull-control
      [(ngModel)]="value"
      [context]="pullControlContext"
    ></acl-image-pull-control>
    <label>value is:</label>
    <pre>{{ value | json }}</pre>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PullControlDemoComponent implements OnInit {
  value: ImageRepositoryValue;

  namespace = 'devops-a6-a6-a6-ops';

  pullControlContext = new MockDataContext();

  ngOnInit() {
    this.pullControlContext.namespace = this.namespace;
  }
}
