import { ImageRepositoryValue } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { MockDataContext } from './mock-data-context';

@Component({
  selector: 'acl-push-control-demo',
  template: `
    <acl-image-push-control
      [(ngModel)]="value"
      [context]="pushControlContext"
    ></acl-image-push-control>
    <label>value is:</label>
    <pre>{{ value | json }}</pre>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PushControlDemoComponent implements OnInit {
  value: ImageRepositoryValue;

  namespace = 'devops-a6-a6-a6-ops';

  pushControlContext = new MockDataContext();

  ngOnInit() {
    this.pushControlContext.namespace = this.namespace;
  }
}
