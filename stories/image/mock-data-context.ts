import {
  ImageRepositoryOption,
  ImageSelectorDataContext,
  ScanStatus,
  TagOption,
} from '@alauda/common-snippet';
import placeholder from 'placeholder.js';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { delay, map, pluck, switchMap } from 'rxjs/operators';

import { fakeExportOptions } from './mock-data/mock-export-options';
import {
  ImageData,
  fakeImageProjectData,
  fakeImageRepoData,
} from './mock-data/mock-image-repo-data';
import { fakeSecretData } from './mock-data/mock-secret-data';

export class MockDataContext implements ImageSelectorDataContext {
  set namespace(value: string) {
    this.namespace$.next(value);
  }

  private readonly namespace$ = new BehaviorSubject('');

  canCreateSecret() {
    return of(true).pipe(delay(1000));
  }

  createSecret() {
    return this.namespace$.pipe(
      switchMap(namespace =>
        of({ name: 'fake-secret', namespace }).pipe(delay(1000)),
      ),
    );
  }

  getImageRepositories(mode?: string) {
    return this.namespace$.pipe(
      switchMap(namespace => {
        let fakeData$: Observable<ImageData>;
        if (mode && mode === 'create') {
          fakeData$ = of(fakeImageProjectData);
        } else {
          fakeData$ = of(fakeImageRepoData);
        }
        return fakeData$.pipe(
          delay(1000),
          pluck('result'),
          map(items =>
            items.filter(
              repository => repository.metadata.namespace === namespace,
            ),
          ),
          map(items =>
            items.map(repository => {
              const {
                imageRegistryEndpoint,
                imageRegistryType,
                secretName,
                secretNamespace,
              } = repository.metadata.annotations;
              const tags = (repository.status as {
                tags: Array<{
                  created_at: string;
                  scanStatus: ScanStatus;
                }>;
              }).tags;
              const image = (repository.spec as { image: string }).image;
              const imageSegments = image.split('/');
              const [imageProject, imageName] =
                imageSegments.length > 1 ? imageSegments : [image, ''];

              return {
                name: repository.metadata.name,
                type: imageRegistryType,
                endpoint: imageRegistryEndpoint,
                image,
                repositoryPath: `${imageRegistryEndpoint}/${image}`,
                projectPath: `${imageRegistryEndpoint}/${imageProject}`,
                imageName,
                tags: tags.map(
                  ({ created_at, ...rest }) =>
                    ({
                      ...rest,
                      createdAt: created_at,
                    } as TagOption),
                ),
                secret: {
                  name: secretName,
                  namespace: secretNamespace,
                },
              };
            }),
          ),
        );
      }),
    );
  }

  getTags(repository: ImageRepositoryOption) {
    return of(repository.tags).pipe(delay(1000));
  }

  getSecrets() {
    return this.namespace$.pipe(
      switchMap(namespace =>
        of(fakeSecretData).pipe(
          delay(1000),
          map(data => data.result),
          map(items =>
            items.filter(item => item.metadata.namespace === namespace),
          ),
          map(items =>
            items.map(item => ({
              name: item.metadata.name,
              namespace: item.metadata.namespace,
            })),
          ),
        ),
      ),
    );
  }

  getAssetsIconPath(name: string) {
    return placeholder.getData({ size: '64x64', text: name, color: '#fff' });
  }

  getExportOptions() {
    return this.namespace$.pipe(
      switchMap(() => of(fakeExportOptions).pipe(delay(1000))),
    );
  }
}
