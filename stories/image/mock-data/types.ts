export enum CredentialType {
  BasicAuth = 'kubernetes.io/basic-auth',
  DockerConfig = 'kubernetes.io/dockerconfigjson',
  OAuth2 = 'devops.alauda.io/oauth2',
}

export interface ListResult<T, E = unknown> {
  total: number;
  items: T[];
  errors: E[];
}
