/* eslint-disable sonarjs/no-duplicate-string */
export const fakeSecretData = {
  result: [
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyJoYXJib3IuZGV2c3BhcnJvdy5ob3N0Ijp7InVzZXJuYW1lIjoiYWRtaW4iLCJwYXNzd29yZCI6IkhhcmJvcjEyMzQ1IiwiZW1haWwiOiJteUBlbWFpbC5jb20iLCJhdXRoIjoiWVdSdGFXNDZTR0Z5WW05eU1USXpORFU9In19fQ==',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--dfhj',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'dfhj',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49152887',
        creationTimestamp: '2019-04-08T08:37:01Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'dfhj',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--dfhj',
        uid: '7b1cc88c-59d9-11e9-ad48-52540052ef9b',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyIxMTguMjQuMjAyLjExOjUwMDAiOnsidXNlcm5hbWUiOiJhbGF1ZGEiLCJwYXNzd29yZCI6ImFsYXVkYSIsImVtYWlsIjoibXlAZW1haWwuY29tIiwiYXV0aCI6IllXeGhkV1JoT21Gc1lYVmtZUT09In19fQ==',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--docker-wx',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'docker-wx',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49756431',
        creationTimestamp: '2019-04-10T00:17:58Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'docker-wx',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--docker-wx',
        uid: '18064ad1-5b26-11e9-ad48-52540052ef9b',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyI2Mi4yMzQuMTA3LjU6MzExMDQiOnsidXNlcm5hbWUiOiJhZG1pbiIsInBhc3N3b3JkIjoiSGFyYm9yMTIzNDUiLCJlbWFpbCI6Im15QGVtYWlsLmNvbSIsImF1dGgiOiJZV1J0YVc0NlNHRnlZbTl5TVRJek5EVT0ifX19',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--f1',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'f1',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '40635410',
        creationTimestamp: '2019-03-28T07:20:07Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'f1',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--f1',
        uid: 'ea5bacfe-5129-11e9-aabd-52540052ef9b',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyI2Mi4yMzQuMTA3LjU6MzExMDQiOnsidXNlcm5hbWUiOiJhZG1pbiIsInBhc3N3b3JkIjoiSGFyYm9yMTIzNDUiLCJlbWFpbCI6Im15QGVtYWlsLmNvbSIsImF1dGgiOiJZV1J0YVc0NlNHRnlZbTl5TVRJek5EVT0ifX19',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--f2',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'f2',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '47481768',
        creationTimestamp: '2019-04-02T19:52:32Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'f2',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--f2',
        uid: 'dae79a6c-5580-11e9-8921-525400bbcebe',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyI2Mi4yMzQuMTA3LjU6MzExMDQiOnsidXNlcm5hbWUiOiJhZG1pbiIsInBhc3N3b3JkIjoiSGFyYm9yMTIzNDUiLCJlbWFpbCI6Im15QGVtYWlsLmNvbSIsImF1dGgiOiJZV1J0YVc0NlNHRnlZbTl5TVRJek5EVT0ifX19',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--f3',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'f3',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '40635665',
        creationTimestamp: '2019-03-28T07:20:34Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'f3',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--f3',
        uid: 'f9ff641a-5129-11e9-aabd-52540052ef9b',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyI2Mi4yMzQuMTA3LjU6MzExMDQiOnsidXNlcm5hbWUiOiJhZG1pbiIsInBhc3N3b3JkIjoiSGFyYm9yMTIzNDUiLCJlbWFpbCI6Im15QGVtYWlsLmNvbSIsImF1dGgiOiJZV1J0YVc0NlNHRnlZbTl5TVRJek5EVT0ifX19',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--f5',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'f5',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '40636074',
        creationTimestamp: '2019-03-28T07:21:24Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'f5',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--f5',
        uid: '1818ad3d-512a-11e9-aabd-52540052ef9b',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyI2Mi4yMzQuMTA3LjU6MzExMDQiOnsidXNlcm5hbWUiOiJhZG1pbiIsInBhc3N3b3JkIjoiSGFyYm9yMTIzNDUiLCJlbWFpbCI6Im15QGVtYWlsLmNvbSIsImF1dGgiOiJZV1J0YVc0NlNHRnlZbTl5TVRJek5EVT0ifX19',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--f7',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'f7',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '40636183',
        creationTimestamp: '2019-03-28T07:21:31Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'f7',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--f7',
        uid: '1bf2593a-512a-11e9-aabd-52540052ef9b',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyJoYXJib3IuZGV2c3BhcnJvdy5ob3N0Ijp7InVzZXJuYW1lIjoiYWRtaW4iLCJwYXNzd29yZCI6IkhhcmJvcjEyMzQ1IiwiZW1haWwiOiJteUBlbWFpbC5jb20iLCJhdXRoIjoiWVdSdGFXNDZTR0Z5WW05eU1USXpORFU9In19fQ==',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--harbor',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'harbor',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49143217',
        creationTimestamp: '2019-04-08T07:52:58Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'harbor',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--harbor',
        uid: '53c15571-59d3-11e9-ad48-52540052ef9b',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyJoYXJib3IuZGV2c3BhcnJvdy5ob3N0Ijp7InVzZXJuYW1lIjoiYWRtaW4iLCJwYXNzd29yZCI6IkhhcmJvcjEyMzQ1IiwiZW1haWwiOiJteUBlbWFpbC5jb20iLCJhdXRoIjoiWVdSdGFXNDZTR0Z5WW05eU1USXpORFU9In19fQ==',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--harbor-demo-to-a6',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'harbor-demo-to-a6',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49756968',
        creationTimestamp: '2019-04-10T00:19:51Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'harbor-demo-to-a6',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--harbor-demo-to-a6',
        uid: '5b9376b8-5b26-11e9-ad48-52540052ef9b',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyJoYXJib3IuZGV2c3BhcnJvdy5ob3N0Ijp7InVzZXJuYW1lIjoiYWRtaW4iLCJwYXNzd29yZCI6IkhhcmJvcjEyMzQ1IiwiZW1haWwiOiJteUBlbWFpbC5jb20iLCJhdXRoIjoiWVdSdGFXNDZTR0Z5WW05eU1USXpORFU9In19fQ==',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--harbor-demo-to-a6-two',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'harbor-demo-to-a6-two',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49079945',
        creationTimestamp: '2019-04-08T03:34:46Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'harbor-demo-to-a6-two',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--harbor-demo-to-a6-two',
        uid: '416bcd16-59af-11e9-a411-5254000cec49',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyJoYXJib3IuZGV2c3BhcnJvdy5ob3N0Ijp7InVzZXJuYW1lIjoiYWRtaW4iLCJwYXNzd29yZCI6IkhhcmJvcjEyMzQ1IiwiZW1haWwiOiJteUBlbWFpbC5jb20iLCJhdXRoIjoiWVdSdGFXNDZTR0Z5WW05eU1USXpORFU9In19fQ==',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--mmmm4',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'mmmm4',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '38539651',
        creationTimestamp: '2019-03-22T12:46:50Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'mmmm4',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--mmmm4',
        uid: '8fd91d32-4ca0-11e9-a074-525400bbcebe',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyJoYXJib3IuZGV2c3BhcnJvdy5ob3N0Ijp7InVzZXJuYW1lIjoiYWRtaW4iLCJwYXNzd29yZCI6IkhhcmJvcjEyMzQ1IiwiZW1haWwiOiJteUBlbWFpbC5jb20iLCJhdXRoIjoiWVdSdGFXNDZTR0Z5WW05eU1USXpORFU9In19fQ==',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'dockercfg--devops-a6-a6-a6-ops--test-permission',
        labels: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'test-permission',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49757510',
        creationTimestamp: '2019-04-10T00:20:59Z',
        annotations: {
          'alauda.io/generatorNamespace': 'devops-a6-a6-a6-ops',
          'alauda.io/generatedBy': 'devops-dockersecret-controller',
          'alauda.io/generatorName': 'test-permission',
        },
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg--devops-a6-a6-a6-ops--test-permission',
        uid: '83fbb928-5b26-11e9-ad48-52540052ef9b',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyJoYXJib3IuaGFyYm9yLnNwYXJyb3cuaG9zdCI6eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJIYXJib3IxMjM0NSIsImVtYWlsIjoibXlAZW1haWwuY29tIiwiYXV0aCI6IllXUnRhVzQ2U0dGeVltOXlNVEl6TkRVPSJ9fX0=',
      },
      apiVersion: 'v1',
      metadata: {
        uid: '4e27756c-23a2-11e9-90f9-525400bbcebe',
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '14082327',
        creationTimestamp: '2019-01-29T08:46:01Z',
        selfLink:
          '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/dockercfg-harbor-demo-harbor-acp-sparrow',
        name: 'dockercfg-harbor-demo-harbor-acp-sparrow',
      },
    },
    {
      type: 'kubernetes.io/dockerconfigjson',
      kind: 'Secret',
      data: {
        '.dockerconfigjson':
          'eyJhdXRocyI6eyJpbmRleC5hbGF1ZGEuY24vYWxhdWRhb3JnL2VuaWdtYSI6eyJ1c2VybmFtZSI6ImFsYXVkYW9yZy96cHl1IiwicGFzc3dvcmQiOiIxMl9hbGFfMzQiLCJlbWFpbCI6Inl1enAxOTk2QHFxLmNvbSIsImF1dGgiOiJZV3hoZFdSaGIzSm5MM3B3ZVhVNk1USmZZV3hoWHpNMCJ9fX0=',
      },
      apiVersion: 'v1',
      metadata: {
        name: 'zpyuenigma',
        labels: {
          'alauda.io/subProject': 'a6-a6-ops',
          'alauda.io/project': 'a6',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49821968',
        creationTimestamp: '2019-04-10T04:28:45Z',
        annotations: {
          'alauda.io/subProject': 'a6-a6-ops',
          'alauda.io/project': 'a6',
          'alauda.io/displayName': '',
          'alauda.io/product': 'ACE',
        },
        selfLink: '/api/v1/namespaces/devops-a6-a6-a6-ops/secrets/zpyuenigma',
        uid: '210b2fff-5b49-11e9-a9ca-5254000cec49',
      },
    },
  ],
  code: 200,
};
