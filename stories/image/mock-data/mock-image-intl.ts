import { ImageIntl } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class MockImageIntl extends ImageIntl {
  constructor() {
    super();
    of('en')
      .pipe(delay(2000))
      .subscribe((currentLang: string) => {
        this.changes.next(currentLang);
      });
  }
}
