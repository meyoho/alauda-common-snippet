/* eslint-disable sonarjs/no-duplicate-string */
import {
  KubernetesResource,
  ScanStatus,
  Severity,
} from '@alauda/common-snippet';

export interface ImageData {
  count: number;
  result: KubernetesResource[];
  code: number;
}

export const fakeImageRepoData: ImageData = {
  count: 5,
  result: [
    {
      status: {
        http: {
          delay: 4448853,
          lastAttempt: '2019-04-10T07:49:29Z',
          errorMessage: '',
          response: 'available',
          statusCode: 200,
        },
        tags: [
          {
            scanStatus: '',
            name: 'latest',
            level: 0,
            created_at: '2019-03-12T00:20:17Z',
            message: '',
            digest:
              'cb247087f96d5d93787d789b0603ebee1f1069bfa9346e06479e7b1b20784aa0',
            size: '',
          },
        ],
        lastUpdated: '2019-04-10T07:49:29Z',
        phase: 'Ready',
        latestTag: {
          scanStatus: '',
          name: 'latest',
          level: 0,
          created_at: '2019-03-12T00:20:17Z',
          message: '',
          digest:
            'cb247087f96d5d93787d789b0603ebee1f1069bfa9346e06479e7b1b20784aa0',
          size: '',
        },
      },
      kind: 'ImageRepository',
      spec: {
        image: 'docker.io/ubuntu',
        imageRegistryBinding: {
          name: 'docker-wx',
        },
        imageRegistry: {
          name: 'docker-registry',
        },
      },
      apiVersion: 'devops.alauda.io/v1alpha1',
      metadata: {
        name: 'docker-registry-docker.io-ubuntu',
        labels: {
          'alauda.io/subProject': 'a6-a6-ops',
          'alauda.io/project': 'a6',
          imageRegistryBinding: 'docker-wx',
          imageRegistry: 'docker-registry',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49876436',
        ownerReferences: [
          {
            kind: 'ImageRegistryBinding',
            uid: '07bb923b-5b65-11e9-9f9f-0a580ac701ef',
            apiVersion: 'devops.alauda.io/v1alpha1',
            controller: true,
            blockOwnerDeletion: true,
            name: 'docker-wx',
          },
        ],
        creationTimestamp: '2019-04-10T07:48:54Z',
        annotations: {
          'alauda.io/subProject': 'a6-a6-ops',
          imageRegistryType: 'Docker',
          'alauda.io/product': 'ACE',
          imageRepositoryLink: '',
          secretName: 'dockercfg--devops-a6-a6-a6-ops--docker-wx',
          'alauda.io/project': 'a6',
          imageRegistryEndpoint: '118.24.202.11:5000',
          secretNamespace: 'devops-a6-a6-a6-ops',
        },
        selfLink:
          '/apis/devops.alauda.io/v1alpha1/namespaces/devops-a6-a6-a6-ops/imagerepositories/docker-registry-docker.io-ubuntu',
        uid: '169aa348-5b65-11e9-9f9f-0a580ac701ef',
      },
    },
    {
      status: {
        http: {
          delay: 4345561,
          lastAttempt: '2019-04-10T07:49:29Z',
          errorMessage: '',
          response: 'available',
          statusCode: 200,
        },
        tags: [
          {
            scanStatus: '',
            name: 'latest',
            level: 0,
            created_at: '2018-10-16T06:07:54Z',
            message: '',
            digest:
              'ee1cd36e1085f05bb403d289b8cc615a66a47ad322ca9fe57be8a21f73af4b41',
            size: '',
          },
        ],
        lastUpdated: '2019-04-10T07:49:29Z',
        phase: 'Ready',
        latestTag: {
          scanStatus: '',
          name: 'latest',
          level: 0,
          created_at: '2018-10-16T06:07:54Z',
          message: '',
          digest:
            'ee1cd36e1085f05bb403d289b8cc615a66a47ad322ca9fe57be8a21f73af4b41',
          size: '',
        },
      },
      kind: 'ImageRepository',
      spec: {
        image: 'nginx',
        imageRegistryBinding: {
          name: 'docker-wx',
        },
        imageRegistry: {
          name: 'docker-registry',
        },
      },
      apiVersion: 'devops.alauda.io/v1alpha1',
      metadata: {
        name: 'docker-registry-nginx',
        labels: {
          'alauda.io/subProject': 'a6-a6-ops',
          'alauda.io/project': 'a6',
          imageRegistryBinding: 'docker-wx',
          imageRegistry: 'docker-registry',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49876437',
        ownerReferences: [
          {
            kind: 'ImageRegistryBinding',
            uid: '07bb923b-5b65-11e9-9f9f-0a580ac701ef',
            apiVersion: 'devops.alauda.io/v1alpha1',
            controller: true,
            blockOwnerDeletion: true,
            name: 'docker-wx',
          },
        ],
        creationTimestamp: '2019-04-10T07:48:54Z',
        annotations: {
          'alauda.io/subProject': 'a6-a6-ops',
          imageRegistryType: 'Docker',
          'alauda.io/product': 'ACE',
          imageRepositoryLink: '',
          secretName: 'dockercfg--devops-a6-a6-a6-ops--docker-wx',
          'alauda.io/project': 'a6',
          imageRegistryEndpoint: '118.24.202.11:5000',
          secretNamespace: 'devops-a6-a6-a6-ops',
        },
        selfLink:
          '/apis/devops.alauda.io/v1alpha1/namespaces/devops-a6-a6-a6-ops/imagerepositories/docker-registry-nginx',
        uid: '169c38de-5b65-11e9-9f9f-0a580ac701ef',
      },
    },
    {
      status: {
        http: {
          delay: 190667818,
          lastAttempt: '2019-04-10T09:33:50Z',
          errorMessage: '',
          response: 'available',
          statusCode: 200,
        },
        tags: [
          {
            scanStatus: ScanStatus.Success,
            name: '3.8',
            level: 1,
            created_at: '2018-09-11T22:19:39Z',
            summary: [
              {
                count: 13,
                severity: Severity.Negligible,
              },
            ],
            message: '',
            digest:
              '56c7bee8935f8f6f75ca64ca37bb01e70b2bc7fd1792ba1da9ba7b748c629223',
            size: '2.01MB',
          },
          {
            scanStatus: ScanStatus.Success,
            name: '3.7',
            level: 1,
            created_at: '2018-09-11T22:19:39Z',
            summary: [
              {
                count: 13,
                severity: Severity.Negligible,
              },
            ],
            message: '',
            digest:
              '56c7bee8935f8f6f75ca64ca37bb01e70b2bc7fd1792ba1da9ba7b748c629223',
            size: '2.01MB',
          },
          {
            scanStatus: ScanStatus.Success,
            name: '4.0',
            level: 5,
            created_at: '2017-01-17T00:52:54Z',
            summary: [
              {
                count: 38,
                severity: Severity.High,
              },
              {
                count: 22,
                severity: Severity.Medium,
              },
              {
                count: 125,
                severity: Severity.Negligible,
              },
              {
                count: 13,
                severity: Severity.Low,
              },
              {
                count: 1,
                severity: Severity.Unknown,
              },
            ],
            message: '',
            digest:
              'f841a2abd0422364ec94bb633a56707a38c330179f2bbccebd95f9aff4a36808',
            size: '235.88MB',
          },
          {
            scanStatus: ScanStatus.Success,
            name: '4.1',
            level: 5,
            created_at: '2017-01-17T00:52:54Z',
            summary: [
              {
                count: 38,
                severity: Severity.High,
              },
              {
                count: 22,
                severity: Severity.Medium,
              },
              {
                count: 125,
                severity: Severity.Negligible,
              },
              {
                count: 13,
                severity: Severity.Low,
              },
              {
                count: 1,
                severity: Severity.Unknown,
              },
            ],
            message: '',
            digest:
              'f841a2abd0422364ec94bb633a56707a38c330179f2bbccebd95f9aff4a36808',
            size: '235.88MB',
          },
          {
            scanStatus: ScanStatus.Success,
            name: '4.2',
            level: 5,
            created_at: '2017-01-17T00:52:54Z',
            summary: [
              {
                count: 38,
                severity: Severity.High,
              },
              {
                count: 22,
                severity: Severity.Medium,
              },
              {
                count: 125,
                severity: Severity.Negligible,
              },
              {
                count: 13,
                severity: Severity.Low,
              },
              {
                count: 1,
                severity: Severity.Unknown,
              },
            ],
            message: '',
            digest:
              'f841a2abd0422364ec94bb633a56707a38c330179f2bbccebd95f9aff4a36808',
            size: '235.88MB',
          },
        ],
        lastUpdated: '2019-04-10T09:33:50Z',
        phase: 'Ready',
        latestTag: {
          scanStatus: ScanStatus.Success,
          name: '3.8',
          level: 1,
          created_at: '2018-09-11T22:19:39Z',
          summary: [
            {
              count: 13,
              severity: Severity.Negligible,
            },
          ],
          message: '',
          digest:
            '56c7bee8935f8f6f75ca64ca37bb01e70b2bc7fd1792ba1da9ba7b748c629223',
          size: '2.01MB',
        },
      },
      kind: 'ImageRepository',
      spec: {
        image: 'a6-dddd/alpine',
        imageRegistryBinding: {
          name: 'test-permission',
        },
        imageRegistry: {
          name: 'harbor-demo',
        },
      },
      apiVersion: 'devops.alauda.io/v1alpha1',
      metadata: {
        name: 'harbor-demo-a6-dddd-alpine',
        labels: {
          'alauda.io/subProject': 'a6-a6-ops',
          'alauda.io/project': 'a6',
          imageRegistryBinding: 'test-permission',
          imageRegistry: 'harbor-demo',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49904117',
        ownerReferences: [
          {
            kind: 'ImageRegistryBinding',
            uid: '6b19c491-5b26-11e9-9f9f-0a580ac701ef',
            apiVersion: 'devops.alauda.io/v1alpha1',
            controller: true,
            blockOwnerDeletion: true,
            name: 'test-permission',
          },
        ],
        creationTimestamp: '2019-04-10T07:47:32Z',
        annotations: {
          'alauda.io/subProject': 'a6-a6-ops',
          imageRegistryType: 'Harbor',
          'alauda.io/product': 'ACE',
          imageRepositoryLink:
            'http://harbor.devsparrow.host/harbor/projects/10/repositories/a6-dddd%2falpine',
          secretName: 'dockercfg--devops-a6-a6-a6-ops--test-permission',
          'alauda.io/project': 'a6',
          imageRegistryEndpoint: 'harbor.devsparrow.host',
          secretNamespace: 'devops-a6-a6-a6-ops',
        },
        selfLink:
          '/apis/devops.alauda.io/v1alpha1/namespaces/devops-a6-a6-a6-ops/imagerepositories/harbor-demo-a6-dddd-alpine',
        uid: 'e647b0b6-5b64-11e9-9f9f-0a580ac701ef',
      },
    },
    {
      status: {
        http: {
          delay: 136344929,
          lastAttempt: '2019-04-10T09:33:49Z',
          errorMessage: '',
          response: 'available',
          statusCode: 200,
        },
        tags: [
          {
            scanStatus: ScanStatus.Success,
            name: 'golang',
            level: 1,
            created_at: '2019-03-25T05:36:15Z',
            message: '',
            digest:
              '746fb0fd5fbfaf493ddc88ac5a7bb41bc14d6216ab1c2ea2718ac62b6e537b77',
            size: '2.28MB',
          },
          {
            scanStatus: ScanStatus.Success,
            name: '0.1',
            level: 1,
            created_at: '2019-03-25T05:36:15Z',
            message: '',
            digest:
              '746fb0fd5fbfaf493ddc88ac5a7bb41bc14d6216ab1c2ea2718ac62b6e537b77',
            size: '2.28MB',
          },
        ],
        lastUpdated: '2019-04-10T09:33:50Z',
        phase: 'Ready',
        latestTag: {
          scanStatus: ScanStatus.Success,
          name: 'golang',
          level: 1,
          created_at: '2019-03-25T05:36:15Z',
          message: '',
          digest:
            '746fb0fd5fbfaf493ddc88ac5a7bb41bc14d6216ab1c2ea2718ac62b6e537b77',
          size: '2.28MB',
        },
      },
      kind: 'ImageRepository',
      spec: {
        image: 'a6-dddd/wly',
        imageRegistryBinding: {
          name: 'test-permission',
        },
        imageRegistry: {
          name: 'harbor-demo',
        },
      },
      apiVersion: 'devops.alauda.io/v1alpha1',
      metadata: {
        name: 'harbor-demo-a6-dddd-wly',
        labels: {
          'alauda.io/subProject': 'a6-a6-ops',
          'alauda.io/project': 'a6',
          imageRegistryBinding: 'test-permission',
          imageRegistry: 'harbor-demo',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49904116',
        ownerReferences: [
          {
            kind: 'ImageRegistryBinding',
            uid: '6b19c491-5b26-11e9-9f9f-0a580ac701ef',
            apiVersion: 'devops.alauda.io/v1alpha1',
            controller: true,
            blockOwnerDeletion: true,
            name: 'test-permission',
          },
        ],
        creationTimestamp: '2019-04-10T07:47:33Z',
        annotations: {
          'alauda.io/subProject': 'a6-a6-ops',
          imageRegistryType: 'Harbor',
          'alauda.io/product': 'ACE',
          imageRepositoryLink:
            'http://harbor.devsparrow.host/harbor/projects/10/repositories/a6-dddd%2fwly',
          secretName: 'dockercfg--devops-a6-a6-a6-ops--test-permission',
          'alauda.io/project': 'a6',
          imageRegistryEndpoint: 'harbor.devsparrow.host',
          secretNamespace: 'devops-a6-a6-a6-ops',
        },
        selfLink:
          '/apis/devops.alauda.io/v1alpha1/namespaces/devops-a6-a6-a6-ops/imagerepositories/harbor-demo-a6-dddd-wly',
        uid: 'e65a2a3f-5b64-11e9-9f9f-0a580ac701ef',
      },
    },
    {
      status: {
        http: {
          delay: 142420256,
          lastAttempt: '2019-04-10T08:39:46Z',
          errorMessage: '',
          response: 'available',
          statusCode: 200,
        },
        tags: [
          {
            scanStatus: ScanStatus.Success,
            name: 'latest',
            level: 1,
            created_at: '2018-09-07T19:25:39Z',
            message: '',
            digest:
              '1a6fd470b9ce10849be79e99529a88371dff60c60aab424c077007f6979b4812',
            size: '2.94KB',
          },
        ],
        lastUpdated: '2019-04-10T08:39:47Z',
        phase: 'Ready',
        latestTag: {
          scanStatus: ScanStatus.Success,
          name: 'latest',
          level: 1,
          created_at: '2018-09-07T19:25:39Z',
          message: '',
          digest:
            '1a6fd470b9ce10849be79e99529a88371dff60c60aab424c077007f6979b4812',
          size: '2.94KB',
        },
      },
      kind: 'ImageRepository',
      spec: {
        image: 'a6-gggg/hello-edison',
        imageRegistryBinding: {
          name: 'harbor-demo-to-a6',
        },
        imageRegistry: {
          name: 'harbor-demo',
        },
      },
      apiVersion: 'devops.alauda.io/v1alpha1',
      metadata: {
        name: 'harbor-demo-a6-gggg-hello-edison',
        labels: {
          'alauda.io/subProject': 'a6-a6-ops',
          'alauda.io/project': 'a6',
          imageRegistryBinding: 'harbor-demo-to-a6',
          imageRegistry: 'harbor-demo',
        },
        namespace: 'devops-a6-a6-a6-ops',
        resourceVersion: '49890799',
        ownerReferences: [
          {
            kind: 'ImageRegistryBinding',
            uid: 'f78d47cb-5b64-11e9-9f9f-0a580ac701ef',
            apiVersion: 'devops.alauda.io/v1alpha1',
            controller: true,
            blockOwnerDeletion: true,
            name: 'harbor-demo-to-a6',
          },
        ],
        creationTimestamp: '2019-04-10T07:48:53Z',
        annotations: {
          'alauda.io/subProject': 'a6-a6-ops',
          imageRegistryType: 'Harbor',
          'alauda.io/product': 'ACE',
          imageRepositoryLink:
            'http://harbor.devsparrow.host/harbor/projects/11/repositories/a6-gggg%2fhello-edison',
          secretName: 'dockercfg--devops-a6-a6-a6-ops--harbor-demo-to-a6',
          'alauda.io/project': 'a6',
          imageRegistryEndpoint: 'harbor.devsparrow.host',
          secretNamespace: 'devops-a6-a6-a6-ops',
        },
        selfLink:
          '/apis/devops.alauda.io/v1alpha1/namespaces/devops-a6-a6-a6-ops/imagerepositories/harbor-demo-a6-gggg-hello-edison',
        uid: '161c5ba0-5b65-11e9-9f9f-0a580ac701ef',
      },
    },
  ],
  code: 200,
};

export const fakeImageProjectData = {
  count: 4,
  result: [
    {
      metadata: {
        name: 'harbor-registry-e2e-automation-helloworld',
        namespace: 'devops-a6-a6-a6-ops',
        labels: {
          'alauda.io/project': 'acp-liuzongyao',
          imageRegistry: 'harbor-registry',
          imageRegistryBinding: 'b1',
        },
        annotations: {
          imageRegistryEndpoint: '10.0.128.23:32002',
          imageRegistryType: 'Harbor',
          imageRepositoryLink:
            'http://10.0.128.23:32002/harbor/projects/4/repositories/e2e-automation%2fhelloworld',
          secretName: 'dockercfg--acp-liuzongyao--b1',
          secretNamespace: 'devops-a6-a6-a6-ops',
        },
        creationTimestamp: '2019-07-18T18:43:49Z',
      },
      typeMeta: {
        kind: 'imagerepository',
      },
      spec: {
        imageRegistry: {
          name: 'harbor-registry',
        },
        imageRegistryBinding: {
          name: 'b1',
        },
        image: 'e2e-automation/helloworld',
      },
      status: {
        phase: 'Ready',
        lastUpdated: '2019-07-18T18:43:49Z',
        http: {
          statusCode: 200,
          response: 'available',
          delay: 73368544,
          lastAttempt: '2019-07-18T18:43:49Z',
          errorMessage: '',
        },
        tags: [
          {
            name: 'latest',
            digest:
              '8e73e3ca25803fd0e70eb54ea3578dae1bead7da01ae4c2a7105b303cdbedabb',
            author: '',
            created_at: '2019-05-15T03:27:12Z',
            updated_at: '2019-07-18T00:00:45Z',
            size: '31.57MB',
            level: 1,
            scanStatus: ScanStatus.Success,
            message: '',
            summary: [
              {
                severity: Severity.Negligible,
                count: 37,
              },
            ],
          },
          {
            name: 'latest1',
            digest:
              '899a03e9816e5283edba63d71ea528cd83576b28a7586cf617ce78af5526f209',
            author: '',
            created_at: '2019-03-07T22:19:46Z',
            updated_at: '2019-07-18T00:00:45Z',
            size: '2.11MB',
            level: 1,
            scanStatus: ScanStatus.Success,
            message: '',
            summary: [
              {
                severity: Severity.Negligible,
                count: 13,
              },
            ],
          },
        ],
        latestTag: {
          name: 'latest',
          digest:
            '8e73e3ca25803fd0e70eb54ea3578dae1bead7da01ae4c2a7105b303cdbedabb',
          author: '',
          created_at: '2019-05-15T03:27:12Z',
          updated_at: '2019-07-18T00:00:45Z',
          size: '31.57MB',
          level: 1,
          scanStatus: ScanStatus.Success,
          message: '',
          summary: [
            {
              severity: Severity.Negligible,
              count: 37,
            },
          ],
        },
      },
    },
    {
      metadata: {
        name: 'harbor-registry-hellotest-hello-go',
        namespace: 'devops-a6-a6-a6-ops',
        labels: {
          'alauda.io/project': 'acp-liuzongyao',
          imageRegistry: 'harbor-registry',
          imageRegistryBinding: 'b1',
        },
        annotations: {
          imageRegistryEndpoint: '10.0.128.23:32002',
          imageRegistryType: 'Harbor',
          imageRepositoryLink:
            'http://10.0.128.23:32002/harbor/projects/2/repositories/hellotest%2fhello-go',
          secretName: 'dockercfg--acp-liuzongyao--b1',
          secretNamespace: 'devops-a6-a6-a6-ops',
        },
        creationTimestamp: '2019-07-18T18:45:00Z',
      },
      typeMeta: {
        kind: 'imagerepository',
      },
      spec: {
        imageRegistry: {
          name: 'harbor-registry',
        },
        imageRegistryBinding: {
          name: 'b1',
        },
        image: 'hellotest/hello-go',
      },
      status: {
        phase: 'Ready',
        lastUpdated: '2019-07-18T18:45:01Z',
        http: {
          statusCode: 200,
          response: 'available',
          delay: 72022351,
          lastAttempt: '2019-07-18T18:45:00Z',
          errorMessage: '',
        },
        tags: [
          {
            name: 'v1',
            digest:
              'a510a8be814fc1a388a8ecfb58d3ba34ab9e6a378ca319847686c28c2f6152eb',
            author: '',
            created_at: '2019-07-16T07:55:54Z',
            updated_at: '2019-07-18T00:00:44Z',
            size: '31.58MB',
            level: 1,
            scanStatus: ScanStatus.Success,
            message: '',
            summary: [
              {
                severity: Severity.Negligible,
                count: 37,
              },
            ],
          },
          {
            name: 'latest',
            digest:
              '899a03e9816e5283edba63d71ea528cd83576b28a7586cf617ce78af5526f209',
            author: '',
            created_at: '2019-03-07T22:19:46Z',
            updated_at: '2019-07-18T00:00:45Z',
            size: '2.11MB',
            level: 1,
            scanStatus: ScanStatus.Success,
            message: '',
            summary: [
              {
                severity: Severity.Negligible,
                count: 13,
              },
            ],
          },
        ],
        latestTag: {
          name: 'v1',
          digest:
            'a510a8be814fc1a388a8ecfb58d3ba34ab9e6a378ca319847686c28c2f6152eb',
          author: '',
          created_at: '2019-07-16T07:55:54Z',
          updated_at: '2019-07-18T00:00:44Z',
          size: '31.58MB',
          level: 1,
          scanStatus: ScanStatus.Success,
          message: '',
          summary: [
            {
              severity: Severity.Negligible,
              count: 37,
            },
          ],
        },
      },
    },
    {
      metadata: {
        name: 'project-e2e-automation',
        namespace: 'devops-a6-a6-a6-ops',
        annotations: {
          imageRegistryEndpoint: '10.0.128.23:32002',
          imageRegistryType: 'Harbor',
          imageRepositoryLink: '',
          secretName: 'harbor',
          secretNamespace: 'global-credentials',
        },
        creationTimestamp: '2019-07-18T18:08:43Z',
      },
      typeMeta: {},
      spec: {
        imageRegistry: {},
        imageRegistryBinding: {},
        image: 'e2e-automation',
      },
      status: {
        phase: 'Ready',
        tags: [] as unknown[],
        latestTag: {
          name: '',
          digest: '',
          author: '',
          size: '',
          level: 0,
          scanStatus: '',
          message: '',
        },
      },
    },
    {
      metadata: {
        name: 'project-nodong',
        namespace: 'devops-a6-a6-a6-ops',
        annotations: {
          imageRegistryEndpoint: '10.0.128.23:32002',
          imageRegistryType: 'Harbor',
          imageRepositoryLink: '',
          secretName: 'harbor',
          secretNamespace: 'global-credentials',
        },
        creationTimestamp: '2019-07-18T18:08:43Z',
      },
      typeMeta: {},
      spec: {
        imageRegistry: {},
        imageRegistryBinding: {},
        image: 'nodong',
      },
      status: {
        phase: 'Ready',
        latestTag: {
          name: '',
          digest: '',
          author: '',
          size: '',
          level: 0,
          scanStatus: '',
          message: '',
        },
      },
    },
  ],
  code: 200,
};
