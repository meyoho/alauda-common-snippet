export const fakeExportOptions = [
  {
    name: 'GIT_COMMIT',
    description: {
      'zh-CN': 'Git Commit ID',
      en: 'Git Commit ID',
    },
  },
  {
    name: 'GIT_BRANCH',
    description: {
      'zh-CN': 'Git 分支名称',
      en: 'Git Branch Name',
    },
  },
  {
    name: 'RelativeDirectory',
    description: {
      'zh-CN': 'RelativeDirectory',
      en: 'RelativeDirectory',
    },
  },
  {
    name: 'QUALITY_GATE',
    description: {
      'zh-CN': 'Quality Gate 结果',
      en: 'Quality Gate Result',
    },
  },
  {
    name: 'IMAGE_REPO',
    description: {
      'zh-CN': '镜像仓库',
      en: 'Image Repo',
    },
  },
  {
    name: 'IMAGE_TAG',
    description: {
      'zh-CN': '镜像标签',
      en: 'Image Tag',
    },
  },
];
