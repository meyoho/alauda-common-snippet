import {
  ImageIntl,
  ImageModule,
  TranslateModule,
} from '@alauda/common-snippet';
import { HttpClientModule } from '@angular/common/http';
import { storiesOf } from '@storybook/angular';

import { MockImageIntl } from './mock-data/mock-image-intl';
import { PullControlDemoComponent } from './pull-control-demo.component';
import { PushControlDemoComponent } from './push-control-demo.component';

const imports = [ImageModule, HttpClientModule, TranslateModule.forRoot()];
const props = {
  spaceName: 'spaceName',
  templateName: 'templateName',
  secretCreatePermission: true,
};

storiesOf('Alauda Common Snippet Library', module)
  .add('image pull control', () => ({
    moduleMetadata: {
      imports,
      providers: [{ provide: ImageIntl, useClass: MockImageIntl }],
      declarations: [PullControlDemoComponent],
    },
    template: `
      <acl-pull-control-demo></acl-pull-control-demo>
    `,
    props,
  }))
  .add('image push control', () => ({
    moduleMetadata: {
      imports,
      declarations: [PushControlDemoComponent],
    },
    template: `
      <acl-push-control-demo></acl-push-control-demo>
    `,
    props,
  }));
