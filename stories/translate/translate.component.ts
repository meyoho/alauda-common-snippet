import { TranslateService } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  template: `
    <div>
      {{ translate.locale$ | async }}
      {{ 'nested.params' | translate: { id: 123, name: 'Peter' } }}
      <br />
      <button (click)="toggleLocale()">
        {{ 'toggle_locale' | translate }}
      </button>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TranslateComponent {
  constructor(public readonly translate: TranslateService) {}

  toggleLocale() {
    this.translate.toggleLocale();
  }
}
