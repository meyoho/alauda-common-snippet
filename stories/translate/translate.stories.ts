import { TranslateModule } from '@alauda/common-snippet';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { storiesOf } from '@storybook/angular';

import { TranslateComponent } from './translate.component';

storiesOf('Alauda Common Snippet Library', module).add('Translate', () => ({
  moduleMetadata: {
    imports: [
      CommonModule,
      HttpClientModule,
      TranslateModule.forRoot({
        translations: {
          zh: {
            toggle_locale: '切换区域',
            nested: {
              params: '证件号: {{ id }}, 姓名: {{ name }}',
            },
          },
          en: {
            nested: {
              params: 'ID: {{ id }}, name: {{ name }}',
            },
          },
        },
        loose: true,
      }),
    ],
    declarations: [TranslateComponent],
  },
  component: TranslateComponent,
}));
