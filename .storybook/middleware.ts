import proxyConfig from '@alauda/custom-webpack/lib/proxy.config.json';
import { Router } from 'express';
import proxy from 'http-proxy-middleware';

const entries = Object.entries(proxyConfig);

export default (router: Router) =>
  entries.forEach(([prefix, config]) => {
    router.use(prefix, proxy(config as proxy.Config));
  });
