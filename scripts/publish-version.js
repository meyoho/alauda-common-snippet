let version = process.env.PUBLISH_VERSION || 'patch';

if (version.startsWith('v')) {
  version = version.slice(1);
}

process.stdout.write(version);
