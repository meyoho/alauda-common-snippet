import vm from 'vm';

import axios from 'axios';
import program from 'commander';
import FormData from 'form-data';
import ts from 'typescript';

import { Locale } from '../packages/alauda-common-lib/src/translate/constants';

const { CLIENT_ID, CLIENT_SECRET } = process.env;

if (!CLIENT_ID || !CLIENT_SECRET) {
  throw new Error('Environments CLIENT_ID and CLIENT_SECRET are required');
}

const REPO_API_PREFIX =
  'https://api.bitbucket.org/2.0/repositories/mathildetech/';

const I18N_FILES = [
  'mephisto/src/{hash}/src/app/translate/i18n.ts',
  'diablo-clone/src/{hash}/src/frontend/app/translate/i18n.ts',
  'icarus/src/{hash}/src/app/i18n/{locale}.i18n.ts',
  'underlord/src/{hash}/src/app/translate/i18n-{locale}.ts',
];

const MAX = I18N_FILES.length;

program
  .version('0.1.0')
  .option(
    '-l, --locale <enum>',
    'which locale to be resolved',
    (_locale: string) => {
      const locales = Object.keys(Locale).map(_ => _.toLowerCase());
      _locale = _locale.toLowerCase();
      if (locales.includes(_locale)) {
        return _locale;
      }
      console.error(`Invalid locale option, it should be one of ${locales}`);
      // eslint-disable-next-line no-process-exit, unicorn/no-process-exit
      process.exit(1);
    },
    Locale.EN,
  )
  .option(
    '-m, --min <number>',
    `minimize duplicate times, integer between 2 and ${MAX}`,
    (_min: string) => {
      const n = parseInt(_min, 10);
      if (n > 1 && n < MAX + 1) {
        return n;
      }
      console.error(
        `Invalid min option, it should be an integer between 2 and ${MAX}`,
      );
      // eslint-disable-next-line no-process-exit, unicorn/no-process-exit
      process.exit(1);
    },
    MAX,
  )
  .option(
    '-n, --no-value <boolean>',
    'whether or not print duplicate key together with value',
  )
  .parse(process.argv);

const locale: Locale = program.locale;
const min: number = program.min;
const withValue: boolean = program.value;

const LOCALE_MAPPER: Partial<Record<
  Locale,
  Record<
    string,
    {
      times: number;
      value: string;
    }
  >
>> = {};

const main = async () => {
  const form = new FormData();

  form.append('grant_type', 'client_credentials');
  form.append('scopes', 'repository');

  const {
    data: { access_token: accessToken },
  } = await axios.post(
    'https://bitbucket.org/site/oauth2/access_token',
    form.getBuffer(),
    {
      auth: {
        username: CLIENT_ID,
        password: CLIENT_SECRET,
      },
      headers: form.getHeaders(),
    },
  );

  const config = {
    headers: {
      Authorization: 'Bearer ' + accessToken,
    },
  };

  const translations = await Promise.all(
    I18N_FILES.map(file =>
      axios(REPO_API_PREFIX + file.split('{hash}')[0], {
        ...config,
        maxRedirects: 0,
        validateStatus: status => [301, 302].includes(status),
      })
        .then(
          ({ headers }) =>
            headers.location +
            file.split('{hash}')[1].replace('{locale}', locale),
        )
        .then(url => axios(url, config))
        .then(({ data }) => data)
        .catch(console.error),
    ),
  );

  console.assert(translations.length === I18N_FILES.length);

  translations.forEach(translation => {
    translation = vm.runInNewContext(ts.transpile(translation), {
      exports: {},
    });
    if (typeof translation[locale] === 'object') {
      translation = translation[locale];
    }

    const mapper = LOCALE_MAPPER[locale] || (LOCALE_MAPPER[locale] = {});
    Object.entries(translation).forEach(([key, value]) => {
      if (mapper[key] == null) {
        mapper[key] = {
          times: 0,
          value: value as string,
        };
      }
      mapper[key].times++;
    });
  });

  const duplicates = Object.entries(LOCALE_MAPPER[locale]).reduce<
    Record<string, number>
  >(
    (acc, [key, { times }]) =>
      Object.assign(
        acc,
        times >= min && {
          [key]: times,
        },
      ),
    {},
  );

  const duplicateKeys = Object.keys(duplicates);

  console.warn(
    JSON.stringify(
      withValue
        ? duplicateKeys.reduce<Record<string, string>>(
            (acc, key) =>
              Object.assign(acc, {
                [key]: LOCALE_MAPPER[locale][key].value,
              }),
            {},
          )
        : min === MAX
        ? duplicateKeys
        : duplicates,
      null,
      2,
    ),
  );
};

main();
