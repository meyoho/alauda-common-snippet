import { Translation } from '@alauda/common-snippet';

export const en: Translation = {
  console: 'Console',
  product: 'Product',
  enterProduct: 'Enter product',
  codeTool: 'Code Tool',
  devops: 'Devops',
  monitoring: 'Monitoring',
  log: 'Log',
};
