import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        loadChildren: () =>
          import('./home/home.module').then(M => M.HomeModule),
      },
      {
        path: 'feature-gate',
        loadChildren: () =>
          import('./feature-gate/module').then(M => M.FeatureGateModule),
      },
      {
        path: 'license',
        loadChildren: () =>
          import('./license/module').then(M => M.LicenseModule),
      },
      {
        path: 'license-error',
        loadChildren: () =>
          import('./license-error/module').then(M => M.LicenseErrorPageModule),
      },
    ]),
  ],
})
export class AppRoutingModule {}
