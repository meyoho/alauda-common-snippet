import {
  API_GATEWAY,
  AsyncDataLoader,
  K8sApiService,
  KubernetesResourceList,
  StringMap,
  TranslateKey,
  publishRef,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import dayjs from 'dayjs';
import { safeLoad } from 'js-yaml';
import { map, switchMap } from 'rxjs/operators';
import { Production, ProductionGroup, ProductionSpec } from 'types';

export interface CardSection {
  title: TranslateKey;
  items: ProductionSpec[];
  group?: ProductionGroup;
}

@Component({
  selector: 'acl-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  fallbackLogo = 'logos/placeholder.svg';

  cardSections$ = this.http
    .get('custom/layout.yaml', { responseType: 'text' })
    .pipe(
      switchMap(data =>
        this.http
          .get<KubernetesResourceList<Production>>(
            API_GATEWAY + '/apis/ace3.alauda.io/v1/productions',
          )
          .pipe(
            map(({ items }) => {
              const Groups: Array<{
                name: string;
                title: string;
                type: ProductionGroup;
              }> = safeLoad(safeLoad(data).Groups);
              const groupTitles = Groups.reduce<StringMap>(
                (acc, { name, title }) =>
                  Object.assign(acc, {
                    [name]: title,
                  }),
                {},
              );
              return items.reduce<
                Partial<Record<ProductionGroup, CardSection>>
              >((cardSectionGroup, { spec }) => {
                const { group } = spec;
                if (!cardSectionGroup[group]) {
                  cardSectionGroup[group] = {
                    title: groupTitles[group],
                    items: [],
                    group,
                  };
                }
                cardSectionGroup[group].items.push(spec);
                return cardSectionGroup;
              }, {});
            }),
          ),
      ),
      map(cardSectionGroup =>
        Object.values(cardSectionGroup).map(cardSection => {
          cardSection.items = cardSection.items
            .slice()
            .sort((x, y) => x.index - y.index);
          return cardSection;
        }),
      ),
      publishRef(),
    );

  dataLoader = new AsyncDataLoader({
    fetcher: this.fetchCsp.bind(this),
  });

  time = dayjs();

  constructor(
    public sanitizer: DomSanitizer,
    private readonly http: HttpClient,
    private readonly k8sApi: K8sApiService,
  ) {}

  fetchCsp() {
    return this.k8sApi.watchResource({
      definition: {
        type: 'rules',
        apiGroup: 'crd.alauda.io',
      },
      cluster: 'ovn',
      namespace: 'cpaas-system',
      name: 'alb2-00080-2abbf3b3-9866-4cac-9e06-99a83ff5cb96',
    });
  }
}
