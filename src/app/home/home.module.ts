import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';

import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home.routing.module';

const EXPORTABLE = [HomeComponent];

@NgModule({
  imports: [SharedModule, HomeRoutingModule],
  declarations: EXPORTABLE,
  exports: EXPORTABLE,
})
export class HomeModule {}
