import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LicenseErrorPageComponent } from './component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: LicenseErrorPageComponent,
      },
    ]),
  ],
})
export class LicenseErrorPageRoutingModule {}
