import { LicenseErrorModule, UtilsModule } from '@alauda/common-snippet';
import { NgModule } from '@angular/core';
import { ComponentsModule } from 'shared/components/components.module';
import { SharedModule } from 'shared/shared.module';

import { LicenseErrorPageComponent } from './component';
import { LicenseErrorPageRoutingModule } from './routing.module';

@NgModule({
  imports: [
    SharedModule,
    LicenseErrorModule,
    UtilsModule,
    ComponentsModule,
    LicenseErrorPageRoutingModule,
  ],
  declarations: [LicenseErrorPageComponent],
  entryComponents: [],
})
export class LicenseErrorPageModule {}
