import { Component } from '@angular/core';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  template: `
    <acl-license-error>
      {{ time$ | async | aclStandardDate }}
      <br />
      {{ time$ | async | aclStandardTime }}
      <img class="brand-logo" src="logos/logo.svg" alt="logo" />
      <span class="logo-separator"></span>
      <span class="project-title">{{ 'console' | translate }}</span>
      <div class="flex-1"></div>
      <acl-account-menu></acl-account-menu>
    </acl-license-error>
  `,
  styleUrls: ['./style.scss'],
})
export class LicenseErrorPageComponent {
  time$ = interval(1000).pipe(map(() => Date.now()));
}
