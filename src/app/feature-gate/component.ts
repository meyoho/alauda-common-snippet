import { FeatureGateService } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeatureGateComponent {
  constructor(private readonly featureGate: FeatureGateService) {}

  forceRefetch() {
    this.featureGate.refetchCache();
  }
}
