import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';

import { FeatureGateComponent } from './component';
import { FeatureGateRoutingModule } from './routing.module';
import { FeatureGateTestComponent } from './test.component';

const EXPORTABLE = [FeatureGateComponent, FeatureGateTestComponent];

@NgModule({
  imports: [SharedModule, FeatureGateRoutingModule],
  declarations: EXPORTABLE,
  exports: EXPORTABLE,
})
export class FeatureGateModule {}
