import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FeatureGateComponent } from './component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: FeatureGateComponent,
      },
    ]),
  ],
})
export class FeatureGateRoutingModule {}
