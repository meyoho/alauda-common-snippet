import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'acl-gate-test',
  template: '<div>test</div>',
})
export class FeatureGateTestComponent implements OnInit {
  constructor() {
    console.log('constructor');
  }

  ngOnInit() {
    console.log('ngOnInit');
  }
}
