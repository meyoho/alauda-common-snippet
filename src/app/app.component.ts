import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'acl-app',
  template: '<router-outlet></router-outlet>',
  styles: [
    `
      :host {
        width: 100vw;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {}
