import { CodeEditorModule } from '@alauda/code-editor';
import {
  AccountMenuModule,
  AsyncDataModule,
  NotificationUtilModule,
  PlatformCenterNavModule,
  TranslateModule,
  UtilsModule,
} from '@alauda/common-snippet';
import {
  ButtonModule,
  CardModule,
  DialogModule,
  DropdownModule,
  IconModule,
  MessageModule,
  NotificationModule,
  PageModule,
  SelectModule,
} from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RxDirectivesModule } from '@rxts/ngrx';

import { ComponentsModule } from './components/components.module';

const NG_MODULES = [CommonModule, FormsModule, RouterModule];
const AUI_MODULES = [
  ButtonModule,
  CardModule,
  CodeEditorModule,
  DialogModule,
  IconModule,
  DropdownModule,
  MessageModule,
  NotificationModule,
  PageModule,
  SelectModule,
];
const COMMON_MODULES = [
  NotificationUtilModule,
  TranslateModule,
  AsyncDataModule,
  UtilsModule,
  AccountMenuModule,
  PlatformCenterNavModule,
];
const THIRD_PARTY_MODULES = [RxDirectivesModule];

const EXPORTABLE_MODULES = [
  ...NG_MODULES,
  ...AUI_MODULES,
  ...COMMON_MODULES,
  ...THIRD_PARTY_MODULES,
  ComponentsModule,
];

export const EXPORTABLE = EXPORTABLE_MODULES;

@NgModule({
  imports: EXPORTABLE_MODULES,
  exports: EXPORTABLE,
})
export class SharedModule {}
