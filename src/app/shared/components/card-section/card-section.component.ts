import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'acl-card-section',
  templateUrl: 'card-section.component.html',
  styleUrls: ['card-section.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardSectionComponent {
  @Input()
  title: string;
}
