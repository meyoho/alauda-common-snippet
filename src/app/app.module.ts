import { CodeEditorModule } from '@alauda/code-editor';
import {
  ApiGatewayInterceptor,
  AuthorizationInterceptorService,
  DEFAULT_CODE_EDITOR_OPTIONS,
  DEFAULT_REMOTE_URL,
  LICENSE_PRODUCT_NAME_TOKEN,
  ResourceErrorInterceptor,
  TOKEN_RESOURCE_DEFINITIONS,
  TranslateModule,
  TranslateService,
} from '@alauda/common-snippet';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from 'shared/shared.module';
import { RESOURCE_DEFINITIONS } from 'utils';

import { LicenseProductName } from '../../packages/alauda-common-lib/src/license/license.type';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { en } from './i18n.en';
import { zh } from './i18n.zh';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    AppRoutingModule,
    CodeEditorModule.forRoot({
      baseUrl: 'monaco_lib/v1',
      defaultOptions: DEFAULT_CODE_EDITOR_OPTIONS,
    }),
    TranslateModule.forRoot({
      loose: true,
      remoteUrl: DEFAULT_REMOTE_URL,
      translations: {
        en,
        zh,
      },
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiGatewayInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResourceErrorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptorService,
      multi: true,
    },
    {
      provide: TOKEN_RESOURCE_DEFINITIONS,
      useValue: RESOURCE_DEFINITIONS,
    },
    {
      provide: LICENSE_PRODUCT_NAME_TOKEN,
      useValue: LicenseProductName.CONTAINER_PLATFORM,
    },
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(translate: TranslateService) {
    translate.locale$.subscribe(locale => {
      document.documentElement.setAttribute('lang', locale);
    });
  }
}
