import { createResourceDefinitions } from '@alauda/common-snippet';

const _ = createResourceDefinitions({
  VIEW: {
    type: 'views',
    apiGroup: 'auth.alauda.io',
  },
  CONFIG_MAP: {
    type: 'configmaps',
  },
});

export const { RESOURCE_DEFINITIONS, RESOURCE_TYPES, getYamlApiVersion } = _;

// eslint-disable-next-line @typescript-eslint/no-type-alias
export type ResourceType = keyof typeof RESOURCE_TYPES;
