import { ChangeDetectionStrategy, Component } from '@angular/core';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  template: `
    Container ACP Active
    <br />
    {{ time$ | async | aclStandardDate }}
    <br />
    {{ time$ | async | aclStandardTime }}
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LicenseComponent {
  time$ = interval(1000).pipe(map(() => Date.now()));
}
