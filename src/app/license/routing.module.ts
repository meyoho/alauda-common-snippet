import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LicenseGuardService } from '../../../packages/alauda-common-lib/src/license/license.guard';

import { LicenseComponent } from './component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        canActivate: [LicenseGuardService],
        canActivateChild: [LicenseGuardService],
        path: '',
        component: LicenseComponent,
      },
    ]),
  ],
})
export class LicenseRoutingModule {}
