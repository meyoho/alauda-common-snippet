import { UtilsModule } from '@alauda/common-snippet';
import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';

import { LicenseComponent } from './component';
import { LicenseRoutingModule } from './routing.module';

const EXPORTABLE = [LicenseComponent];

@NgModule({
  imports: [SharedModule, LicenseRoutingModule, UtilsModule],
  declarations: EXPORTABLE,
  exports: EXPORTABLE,
})
export class LicenseModule {}
