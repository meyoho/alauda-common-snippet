import { KubernetesResource, TranslateKey } from '@alauda/common-snippet';

export type ProductionGroup = 'production' | 'others';

export interface ProductionSpec {
  group: ProductionGroup;
  index: number;
  logoURL?: string;
  title: TranslateKey;
  version?: string;
  description?: string;
  openInBlank?: boolean;
  homepage: string;
}

export interface Production extends KubernetesResource {
  kind: 'Production';
  spec: ProductionSpec;
}
