# 错误反馈通知相关功能

## `NotificationUtilService`

手动创建反馈通知，内部服务于 `ResourceErrorInterceptor`

## `ResourceErrorInterceptor`

资源 API 错误处理拦截器

1. 针对 Writable (`['delete', 'patch', 'post', 'put']`) 的请求发生异常时会自动调用 `NotificationUtilService` 创建反馈通知，并可以点击查看 `request` 和 `response` 详情。如果某个 Writable 的请求不希望触发反馈通知，可以通过设置 `header`: `[NOTIFY_ON_ERROR_HEADER]: FALSE` 的方式跳过：

```ts
class A {
  constructor(private readonly http: HttpClient) {}

  fetch() {
    return this.http.post('x', null, {
      headers: {
        [NOTIFY_ON_ERROR_HEADER]: FALSE,
      },
    });
  }
}
```

2. 其他非 Writable 的请求默认异常时不自动创建反馈通知，可以通过设置 `header`: `[NOTIFY_ON_ERROR_HEADER]: TRUE` 的方式通知拦截器也进行反馈通知处理：

   ```ts
   class B {
     constructor(private readonly http: HttpClient) {}

     fetch() {
       return this.http.get('y', {
         headers: {
           [NOTIFY_ON_ERROR_HEADER]: TRUE,
         },
       });
     }
   }
   ```
