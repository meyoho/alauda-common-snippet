import { NgModule } from '@angular/core';

import { CodeModule } from '../code/code.module';
import { TranslateModule } from '../translate/translate.module';

import { FeedbackNotificationComponent } from './feedback-notification/component';

const EXPORTABLE_MODULES = [CodeModule, TranslateModule];

const ENTRY_COMPONENTS = [FeedbackNotificationComponent];

const EXPORTABLE_COMPONENTS = ENTRY_COMPONENTS;

@NgModule({
  imports: EXPORTABLE_MODULES,
  declarations: EXPORTABLE_COMPONENTS,
  exports: [...EXPORTABLE_MODULES, ...EXPORTABLE_COMPONENTS],
  entryComponents: ENTRY_COMPONENTS,
})
export class NotificationUtilModule {}
