import {
  DialogService,
  DialogSize,
  MessageType,
  NotificationConfig,
  NotificationService,
} from '@alauda/ui';
import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';

import { CodeDisplayDialogComponent } from '../code/code-display-dialog/component';
import { Callback } from '../types/public-api';

import {
  FeedbackNotificationComponent,
  FeedbackNotificationProps,
} from './feedback-notification/component';

export type FeedbackNotificationPropsWithEvents = Omit<
  FeedbackNotificationProps,
  'content' | 'primary' | 'secondary'
> &
  Partial<{
    content: string | object;
    summary: string | object;
    primary: string | true;
    secondary: string | true;
    onPrimary: Callback;
    onSecondary: Callback;
  }>;

export type FeedbackNotificationConfig = Omit<NotificationConfig, 'contentRef'>;

@Injectable({
  providedIn: 'root',
})
export class NotificationUtilService {
  constructor(
    private readonly dialog: DialogService,
    private readonly notification: NotificationService,
  ) {}

  viewDetail(json: string | object, title?: string, notificationId?: string) {
    this.dialog.open(CodeDisplayDialogComponent, {
      data: {
        code: this.normalizeJson(json),
        language: 'json',
        title: title || 'view_detail',
      },
      size: DialogSize.Big,
    });
    if (notificationId) {
      this.notification.remove(notificationId);
    }
  }

  createFeedback(
    config: FeedbackNotificationConfig,
    {
      content,
      onPrimary,
      onSecondary,
      pre,
      primary,
      secondary,
      summary,
    }: FeedbackNotificationPropsWithEvents = {},
  ) {
    const { instance } = this.notification.create({
      type: MessageType.Error,
      ...config,
      content: null,
      contentRef: FeedbackNotificationComponent,
    });

    const feedback: FeedbackNotificationComponent =
      instance.childComponentInstance;

    if (primary === true) {
      primary = 'view_detail';
    } else if (secondary === true) {
      secondary = 'view_detail';
    }

    content = this.normalizeJson(content || config.content);
    const viewDetail = this.viewDetail.bind(
      this,
      content,
      config.title,
      instance.uniqueId,
    );

    if (primary && !onPrimary) {
      onPrimary = viewDetail;
    }

    if (onPrimary) {
      feedback.onPrimary.pipe(take(1)).subscribe(onPrimary);
    } else if (secondary && !onSecondary) {
      onSecondary = viewDetail;
    }

    if (onSecondary) {
      feedback.onSecondary.pipe(take(1)).subscribe(onSecondary);
    }

    Object.assign(feedback, {
      content: this.normalizeJson(summary),
      pre,
      primary,
      secondary,
    });
  }

  private normalizeJson(json: string | object) {
    return (
      json && (typeof json === 'string' ? json : JSON.stringify(json, null, 2))
    );
  }
}
