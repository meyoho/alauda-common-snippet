import {
  HttpErrorResponse,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { get } from 'lodash-es';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Locale, TranslateService } from '../translate/public-api';
import { Status } from '../types/public-api';
import {
  EMPTY,
  FALSE,
  NOTIFY_ON_ERROR_HEADER,
  TimeService,
} from '../utils/public-api';

import { DEFAULT_ZH_ERROR, ZH_ERRORS } from './errors-mapper';
import { NotificationUtilService } from './notification-util.service';

export const WRITABLE_METHODS = ['delete', 'patch', 'post', 'put'] as const;

export type WritableMethod = typeof WRITABLE_METHODS[number];

export interface NotificationHttpRequest<T = unknown> extends HttpRequest<T> {
  requestAt?: number;
}

@Injectable()
export class ResourceErrorInterceptor implements HttpInterceptor {
  constructor(
    private readonly injector: Injector,
    private readonly notificationUtil: NotificationUtilService,
    private readonly time: TimeService,
  ) {}

  intercept(req: NotificationHttpRequest, next: HttpHandler) {
    let notifyOnError = this.notifyOnError(req);

    if (notifyOnError || req.headers.has(NOTIFY_ON_ERROR_HEADER)) {
      const notifyOnErrorHeader = req.headers.get(NOTIFY_ON_ERROR_HEADER);
      req = req.clone({
        headers: req.headers.delete(NOTIFY_ON_ERROR_HEADER),
      });
      const skipNotifyOnError =
        notifyOnErrorHeader != null &&
        [EMPTY, FALSE].includes(notifyOnErrorHeader);
      if (skipNotifyOnError) {
        notifyOnError = false;
      } else {
        if (!notifyOnError) {
          notifyOnError = true;
        }
        req.requestAt = Date.now();
      }
    }

    return next.handle(req).pipe(
      catchError((e: HttpErrorResponse) => {
        if (notifyOnError) {
          this.notify(req, e);
        }
        return throwError(this.isK8sErrorStatus(e.error) ? e.error : e);
      }),
    );
  }

  private notifyOnError(req: NotificationHttpRequest) {
    return (
      WRITABLE_METHODS.includes(req.method.toLowerCase() as WritableMethod) &&
      !req.url.endsWith('/v1/selfsubjectaccessreviews')
    );
  }

  private notify(req: NotificationHttpRequest, res: HttpErrorResponse) {
    let reason;
    if (this.isK8sErrorStatus(res.error)) {
      reason = res.error.reason;
      reason =
        this.injector.get(TranslateService).locale === Locale.ZH
          ? ZH_ERRORS[reason] || DEFAULT_ZH_ERROR
          : reason;
    }
    this.notificationUtil.createFeedback(
      {
        title: reason || this.getResErrorMessage(res),
      },
      {
        content: {
          apiVersion: 'v1',
          RequestURI: req.urlWithParams,
          Method: req.method,
          RequestObject: {
            Time: this.time.format(req.requestAt),
            Header: this.getAllHeaders(req.headers),
            Body: req.body,
          },
          ResponseObject: {
            Time: this.time.format(),
            RelativeTime: this.time.distance(req.requestAt),
            StatusCode: res.status,
            Header: this.getAllHeaders(res.headers),
            Body: res.error,
          },
        },
        secondary: true,
      },
    );
  }

  private getResErrorMessage(res: HttpErrorResponse) {
    return typeof res.error === 'string'
      ? res.error
      : (res.statusText === 'OK' && res.error.message) ||
          // compatible with legacy ACE API
          get(res.error, 'errors[0].message') ||
          res.statusText;
  }

  private getAllHeaders(headers: HttpHeaders) {
    return headers
      .keys()
      .reduce<Record<string, string | string[]>>((acc, key) => {
        let values: string | string[] = headers.getAll(key);
        if (Array.isArray(values) && values.length === 1) {
          values = values[1];
        }
        acc = acc || {};
        acc[key] = values;
        return acc;
      }, null);
  }

  private isK8sErrorStatus(e: any): e is Status {
    return e?.metadata && e.apiVersion;
  }
}
