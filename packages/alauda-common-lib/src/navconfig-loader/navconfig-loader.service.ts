import { NavItemConfig, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import * as jsyaml from 'js-yaml';
import { Observable, concat, of, pipe } from 'rxjs';
import { filter, map, shareReplay, switchMap, tap } from 'rxjs/operators';

import { TranslateService } from '../translate/public-api';

import { NavItem } from './types';
import { filterNavTrees, mapNavTrees } from './utils';

export const NAV_CONFIG_LOCAL_STORAGE_KEY = new InjectionToken<string>(
  'nav config local storage key',
);

@Injectable({
  providedIn: 'root',
})
export class NavConfigLoaderService {
  private readonly cachedConfig = new Map<string, Observable<string>>();

  constructor(
    private readonly http: HttpClient,
    private readonly translate: TranslateService,
    private readonly notification: NotificationService,
    @Inject(NAV_CONFIG_LOCAL_STORAGE_KEY)
    @Optional()
    private readonly localStorageKey: string = '',
  ) {}

  loadNavConfig(address: string) {
    if (!this.cachedConfig.has(address)) {
      this.cachedConfig.set(address, this.buildLoadStream(address));
    }
    return this.cachedConfig.get(address);
  }

  parseYaml() {
    return map<string, NavItem[]>((yaml: string) => {
      let data = [];
      try {
        data = jsyaml.safeLoad(yaml);
      } catch (err) {
        console.error(err);
        this.toastParseError();
      }
      return data;
    });
  }

  mapToAuiNav() {
    return pipe(
      map(filterNavTrees(tree => !tree.hidden)),
      switchMap<NavItem[], Observable<NavItemConfig[]>>(items => {
        return this.translate.locale$.pipe(
          map(() => {
            let data: NavItemConfig[] = [];
            try {
              data = mapNavTrees(this.mapToAuiNavItem)(items);
            } catch (err) {
              console.error(err);
              this.toastParseError();
            }
            return data;
          }),
        );
      }),
    );
  }

  private buildLoadStream(path: string): Observable<string> {
    return concat(
      of(this.getLocal(path)),
      this.http.get(path, { responseType: 'text' }).pipe(
        tap(config => {
          this.setLocal(path, config);
        }),
      ),
    ).pipe(
      filter(config => !!config),
      shareReplay(1),
    );
  }

  private readonly mapToAuiNavItem = (
    item: NavItem,
    parents: NavItem[],
  ): NavItemConfig => {
    return {
      ...item,
      children: null,
      key: parents
        .map(({ name }) => name)
        .concat(item.name)
        .join('/'),
      label: item.label || this.translate.get(item.name),
    };
  };

  private toastParseError() {
    this.notification.error(this.translate.get('nav_config_error'));
  }

  private localKey(path: string) {
    const nodes = path.split('/');
    return `${this.localStorageKey}.${nodes[nodes.length - 1]}`;
  }

  private setLocal(path: string, value: string) {
    if (this.localStorageKey) {
      localStorage.setItem(this.localKey(path), value);
    }
  }

  private getLocal(path: string): string {
    if (this.localStorageKey) {
      return localStorage.getItem(this.localKey(path)) || '';
    } else {
      return '';
    }
  }
}
