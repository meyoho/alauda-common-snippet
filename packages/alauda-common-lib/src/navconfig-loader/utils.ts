import {
  filterTrees,
  findPath,
  firstPath,
  mapTrees,
  reduceTrees,
} from '../utils/tree';

export const reduceNavTrees = reduceTrees('children');
export const mapNavTrees = mapTrees('children');
export const filterNavTrees = filterTrees('children');
export const findNavPath = findPath('children');
export const firstNavPath = firstPath('children');
