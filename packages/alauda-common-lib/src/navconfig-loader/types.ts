export interface NavItem {
  name: string;
  icon?: string;
  label?: string;
  href?: string;
  hidden?: boolean;
  children?: NavItem[];
  [key: string]: any;
}

export interface NavGroup {
  title?: string;
  items: NavItem[];
}
