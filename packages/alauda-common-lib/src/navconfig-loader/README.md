# NavConfigLoader

[导航配置格式](http://confluence.alauda.cn/pages/viewpage.action?pageId=50832478)

## NavConfigLoaderService

## 注入配置

| 名称                         | 类型   | 必填 | 说明                                             |
| ---------------------------- | ------ | ---- | ------------------------------------------------ |
| NAV_CONFIG_LOCAL_STORAGE_KEY | string | ✘    | 在 localStorage 中存储的 key，如果不填将不会存储 |

### 接口

| 名称          | 类型                           | 说明                                                                           |
| ------------- | ------------------------------ | ------------------------------------------------------------------------------ |
| loadNavConfig | (string) => Observable<string> | 从指定地址加载配置文本                                                         |
| parseYaml     | RxOperatorFunction             | 自定义 Rx 操作符，解析 yaml 文本，并在失败时弹出提醒                           |
| mapToAuiNav   | RxOperatorFunction             | 自定义 Rx 操作符，将导航栏配置转换为 AuiPlatformNav 的配置格式，失败时弹出提醒 |

## 工具函数

| 名称           | 说明                                   |
| -------------- | -------------------------------------- |
| reduceNavTrees | 对树形结构执行 reduce 操作             |
| mapNavTrees    | 对树形结构执行 map 操作                |
| filterNavTrees | 对树形结构执行过滤操作                 |
| findNavPath    | 在树中查找到达满足指定条件的节点的路径 |
| firstNavPath   | 返回树中第一条路径                     |
