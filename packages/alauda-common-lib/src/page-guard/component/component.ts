import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  Input,
  TemplateRef,
} from '@angular/core';
import { isBoolean } from 'lodash-es';

import {
  ContentDirective,
  DescriptionDirective,
  OperationDirective,
} from './helper-directives';
import {
  DEFAULT_REASON,
  GuardStatus,
  REASON_MAP,
  Reason,
} from './page-guard.type';
@Component({
  selector: 'acl-page-guard',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageGuardComponent {
  @Input()
  get allowed() {
    return this.status === GuardStatus.Available;
  }

  set allowed(allowed: boolean) {
    if (isBoolean(allowed)) {
      this.status = allowed ? GuardStatus.Available : GuardStatus.Unavailable;
    } else {
      this.status = GuardStatus.Pending;
    }
  }

  @Input()
  status: GuardStatus = GuardStatus.Pending;

  @Input()
  set reason(reason: Reason) {
    if (reason) {
      this.reasonModel = REASON_MAP[reason] || DEFAULT_REASON;
    }
  }

  @Input() title = '';

  @Input() description: string;

  @ContentChild(DescriptionDirective, { read: TemplateRef })
  descriptionTpl: TemplateRef<unknown>;

  @ContentChild(OperationDirective, { read: TemplateRef })
  operation: TemplateRef<unknown>;

  @ContentChild(ContentDirective, { read: TemplateRef })
  template: TemplateRef<unknown>;

  reasonModel = DEFAULT_REASON;

  isNonEmptyString(value: {}): boolean {
    return typeof value === 'string' && value !== '';
  }

  isUndefined(value: string): boolean {
    return typeof value === 'undefined';
  }

  isTemplateRef(value: {}): boolean {
    return value instanceof TemplateRef;
  }
}
