/* eslint-disable @typescript-eslint/no-extraneous-class */
import { Directive } from '@angular/core';
@Directive({
  selector: '[aclPageGuardOperation]',
})
export class OperationDirective {}

@Directive({
  selector: '[aclPageGuardDescription]',
})
export class DescriptionDirective {}

@Directive({
  selector: '[aclPageGuardContent]',
})
export class ContentDirective {}
