export enum GuardStatus {
  Pending = 'pending',
  Available = 'available',
  Unavailable = 'unavailable',
}
export enum Reason {
  NotSupported = 'notSupported',
  NoPermission = 'noPermission',
  NotDeployed = 'notDeployed',
}
export const REASON_MAP = {
  [Reason.NotSupported]: {
    src: 'images/not-supported.svg',
    title: 'not_support',
    description: 'not_support_description',
  },
  [Reason.NoPermission]: {
    src: 'images/no-permission.svg',
    title: 'inaccessible',
    description: 'no_permission_description',
  },
  [Reason.NotDeployed]: {
    src: 'images/not-deployed.svg',
    title: 'inaccessible',
    description: 'not_deployed_description',
  },
};

export const DEFAULT_REASON = REASON_MAP[Reason.NotSupported];
