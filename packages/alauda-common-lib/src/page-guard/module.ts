import { CardModule, IconModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '../translate/public-api';

import { PageGuardComponent } from './component/component';
import {
  ContentDirective,
  DescriptionDirective,
  OperationDirective,
} from './component/helper-directives';
@NgModule({
  imports: [CommonModule, IconModule, CardModule, TranslateModule],
  declarations: [
    PageGuardComponent,
    ContentDirective,
    DescriptionDirective,
    OperationDirective,
  ],
  exports: [
    PageGuardComponent,
    ContentDirective,
    DescriptionDirective,
    OperationDirective,
  ],
})
export class PageGuardModule {}
