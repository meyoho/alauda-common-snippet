import { NavItemConfig } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { K8sPermissionService } from '../../permission/public-api';
import { publishRef } from '../../utils/operators';

export interface PlatformCenterNav extends NavItemConfig {
  allowed?: boolean;
}

const VIEW = {
  type: 'views',
  apiGroup: 'auth.alauda.io',
};

const NAVS: PlatformCenterNav[] = [
  {
    label: 'platform_management',
    key: 'platformview',
    icon: 'basic:setting_home',
    href: '/manage-platform',
  },
  {
    label: 'project_management',
    key: 'projectview',
    icon: 'basic:project_management_s',
    href: '/home/project',
    children: [
      {
        label: 'project_management',
        key: 'projectview',
        href: '/manage-project/project',
      },
    ],
  },
  {
    label: 'maintenance_center',
    key: 'maintenanceview',
    icon: 'basic:operation_center',
    href: '/maintenance-center',
  },
];

@Component({
  selector: 'acl-platform-center-nav',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlatformCenterNavComponent {
  @Input()
  platformPath = '/console-platform';

  menus$: Observable<PlatformCenterNav[]> = this.k8sPermission
    .isAllowed({
      type: VIEW,
      name: NAVS.map(item => item.key),
    })
    .pipe(
      map(statuses =>
        NAVS.map((menu, index) => ({
          ...menu,
          allowed: menu.allowed || statuses[index],
        })),
      ),
      publishRef(),
    );

  constructor(
    private readonly router: Router,
    private readonly k8sPermission: K8sPermissionService,
  ) {}

  hasPermission(menus: PlatformCenterNav[]) {
    return menus.some(item => item.allowed);
  }

  isActive = (item: NavItemConfig) => {
    return (
      (!this.platformPath && this.router.url.startsWith(item.href)) ||
      item.children?.some(nav => this.router.url.startsWith(nav.href))
    );
  };

  navigateToLink(href: string) {
    if (!this.platformPath) {
      this.router.navigate([href]);
    } else {
      window.open(`${this.platformPath}${href}`, '_parent');
    }
  }
}
