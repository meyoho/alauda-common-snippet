# Async Data

## Async Data Loader

### 选项

| 名称     | 类型       | 必填 | 说明                                      |
| -------- | ---------- | ---- | ----------------------------------------- |
| fetcher  | function   | ✔︎   | 接受参数并返回数据流的函数，reload 时调用 |
| params\$ | observable | ✘    | 传给 fecher 函数的参数流                  |
| params   | object     | ✘    | 传给 fecher 函数的参数                    |

### 接口

| 名称      | 类型       | 说明                                           |
| --------- | ---------- | ---------------------------------------------- |
| reload    | function   | 刷新数据，接受一个参数作为起始数据             |
| mapData   | function   | 转换数据，接受一个参数作为 map 函数            |
| data\$    | observable | 数据流                                         |
| loading\$ | observable | 当前加载状态                                   |
| error\$   | observable | 当前错误                                       |
| snapshot  | object     | 状态快照，包含 data, params, loading, error 等 |

## Page State

### Input

| 名称         | 类型             |
| ------------ | ---------------- |
| adapter      | DataStateAdapter |
| autoRedirect | boolean          |

### Output

| 名称     | 类型         |
| -------- | ------------ |
| redirect | EventEmitter |

## Error Page

### Input

| 名称         | 类型    |
| ------------ | ------- |
| errorCode    | number  |
| autoRedirect | boolean |

### Output

| 名称     | 类型         |
| -------- | ------------ |
| redirect | EventEmitter |
