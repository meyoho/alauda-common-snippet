import { get, isEqual } from 'lodash-es';
import { Observable, Subject, combineLatest, interval, merge, of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  filter,
  map,
  scan,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { publishRef } from '../utils/public-api';

import {
  DataError,
  DataMapper,
  DataStateAdapter,
  Fetcher,
  LoaderConfig,
  LoaderConfigWithInterval,
} from './types';

export class AsyncDataLoader<D = unknown, P = { [key: string]: string }>
  implements DataStateAdapter<D> {
  private readonly reloadAction$$ = new Subject<void | D>();
  private readonly dataMapper$$ = new Subject<DataMapper<D>>();

  snapshot: { data: D; params: P; loading: boolean; error: DataError } = {
    params: null,
    data: null,
    loading: false,
    error: null,
  };

  loadState$: Observable<{
    data?: D;
    loading?: boolean;
    error?: DataError;
  }>;

  data$: Observable<D>;
  loading$: Observable<boolean>;
  error$: Observable<DataError>;

  constructor(config: LoaderConfig<D, P> | LoaderConfigWithInterval<D, P>) {
    const { params$, params } = config;

    const queryParams$ = params$ || of(params || null);

    this.loadState$ = combineLatest([
      queryParams$,
      this.reloadAction$$.pipe(startWith(null as D)),
    ]).pipe(
      switchMap(([queryParams, startData]) => {
        this.snapshot.params = queryParams;
        return this.buildFetcher(config)(queryParams).pipe(
          map(data => ({ data })),
          catchError((error: DataError) => {
            return of({ error });
          }),
          startWith({
            data: startData || null,
            loading: true,
          }),
        );
      }),
      map(state => ({
        data: null,
        loading: false,
        error: null,
        ...state,
      })),
      publishRef(),
    );

    this.data$ = merge(
      this.loadState$.pipe(
        map(state => (prev: D) => (state.loading ? prev : state.data)),
      ),
      this.dataMapper$$,
    ).pipe(
      scan<DataMapper<D>, D>((acc, mapper) => mapper(acc), null),
      tap(data => {
        this.snapshot.data = data;
      }),
      publishRef(),
    );

    this.loading$ = this.loadState$.pipe(
      map(state => state.loading),
      distinctUntilChanged(),
      tap(loading => {
        this.snapshot.loading = loading;
      }),
      publishRef(),
    );

    this.error$ = this.loadState$.pipe(
      map(state => state.error),
      distinctUntilChanged(),
      tap(error => {
        this.snapshot.error = error;
      }),
      publishRef(),
    );
  }

  reload(data?: D) {
    this.reloadAction$$.next(data);
  }

  mapData(mapper: DataMapper<D>) {
    this.dataMapper$$.next(mapper);
  }

  private buildFetcher(
    config: LoaderConfig<D, P> | LoaderConfigWithInterval<D, P>,
  ): Fetcher<D, P> {
    const {
      fetcher,
      interval: period,
      intervalFilter,
      compare,
    } = config as LoaderConfigWithInterval<D, P>;

    if (!period) {
      return fetcher;
    }

    const compareFn = Array.isArray(compare)
      ? (a: D, b: D) => isEqual(get(a, compare), get(b, compare))
      : compare;

    return params =>
      interval(period).pipe(
        filter(intervalFilter ?? (() => true)),
        startWith(0),
        switchMap(() => fetcher(params)),
        distinctUntilChanged(compareFn),
      );
  }
}
