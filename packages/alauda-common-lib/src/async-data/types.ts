import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Status } from '../types/k8s';

export interface DataStateAdapter<D> {
  data$: Observable<D>;
  loading$: Observable<boolean>;
  error$: Observable<DataError>;
  reload: () => void;
}

export type Fetcher<D, P> = (params: P) => Observable<D>;

export interface LoaderConfig<D, P> {
  fetcher: Fetcher<D, P>;
  params$?: Observable<P>;
  params?: P;
}

export interface LoaderConfigWithInterval<
  D = unknown,
  P = { [key: string]: string }
> extends LoaderConfig<D, P> {
  interval: number;
  intervalFilter?: (index: number) => boolean;
  compare: string[] | ((a: D, b: D) => boolean);
}

export type DataError = Partial<HttpErrorResponse & Status>;

export type DataMapper<D> = (data: D) => D;
