import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'acl-error-state',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorStateComponent {
  @Input() errorCode: number;
  @Input() autoRedirect: boolean;

  @Output()
  reload = new EventEmitter();

  @Output()
  redirect = new EventEmitter();

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
  ) {}

  backToList() {
    this.redirect.emit();
    if (this.autoRedirect) {
      this.router.navigate(['..'], {
        relativeTo: this.route,
      });
    }
  }
}
