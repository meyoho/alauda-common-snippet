import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { Observable, combineLatest, merge } from 'rxjs';
import { filter, map, mapTo } from 'rxjs/operators';

import { publishRef } from '../../utils/public-api';
import { DataStateAdapter } from '../types';

@Component({
  selector: 'acl-page-state',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageStateComponent implements OnInit {
  @Input() adapter: DataStateAdapter<{}>;
  @Input() autoRedirect = true;
  @Output()
  redirect = new EventEmitter();

  @ContentChild(TemplateRef, { static: true })
  template: TemplateRef<unknown>;

  context$: Observable<unknown>;
  errorCode$: Observable<number>;

  ngOnInit() {
    this.context$ = this.adapter.data$.pipe(
      map(data => {
        return { $implicit: data, ...data };
      }),
      publishRef(),
    );

    this.errorCode$ = merge(
      combineLatest([this.adapter.data$, this.adapter.loading$]).pipe(
        filter(([data, loading]) => !data && !loading),
        mapTo(404),
      ),
      this.adapter.error$.pipe(
        filter(error => !!error),
        map(error => {
          if (typeof error.code === 'number') {
            return error.code;
          }
          if (typeof error.status === 'number') {
            return error.status;
          }
          return 0;
        }),
      ),
    ).pipe(publishRef());
  }
}
