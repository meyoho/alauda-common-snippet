import { ButtonModule, IconModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '../translate/public-api';

import { ErrorStateComponent } from './error-page/component';
import { PageStateComponent } from './page-state/component';

@NgModule({
  imports: [CommonModule, ButtonModule, IconModule, TranslateModule],
  declarations: [PageStateComponent, ErrorStateComponent],
  exports: [PageStateComponent],
})
export class AsyncDataModule {}
