import { get } from 'lodash-es';
import { pipe, pipeFromArray } from 'rxjs/internal/util/pipe';
import { map, startWith } from 'rxjs/operators';

import {
  Arrayable,
  IfEqual,
  K8sResourceAction,
  SelfSubjectAccessReview,
  UnaryObservableFunction,
} from '../types/public-api';
import { publishRef } from '../utils/public-api';

import { ALL_ACTIONS } from './types';

export const isAdmin: UnaryObservableFunction<
  { ext?: { admin: boolean } },
  boolean
> = pipe(
  map(payload => !!get(payload, 'ext.admin')),
  publishRef(),
);

export function isAllowed<
  T extends Arrayable<SelfSubjectAccessReview>,
  K extends boolean = true
>(
  transform?: K,
): UnaryObservableFunction<
  T,
  IfEqual<
    K,
    false,
    T extends SelfSubjectAccessReview | Readonly<SelfSubjectAccessReview>
      ? boolean
      : boolean[],
    Record<
      T extends Arrayable<SelfSubjectAccessReview<infer R>> ? R : never,
      boolean
    >
  >
>;
export function isAllowed(transform = true) {
  const operators = [
    map((access: Arrayable<SelfSubjectAccessReview, never>) => {
      if (!transform) {
        return Array.isArray(access)
          ? access.map(acc => acc.status.allowed)
          : access.status.allowed;
      }
      return (Array.isArray(access) ? access : [access]).reduce<
        Partial<Record<K8sResourceAction, boolean>>
      >((acc, item) => {
        const { verb } = item.spec.resourceAttributes;
        const { allowed } = item.status;
        if (verb === K8sResourceAction.ALL) {
          ALL_ACTIONS.forEach(action => (acc[action] = allowed));
          // @ts-ignore
          acc.all = allowed;
        } else {
          acc[verb] = allowed;
        }
        return acc;
      }, {});
    }),
    publishRef(),
  ];
  if (transform) {
    operators.splice(1, 0, startWith({} as any));
  }
  return pipeFromArray(operators);
}
