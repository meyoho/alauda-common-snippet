# K8s 资源操作权限 API

## Services

### `K8sPermissionService`

- `#getAccess`: 获取对 K8s 资源的操作权限，一般和 `isAllowed` 操作符配合使用
- `#isAllowed`: 根据已经获取到的操作权限对象或列表转化为更方便使用的权限 record 或数组

## Helpers

### `isAllowed`

自定义 `rxjs` 操作符，辅助将已获取权限对象或列表转化为更方便使用的权限 record 或数组，当时直接使用上述 `getAccess` 方法时需要此操作符来辅助转化权限使用
