import { Inject, Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { tap } from 'rxjs/operators';

import { AuthorizationStateService } from '../authorization/public-api';
import { TOKEN_NON_ADMIN_REDIRECT_PATH } from '../utils/public-api';

import { isAdmin } from './helpers';

@Injectable({ providedIn: 'root' })
export class AdminGuardService implements CanActivate, CanActivateChild {
  isAdmin$ = isAdmin(this.auth.getTokenPayload());

  constructor(
    private readonly router: Router,
    private readonly auth: AuthorizationStateService,
    @Inject(TOKEN_NON_ADMIN_REDIRECT_PATH)
    private readonly nonAdminRedirectPath: string,
  ) {}

  canActivate() {
    return this.isAdmin$.pipe(
      tap(admin => {
        if (!admin && this.nonAdminRedirectPath) {
          this.router.navigateByUrl(this.nonAdminRedirectPath, {
            replaceUrl: true,
          });
        }
      }),
    );
  }

  canActivateChild() {
    return this.canActivate();
  }
}
