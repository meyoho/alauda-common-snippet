import { K8sResourceDefinition } from '../api/public-api';
import {
  AnyArray,
  K8sResourceAction,
  KubernetesResource,
} from '../types/public-api';

export const READABLE_ACTIONS = [
  K8sResourceAction.GET,
  K8sResourceAction.LIST,
  K8sResourceAction.PROXY,
  K8sResourceAction.REDIRECT,
  K8sResourceAction.WATCH,
] as const;

export const WRITABLE_ACTIONS = [
  K8sResourceAction.CREATE,
  K8sResourceAction.DELETE,
  K8sResourceAction.DELETE_COLLECTION,
  K8sResourceAction.PATCH,
  K8sResourceAction.UPDATE,
] as const;

export const COMMON_READABLE_ACTIONS = [K8sResourceAction.GET] as const;

export const COMMON_WRITABLE_ACTIONS = [
  K8sResourceAction.CREATE,
  K8sResourceAction.DELETE,
  K8sResourceAction.UPDATE,
] as const;

export const ALL_ACTIONS = [
  K8sResourceAction.CREATE,
  K8sResourceAction.DELETE,
  K8sResourceAction.DELETE_COLLECTION,
  K8sResourceAction.GET,
  K8sResourceAction.LIST,
  K8sResourceAction.PATCH,
  K8sResourceAction.PROXY,
  K8sResourceAction.REDIRECT,
  K8sResourceAction.UPDATE,
  K8sResourceAction.WATCH,
] as const;

export type ReadablePermissions = Readonly<
  Record<typeof READABLE_ACTIONS[number], boolean>
>;

export type WritablePermissions = Readonly<
  Record<typeof WRITABLE_ACTIONS[number], boolean>
>;

export type CommonReadablePermissions = Readonly<
  Record<typeof COMMON_READABLE_ACTIONS[number], boolean>
>;

export type CommonWritablePermissions = Readonly<
  Record<typeof COMMON_WRITABLE_ACTIONS[number], boolean>
>;

export type AllPermissions = Readonly<
  Record<typeof ALL_ACTIONS[number] | 'all', boolean>
>;

export interface K8sPermissionBaseParams {
  advanced?: boolean;
  project?: string;
}

export interface K8sTypePermissionBaseParams extends K8sPermissionBaseParams {
  type: string | K8sResourceDefinition;
  cluster?: string;
  namespace?: string;
}

export type K8sTypePermissionNoneParams = K8sTypePermissionBaseParams & {
  action?: null;
  name?: null;
};

export type K8sTypePermissionSingleParams<
  T extends K8sResourceAction
> = K8sTypePermissionBaseParams &
  (
    | {
        action: T;
        name?: string;
      }
    | {
        action?: T;
        name: string;
      }
  );

export interface K8sTypePermissionActionsParams<T extends K8sResourceAction>
  extends K8sTypePermissionBaseParams {
  action: AnyArray<T>;
  name?: string;
}

export interface K8sTypePermissionNamesParams<T extends K8sResourceAction>
  extends K8sTypePermissionBaseParams {
  action?: T;
  name: AnyArray<string>;
}

export type K8sTypePermissionMultiParams<T extends K8sResourceAction> =
  | K8sTypePermissionActionsParams<T>
  | K8sTypePermissionNamesParams<T>;

export interface K8sTypePermissionMatrixParams<T extends K8sResourceAction>
  extends K8sTypePermissionBaseParams {
  action: AnyArray<T>;
  name: AnyArray<string>;
}

export type K8sTypePermissionParams<T extends K8sResourceAction> =
  | K8sTypePermissionNoneParams
  | K8sTypePermissionSingleParams<T>
  | K8sTypePermissionMultiParams<T>
  | K8sTypePermissionMatrixParams<T>;

export interface K8sPermissionTransformable<T extends boolean> {
  transform: T;
}

export type K8sTypePermissionWithTransformParams<
  T extends K8sResourceAction,
  K extends boolean
> = K8sTypePermissionParams<T> & Partial<K8sPermissionTransformable<K>>;

export interface K8sResourcePermissionBaseParams<T extends KubernetesResource>
  extends K8sPermissionBaseParams {
  cluster: string;
  resource: T;
}

export interface K8sResourcePermissionNoneParams<T extends KubernetesResource>
  extends K8sResourcePermissionBaseParams<T> {
  action?: null;
}

export interface K8sResourcePermissionSingleParams<
  T extends KubernetesResource,
  K extends K8sResourceAction
> extends K8sResourcePermissionBaseParams<T> {
  action: K;
}

export interface K8sResourcePermissionMultiParams<
  T extends KubernetesResource,
  K extends K8sResourceAction
> extends K8sResourcePermissionBaseParams<T> {
  action: AnyArray<K>;
}

export type K8sResourcePermissionParams<
  T extends KubernetesResource,
  K extends K8sResourceAction
> =
  | K8sResourcePermissionNoneParams<T>
  | K8sResourcePermissionSingleParams<T, K>
  | K8sResourcePermissionMultiParams<T, K>;

export type K8sResourcePermissionWithTransformParams<
  T extends KubernetesResource,
  K extends K8sResourceAction,
  R extends boolean
> = K8sResourcePermissionParams<T, K> & Partial<K8sPermissionTransformable<R>>;
