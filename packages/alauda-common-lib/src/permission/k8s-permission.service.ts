import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, forkJoin, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { K8sApiResourceService, getApiPrefixParts } from '../api/public-api';
import {
  IfEqual,
  K8sResourceAction,
  KubernetesResource,
  SelfSubjectAccessReview,
} from '../types/public-api';
import { API_GATEWAY, ifExist, publishRef } from '../utils/public-api';

import { isAllowed } from './helpers';
import {
  AllPermissions,
  K8sPermissionTransformable,
  K8sResourcePermissionMultiParams,
  K8sResourcePermissionNoneParams,
  K8sResourcePermissionParams,
  K8sResourcePermissionSingleParams,
  K8sResourcePermissionWithTransformParams,
  K8sTypePermissionActionsParams,
  K8sTypePermissionMatrixParams,
  K8sTypePermissionMultiParams,
  K8sTypePermissionNamesParams,
  K8sTypePermissionNoneParams,
  K8sTypePermissionParams,
  K8sTypePermissionSingleParams,
  K8sTypePermissionWithTransformParams,
} from './types';

@Injectable({
  providedIn: 'root',
})
export class K8sPermissionService {
  constructor(
    private readonly http: HttpClient,
    private readonly k8sApiResource: K8sApiResourceService,
  ) {
    this.getAccess = this.getAccess.bind(this);
    this.isAllowed = this.isAllowed.bind(this);
  }

  /**
   * 获取对 K8s 资源的操作权限，一般和 isAllowed 配合使用
   */

  /**
   * 根据已有 K8s 资源，不传入 action 或为 null 时默认取 get 操作权限
   */
  getAccess<
    K extends KubernetesResource,
    T extends K8sResourceAction = K8sResourceAction.GET
  >(
    params: K8sResourcePermissionNoneParams<K>,
  ): Observable<SelfSubjectAccessReview<T>>;

  /**
   * 根据已有 K8s 资源，进行单个指定 action 操作权限
   */
  getAccess<T extends K8sResourceAction, K extends KubernetesResource>(
    params: K8sResourcePermissionSingleParams<K, T>,
  ): Observable<SelfSubjectAccessReview<T>>;

  /**
   * 根据已有 K8s 资源，进行多个指定 action 操作权限列表
   */
  getAccess<T extends K8sResourceAction, K extends KubernetesResource>(
    params: K8sResourcePermissionMultiParams<K, T>,
  ): Observable<Array<SelfSubjectAccessReview<T>>>;

  /**
   * 根据 K8s 资源类型，不传入 action 或为 null 时默认取 list 操作权限
   */
  getAccess(
    params: K8sTypePermissionNoneParams,
  ): Observable<SelfSubjectAccessReview<K8sResourceAction.LIST>>;

  /**
   * 根据 K8s 资源类型和名称进行单个指定 action 操作权限，默认取 get 操作权限
   */
  getAccess<T extends K8sResourceAction = K8sResourceAction.GET>(
    params: K8sTypePermissionSingleParams<T>,
  ): Observable<SelfSubjectAccessReview<T>>;

  /**
   * 根据 K8s 资源类型，进行多个指定 action 操作或者多个名称的权限列表
   */
  getAccess<T extends K8sResourceAction>(
    params: K8sTypePermissionMultiParams<T>,
  ): Observable<Array<SelfSubjectAccessReview<T>>>;

  /**
   * 根据 K8s 资源类型，进行多个指定 action 操作和多个名称的权限二位数组列表
   */
  getAccess<T extends K8sResourceAction>(
    params: K8sTypePermissionMatrixParams<T>,
  ): Observable<Array<Array<SelfSubjectAccessReview<T>>>>;

  getAccess<T extends K8sResourceAction, K extends KubernetesResource>(
    typeOrResourceParams:
      | K8sTypePermissionParams<T>
      | K8sResourcePermissionParams<K, T>,
  ) {
    let params: K8sTypePermissionParams<T>;
    let group: string;
    let type$: Observable<string>;
    if ('resource' in typeOrResourceParams) {
      const {
        advanced: resourceAdvanced,
        project: resourceProject,
        cluster: resourceCluster,
        resource,
        action,
      } = typeOrResourceParams;
      params = {
        advanced: resourceAdvanced,
        project: resourceProject,
        cluster: resourceCluster,
        namespace: resource.metadata.namespace,
        name: resource.metadata.name,
        action,
      } as K8sTypePermissionParams<T>;
      group = getApiPrefixParts(resource.apiVersion).apiGroup;
      type$ = this.k8sApiResource.getResourceType(resource, resourceCluster);
    } else {
      params = typeOrResourceParams;
      const { apiGroup, type } = this.k8sApiResource.getDefinition(params.type);
      group = apiGroup;
      type$ = of(type);
    }
    const { advanced, project, cluster, namespace } = params;
    const isNameArray = Array.isArray(params.name);
    const isActionArray = Array.isArray(params.action);
    const isArray = isNameArray && isActionArray;
    const names = (isNameArray ? params.name : [params.name]) as string[];
    const actions = (isActionArray ? params.action : [params.action]) as T[];
    return type$.pipe(
      switchMap(type =>
        forkJoin(
          names.map(name =>
            of(actions).pipe(
              switchMap(actionList =>
                forkJoin(
                  actionList.map(action =>
                    this._getAccess<T>({
                      action,
                      project,
                      cluster,
                      group,
                      name,
                      namespace,
                      type,
                      advanced,
                    }),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      map(accesses =>
        isArray
          ? accesses
          : isActionArray
          ? accesses[0]
          : isNameArray
          ? accesses.map(accessGroup => accessGroup[0])
          : accesses[0][0],
      ),
      publishRef(),
    );
  }

  /**
   * 根据已经获取到的操作权限对象或列表转化为更方便使用的权限 record 或数组
   */

  /**
   * 根据已有 K8s 资源，不传入 action 或为 null 时转化为 boolean
   */
  isAllowed<T extends K8sResourceAction, K extends KubernetesResource>(
    params:
      | K8sResourcePermissionNoneParams<K>
      | K8sResourcePermissionSingleParams<K, T>,
  ): Observable<boolean>;

  /**
   * 根据已有 K8s 资源，多个 action 默认转为 record，`transform === false` 时转为 `boolean[]`
   */
  isAllowed<
    T extends K8sResourceAction,
    K extends KubernetesResource,
    R extends boolean = true
  >(
    params: K8sResourcePermissionMultiParams<K, T> &
      Partial<K8sPermissionTransformable<R>>,
  ): Observable<IfEqual<R, false, boolean[], Record<T, boolean>>>;

  /**
   * 根据 K8s 资源类型，不传入 action/name 或为 null 时，默认取 get 权限，自动转化为 boolean，`transform !== false` 时转化为 record
   */
  isAllowed<
    T extends K8sResourceAction = K8sResourceAction.GET,
    K extends boolean = false
  >(
    params: K8sTypePermissionNoneParams &
      Partial<K8sPermissionTransformable<K>>,
  ): Observable<IfEqual<K, false, boolean, Record<T, boolean>>>;

  /**
   * 根据 K8s 资源类型，传入单个 action 或 name 时，`action === ALL(*)` 时自动转化为所有权限的 record，否则转化为 `boolean`
   */
  isAllowed<T extends K8sResourceAction>(
    params: K8sTypePermissionSingleParams<T>,
  ): Observable<IfEqual<T, K8sResourceAction.ALL, AllPermissions, boolean>>;

  /**
   * 根据 K8s 资源类型，传入单个 action 或 name 时，自动转化为 record，`transform === false` 时转化为 `boolean`
   */
  isAllowed<T extends K8sResourceAction, K extends boolean>(
    params: K8sTypePermissionSingleParams<T> & K8sPermissionTransformable<K>,
  ): Observable<
    IfEqual<
      K,
      false,
      boolean,
      IfEqual<T, K8sResourceAction.ALL, AllPermissions, Record<T, boolean>>
    >
  >;

  /**
   * 根据 K8s 资源类型，传入多个 action 时，自动转化为 record，`transform === false` 时转化为 `boolean[]`
   */
  isAllowed<T extends K8sResourceAction, K extends boolean = true>(
    params: K8sTypePermissionActionsParams<T> &
      Partial<K8sPermissionTransformable<K>>,
  ): Observable<IfEqual<K, false, boolean[], Record<T, boolean>>>;

  /**
   * 根据 K8s 资源类型，传入多个 name 时，自动转化为 `boolean[]`
   */
  isAllowed<T extends K8sResourceAction>(
    params: K8sTypePermissionNamesParams<T>,
  ): Observable<boolean[]>;

  isAllowed<
    T extends K8sResourceAction,
    K extends KubernetesResource,
    R extends boolean = true
  >(
    typeOrResourceParams:
      | K8sTypePermissionWithTransformParams<T, R>
      | K8sResourcePermissionWithTransformParams<K, T, R>,
  ) {
    let { transform } = typeOrResourceParams;
    if (
      ('name' in typeOrResourceParams &&
        Array.isArray(typeOrResourceParams.name)) ||
      (typeOrResourceParams.action !== K8sResourceAction.ALL &&
        !Array.isArray(typeOrResourceParams.action) &&
        transform == null)
    ) {
      transform = false as R;
    }
    // @ts-ignore
    return this.getAccess(typeOrResourceParams).pipe(isAllowed(transform));
  }

  // @internal
  private _getAccess<T extends K8sResourceAction>({
    action,
    project,
    cluster,
    group,
    name,
    namespace,
    type,
    advanced,
  }: {
    action: T;
    project?: string;
    cluster?: string;
    group?: string;
    name?: string;
    namespace?: string;
    type?: string;
    advanced?: boolean;
  }) {
    return this.http.post<SelfSubjectAccessReview<T>>(
      `${API_GATEWAY}${
        advanced
          ? '/auth'
          : // tslint:disable-next-line: no-nested-template-literals
            `${ifExist(
              cluster,
              // tslint:disable-next-line: no-nested-template-literals
              `/kubernetes/${cluster}`,
            )}/apis/authorization.k8s.io`
      }/v1/selfsubjectaccessreviews`,
      {
        apiVersion: 'authorization.k8s.io/v1',
        kind: 'SelfSubjectAccessReview',
        spec: {
          resourceAttributes: {
            namespace,
            verb:
              action || (name ? K8sResourceAction.GET : K8sResourceAction.LIST),
            group,
            resource: type,
            name,
            cluster: advanced ? cluster : undefined,
            project: advanced ? project : undefined,
          },
        },
      },
    );
  }
}
