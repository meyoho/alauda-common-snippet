import {
  DeepPartial,
  KubernetesResource,
  ResourceListParams,
} from '../types/public-api';

export type K8sResourceDefinition = Readonly<{
  type: string;
  apiGroup?: string;
  apiVersion?: string;
}>;

export type K8sResourceDefinitions = Readonly<
  Record<string, K8sResourceDefinition>
>;

export interface ApiDefinitionBaseParams {
  definition: K8sResourceDefinition; // 一个完整的 K8sResourceDefinition，当注入的 K8sResourceDefinitions 不包含需要的资源定义时，可直接传入这个参数
}

export interface ApiTypeBaseParams<T extends string> {
  type: T;
}

export type ApiBaseParams<T extends string> = {
  notifyOnError?: boolean;
} & (ApiDefinitionBaseParams | ApiTypeBaseParams<T>);

export type ResourceBaseParams<T extends string> = ApiBaseParams<T> & {
  name: string;
};

export type ResourceListBaseParams<T extends string> = ApiBaseParams<T> & {
  queryParams?: ResourceListParams;
};

export interface GlobalResourceBaseParams {
  namespaced?: boolean; // 为 true 表示 Global 集群下 alauda-system 下的资源，比如 Cluster
}

export type GlobalResourceParams<T extends string> = ResourceBaseParams<T> &
  GlobalResourceBaseParams;

export type GlobalResourceListParams<T extends string> = ResourceListBaseParams<
  T
> &
  GlobalResourceBaseParams;

export interface ErebusResourceBaseParams {
  cluster: string;
}

export type ErebusResourceReadParams = ErebusResourceBaseParams & {
  namespace?: string;
};

export type ErebusResourceParams<T extends string> = ResourceBaseParams<T> &
  ErebusResourceReadParams;

export type ErebusResourceListParams<T extends string> = ResourceListBaseParams<
  T
> &
  ErebusResourceReadParams;

export type ResourceReadParams<T extends string> =
  | GlobalResourceParams<T>
  | ErebusResourceParams<T>;

export type ResourceListReadParams<T extends string> =
  | GlobalResourceListParams<T>
  | ErebusResourceListParams<T>;

export type ResourceBaseWriteParams<
  T extends KubernetesResource,
  R extends string
> = ApiBaseParams<R> & {
  resource: T;
};

/**
 * patch 资源时需要传递完整的原始资源 (resource) 和更新的部分 (part)
 */
export type ResourceBasePatchParams<
  T extends KubernetesResource,
  R extends string
> = ResourceBaseWriteParams<T, R> & {
  operation?: PatchOperation;
  part: DeepPartial<T> | JSONPatch[];
};

export interface ResourceAdditionalParams {
  body?: {};
}

export type GlobalResourceWriteParams<
  T extends KubernetesResource,
  R extends string
> = ResourceBaseWriteParams<T, R> & GlobalResourceBaseParams;

export type GlobalResourcePatchParams<
  T extends KubernetesResource,
  R extends string
> = ResourceBasePatchParams<T, R> & GlobalResourceBaseParams;

export type GlobalResourceDeleteParams<
  T extends KubernetesResource,
  R extends string
> = (GlobalResourceWriteParams<T, R> | GlobalResourceParams<R>) &
  ResourceAdditionalParams;

export type ErebusResourceWriteParams<
  T extends KubernetesResource,
  R extends string
> = ResourceBaseWriteParams<T, R> & ErebusResourceBaseParams;

export type ErebusResourcePatchParams<
  T extends KubernetesResource,
  R extends string
> = ResourceBasePatchParams<T, R> & ErebusResourceBaseParams;

export type ErebusResourceDeleteParams<
  T extends KubernetesResource,
  R extends string
> = (ErebusResourceWriteParams<T, R> | ErebusResourceParams<R>) &
  ResourceAdditionalParams;

export type ResourceWriteParams<
  R extends string,
  T extends KubernetesResource = KubernetesResource
> = GlobalResourceWriteParams<T, R> | ErebusResourceWriteParams<T, R>;

export type ResourcePatchParams<
  R extends string,
  T extends KubernetesResource = KubernetesResource
> = GlobalResourcePatchParams<T, R> | ErebusResourcePatchParams<T, R>;

export type ResourceDeleteParams<
  R extends string,
  T extends KubernetesResource = KubernetesResource
> = GlobalResourceDeleteParams<T, R> | ErebusResourceDeleteParams<T, R>;

export type ResourceApiParams<T extends string> =
  | GlobalResourceListParams<T>
  | ErebusResourceListParams<T>
  | ResourceReadParams<T>
  | ResourceWriteParams<T>
  | ResourcePatchParams<T>
  | ResourceDeleteParams<T>;

// https://stupefied-goodall-e282f7.netlify.com/contributors/devel/api-conventions/#patch-operations
// https://bitbucket.org/mathildetech/alauda-common-snippet/issues/17/k8sresourceapi-patch-json-patch
export enum PatchOperation {
  JSON = 'application/json-patch+json',
  Merge = 'application/merge-patch+json',
  StrategicMerge = 'application/strategic-merge-patch+json',
}

export enum JSONPatchOp {
  Test = 'test',
  Remove = 'remove',
  Add = 'add',
  Replace = 'replace',
  Move = 'move',
  Copy = 'copy',
}

export type JSONPatch =
  | {
      op: JSONPatchOp.Test | JSONPatchOp.Add | JSONPatchOp.Replace;
      path: string;
      value: unknown;
    }
  | {
      op: JSONPatchOp.Remove;
      path: string;
    }
  | {
      op: JSONPatchOp.Move | JSONPatchOp.Copy;
      from: string;
      path: string;
    };

export interface WatchAPIResponse<T extends KubernetesResource> {
  type: WatchEvent;
  object: T;
}

export enum WatchEvent {
  Added = 'ADDED',
  Modified = 'MODIFIED',
  Deleted = 'DELETED',
  Error = 'ERROR',
  Bookmark = 'BOOKMARK',
}
