import { DeepReadonly, Keys, ValueOf } from '../types/public-api';
import { toKeys } from '../utils/helpers';

import { K8sResourceDefinition, K8sResourceDefinitions } from './types';

export const EMPTY_API_PREFIX_PARTS = {
  apiGroup: '',
  apiVersion: '',
} as const;

export const getApiPrefixParts = (
  apiPrefix: string,
): Omit<Required<K8sResourceDefinition>, 'type'> => {
  if (!apiPrefix) {
    return EMPTY_API_PREFIX_PARTS;
  }
  const parts: readonly string[] = apiPrefix.split('/');
  return parts.length > 1
    ? { apiGroup: parts[0], apiVersion: parts[1] }
    : { apiGroup: '', apiVersion: parts[0] };
};

export const normalizeApiGroup = (apiGroup?: string) =>
  !apiGroup || apiGroup === 'core' ? '' : apiGroup;

export const normalizeDefinition = <T extends K8sResourceDefinition>(
  definition: T,
): Required<T> =>
  ({
    type: definition.type,
    apiGroup: normalizeApiGroup(definition.apiGroup),
    apiVersion: definition.apiVersion || 'v1',
  } as Required<T>);

function isDefinition<T extends K8sResourceDefinitions>(
  definition: keyof T | ValueOf<T>,
): definition is ValueOf<T> {
  return typeof definition === 'object';
}

export const createResourceDefinitions = <
  T extends K8sResourceDefinitions,
  R extends DeepReadonly<{
    RESOURCE_DEFINITIONS: T;
    RESOURCE_TYPES: Keys<T>;
    getYamlApiVersion: (definition: keyof T | ValueOf<T>) => string;
  }>
>(
  RESOURCE_DEFINITIONS: T,
): R =>
  ({
    RESOURCE_DEFINITIONS,
    RESOURCE_TYPES: toKeys(RESOURCE_DEFINITIONS),
    getYamlApiVersion(definition) {
      if (!isDefinition(definition)) {
        definition = RESOURCE_DEFINITIONS[definition] as ValueOf<T>;
      }
      const { apiGroup, apiVersion } = normalizeDefinition(definition);
      return (
        (!apiGroup || apiGroup === 'core' ? '' : apiGroup + '/') + apiVersion
      );
    },
  } as R);
