import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { map, retry, shareReplay, takeUntil } from 'rxjs/operators';

import { MODULES_API } from '../utils/public-api';

export interface ApiGatewayState {
  apiAddress: string;
  assetsHostPath: string;
  assetsURLPath: string;
}

const RETRY_COUNT = 3;

@Injectable({ providedIn: 'root' })
export class ApiGatewayService implements OnDestroy {
  private readonly destroy$$ = new Subject<void>();

  private readonly state$ = this.http
    .get<ApiGatewayState>(this.modulesApi)
    .pipe(retry(RETRY_COUNT), shareReplay(1), takeUntil(this.destroy$$));

  constructor(
    private readonly http: HttpClient,
    @Inject(MODULES_API) private readonly modulesApi: string,
  ) {}

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  getApiAddress() {
    return this.state$.pipe(map(state => state.apiAddress));
  }
}
