// tslint:disable no-nested-template-literals
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, isDevMode } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { StringMap } from '../types/commons';
import {
  APIResourceItem,
  APIResourceList,
  KubernetesResource,
} from '../types/k8s';
import { API_GATEWAY } from '../utils/constants';
import { ifExist } from '../utils/helpers';
import { publishRef } from '../utils/operators';
import {
  TOKEN_RESOURCE_API_PREFIXES,
  TOKEN_RESOURCE_DEFINITIONS,
} from '../utils/tokens';

import {
  EMPTY_API_PREFIX_PARTS,
  getApiPrefixParts,
  normalizeApiGroup,
  normalizeDefinition,
} from './helpers';
import { K8sResourceDefinition, K8sResourceDefinitions } from './types';

@Injectable({
  providedIn: 'root',
})
export class K8sApiResourceService {
  constructor(
    private readonly http: HttpClient,
    // tslint:disable-next-line: deprecation
    @Inject(TOKEN_RESOURCE_API_PREFIXES)
    private readonly resourceApiPrefixes: StringMap,
    @Inject(TOKEN_RESOURCE_DEFINITIONS)
    private readonly resourceDefinitions: K8sResourceDefinitions,
  ) {}

  // @internal
  private _listCaches: Record<
    string,
    Record<string, Observable<APIResourceItem[]>>
  > = {};

  // @internal
  private _typeCaches: Record<string, Record<string, Observable<string>>> = {};

  getDefinition(
    typeOrDefinition: string | K8sResourceDefinition,
  ): Required<K8sResourceDefinition> {
    if (isDevMode() && !typeOrDefinition) {
      throw new TypeError(
        `resource type or definition is required, but received: '${typeOrDefinition}'`,
      );
    }

    const definition =
      typeof typeOrDefinition === 'string'
        ? this.resourceDefinitions[typeOrDefinition]
        : typeOrDefinition;

    if (definition) {
      return normalizeDefinition(definition);
    }

    const type = typeOrDefinition as string;
    const apiPrefix = this.resourceApiPrefixes[type];
    if (!apiPrefix) {
      return { type, ...EMPTY_API_PREFIX_PARTS };
    }
    if (isDevMode()) {
      console.warn(
        '`TOKEN_RESOURCE_API_PREFIXES` now is deprecated, please use `TOKEN_RESOURCE_DEFINITIONS` instead',
      );
    }
    return { type, ...getApiPrefixParts(apiPrefix) };
  }

  getApiPath({
    cluster,
    namespace,
    name,
    type,
    apiGroup,
    apiVersion,
  }: {
    cluster?: string;
    namespace?: string;
    name?: string;
    type?: string;
    apiGroup?: string;
    apiVersion?: string;
  }) {
    apiGroup = normalizeApiGroup(apiGroup);
    return `${API_GATEWAY}${ifExist(cluster, `/kubernetes/${cluster}`)}/api${
      apiGroup && 's'
    }${ifExist(apiGroup, `/${apiGroup}`)}/${apiVersion || 'v1'}${ifExist(
      namespace,
      `/namespaces/${namespace}`,
    )}${ifExist(type, `/${type}`)}${ifExist(name, `/${name}`)}`;
  }

  getResourceList<T extends KubernetesResource>(resource: T, cluster: string) {
    const { apiVersion } = resource;

    const cached =
      this._listCaches[cluster] || (this._listCaches[cluster] = {});

    return (
      cached[apiVersion] ||
      (cached[apiVersion] = this.http
        .get<APIResourceList>(
          this.getApiPath({
            cluster,
            ...getApiPrefixParts(apiVersion),
          }),
        )
        .pipe(
          map(({ resources }) => resources),
          publishRef(),
        ))
    );
  }

  getResourceType<T extends KubernetesResource>(resource: T, cluster: string) {
    const { apiVersion } = resource;

    const cached =
      this._typeCaches[cluster] || (this._typeCaches[cluster] = {});

    return (
      cached[apiVersion] ||
      (cached[apiVersion] = this.getResourceList(resource, cluster).pipe(
        map(
          resources => resources.find(item => item.kind === resource.kind).name,
        ),
        publishRef(),
      ))
    );
  }
}
