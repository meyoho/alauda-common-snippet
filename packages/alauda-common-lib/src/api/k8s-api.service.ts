import {
  HttpClient,
  HttpDownloadProgressEvent,
  HttpEventType,
} from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { get } from 'lodash-es';
import { EMPTY, Observable, Subscription, from } from 'rxjs';
import {
  catchError,
  filter,
  map,
  retry,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { KubernetesResource, KubernetesResourceList } from '../types/k8s';
import {
  API_GATEWAY,
  NOTIFY_ON_ERROR_HEADERS,
  NOT_NOTIFY_ON_ERROR_HEADERS,
  TRUE,
} from '../utils/constants';
import { parseJSONStream } from '../utils/helpers';
import { TOKEN_GLOBAL_NAMESPACE } from '../utils/tokens';

import { getApiPrefixParts } from './helpers';
import { K8sApiResourceService } from './k8s-api-resource.service';
import {
  ApiBaseParams,
  ErebusResourceDeleteParams,
  ErebusResourceListParams,
  ErebusResourceParams,
  ErebusResourcePatchParams,
  ErebusResourceWriteParams,
  GlobalResourceDeleteParams,
  GlobalResourceListParams,
  GlobalResourceParams,
  GlobalResourcePatchParams,
  GlobalResourceWriteParams,
  PatchOperation,
  ResourceApiParams,
  ResourceDeleteParams,
  ResourceListReadParams,
  ResourcePatchParams,
  ResourceReadParams,
  ResourceWriteParams,
  WatchAPIResponse,
  WatchEvent,
} from './types';

// 名称前缀 Erebus 参考 http://confluence.alauda.cn/pages/viewpage.action?pageId=44663484 指 /kubernetes/ 开头的 api 组件

export const TIMEOUT_SECONDS = 59;

@Injectable({ providedIn: 'root' })
export class K8sApiService<R extends string = string> {
  constructor(
    private readonly http: HttpClient,
    private readonly k8sApiResource: K8sApiResourceService,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  /**
   * 获取 Global 集群单个资源对象
   */
  getGlobalResource<T extends KubernetesResource>(
    params: GlobalResourceParams<R>,
  ) {
    return this._getResource<T>(params);
  }

  /**
   * 监听 Global 集群单个资源对象
   */
  watchGlobalResource<T extends KubernetesResource>(
    params: GlobalResourceParams<R>,
  ) {
    return this._watchResource<T>(params);
  }

  /**
   * 监听 Global 集群资源列表变化
   */
  watchGlobalResourceChange<T extends KubernetesResource>(
    seedVersion: string,
    params: GlobalResourceListParams<R>,
  ) {
    return this._keepWatchResourceChange<T>(seedVersion, params);
  }

  /**
   * 获取 Global 集群资源对象列表，包含分页信息
   */
  getGlobalResourceList<T extends KubernetesResource>(
    params: GlobalResourceListParams<R>,
  ) {
    return this._getResourceList<T>(params);
  }

  /**
   * 创建 Global 集群资源
   */
  postGlobalResource<T extends KubernetesResource>(
    params: GlobalResourceWriteParams<T, R>,
  ) {
    return this._postResource<T>(params);
  }

  /**
   * 完整覆盖更新 Global 集群单个资源对象
   */
  putGlobalResource<T extends KubernetesResource>(
    params: GlobalResourceWriteParams<T, R>,
  ) {
    return this._putResource<T>(params);
  }

  /**
   * 增量更新 Global 集群单个资源对象的部分属性
   */
  patchGlobalResource<T extends KubernetesResource>(
    params: GlobalResourcePatchParams<T, R>,
  ) {
    return this._patchResource<T>(params);
  }

  /**
   * 删除 Global 集群单个资源
   */
  deleteGlobalResource<T extends KubernetesResource>(
    params: GlobalResourceDeleteParams<T, R>,
  ) {
    return this._deleteResource<T>(params);
  }

  /**
   * 获取业务集群单个资源对象
   */
  getResource<T extends KubernetesResource>(params: ErebusResourceParams<R>) {
    return this._getResource<T>(params);
  }

  /**
   * 监听业务集群单个资源对象
   */
  watchResource<T extends KubernetesResource>(params: ErebusResourceParams<R>) {
    return this._watchResource<T>(params);
  }

  /**
   * 监听资源列表变化
   */
  watchResourceChange<T extends KubernetesResource>(
    seedVersion: string,
    params: ErebusResourceListParams<R>,
  ) {
    return this._keepWatchResourceChange<T>(seedVersion, params);
  }

  /**
   * 获取业务集群资源对象列表，包含分页信息
   */
  getResourceList<T extends KubernetesResource>(
    params: ErebusResourceListParams<R>,
  ) {
    return this._getResourceList<T>(params);
  }

  /**
   * 创建业务集群资源
   */
  postResource<T extends KubernetesResource>(
    params: ErebusResourceWriteParams<T, R>,
  ) {
    return this._postResource<T>(params);
  }

  /**
   * 完整覆盖更新业务集群单个资源对象
   */
  putResource<T extends KubernetesResource>(
    params: ErebusResourceWriteParams<T, R>,
  ) {
    return this._putResource<T>(params);
  }

  /**
   * 增量更新业务集群单个资源对象的部分属性
   */
  patchResource<T extends KubernetesResource>(
    params: ErebusResourcePatchParams<T, R>,
  ) {
    return this._patchResource<T>(params);
  }

  /**
   * 删除业务集群单个资源
   */
  deleteResource<T extends KubernetesResource>(
    params: ErebusResourceDeleteParams<T, R>,
  ) {
    return this._deleteResource<T>(params);
  }

  // @internal
  private _getResourceDefinition(params: ResourceApiParams<R>) {
    const definition = this.k8sApiResource.getDefinition(
      'definition' in params && params.definition
        ? params.definition
        : 'type' in params && params.type,
    );
    if ('resource' in params && params.resource) {
      return {
        type: definition.type,
        ...getApiPrefixParts(params.resource.apiVersion),
      };
    }
    return definition;
  }

  // @internal
  private _getApiPath(params: ResourceApiParams<R>, ignoreName = false) {
    return this.k8sApiResource.getApiPath({
      ...this._getResourceDefinition(params),
      cluster: 'cluster' in params && params.cluster,
      namespace: this._getNamespace(params),
      name: ignoreName ? null : this._getName(params),
    });
  }

  // @internal
  private _getNamespace(params: ResourceApiParams<R>) {
    return 'namespace' in params && params.namespace
      ? params.namespace
      : 'resource' in params && params.resource
      ? get(params.resource, ['metadata', 'namespace'])
      : 'namespaced' in params && params.namespaced && this.globalNamespace;
  }

  // @internal
  private _getName(params: ResourceApiParams<R>) {
    return 'resource' in params && params.resource
      ? get(params.resource, ['metadata', 'name'])
      : 'name' in params && params.name;
  }

  /**
   * k8s list api 返回的结果里，items 缺失 kind 和 apiVersion 字段
   * 规则如 Service 类型对应 kind: ServiceList，去掉最后四个字符可得到资源的 kind
   * @internal
   */
  private _getNormalizedList<T extends KubernetesResourceList>(list: T): T {
    const { apiVersion, items } = list;

    if (!items || items.length === 0) {
      return list;
    }

    const kind = list.kind.slice(0, -4);
    return {
      ...list,
      items: items.map(item => ({
        ...item,
        kind,
        apiVersion,
      })),
    };
  }

  // @internal
  private _getResource<T extends KubernetesResource>(
    params: ResourceReadParams<R>,
  ) {
    return this.http.get<T>(this._getApiPath(params), {
      headers: this._getHeaders(params),
    });
  }

  // @internal
  private _watchResource<T extends KubernetesResource>(
    params: ResourceReadParams<R>,
  ) {
    const detailParams = {
      ...params,
      queryParams: { fieldSelector: 'metadata.name=' + this._getName(params) },
    };
    return this._getResourceList<T>(detailParams).pipe(
      map(list => this._getNormalizedList(list)),
      switchMap(list => {
        return this._keepWatchResourceChange<T>(
          list.metadata.resourceVersion,
          detailParams,
        ).pipe(
          filter(
            res =>
              res.type !== WatchEvent.Bookmark && res.type !== WatchEvent.Error,
          ),
          map(res => (res.type === WatchEvent.Deleted ? null : res.object)),
          startWith(list.items[0] ?? null),
        );
      }),
    );
  }

  // @internal
  private _keepWatchResourceChange<T extends KubernetesResource>(
    seedVersion: string,
    params: ResourceListReadParams<R>,
  ) {
    let resourceVersion = seedVersion;
    let sub: Subscription;

    return new Observable<WatchAPIResponse<T>>(observer => {
      const unsubscribe = () => {
        if (sub && !sub.closed) {
          sub.unsubscribe();
        }
      };

      const watch = () => {
        unsubscribe();
        sub = this._watchResourceChange<T>({
          ...params,
          queryParams: {
            ...params.queryParams,
            watch: TRUE,
            allowWatchBookmarks: TRUE,
            timeoutSeconds: String(TIMEOUT_SECONDS),
            resourceVersion,
          },
        }).subscribe({
          next(res) {
            if (res.type === WatchEvent.Bookmark) {
              resourceVersion = res.object.metadata.resourceVersion;
            }
            observer.next(res);
          },
          error(err) {
            observer.error(err);
          },
          complete: watch,
        });
      };

      watch();
      return unsubscribe;
    }).pipe(
      retry(3),
      catchError(() => EMPTY),
    );
  }

  // @internal
  private _watchResourceChange<T extends KubernetesResource>(
    params: ResourceListReadParams<R>,
  ): Observable<WatchAPIResponse<T>> {
    return this.http
      .get(this._getApiPath(params, true), {
        responseType: 'text',
        observe: 'events',
        reportProgress: true,
        headers: this._getHeaders(params),
        params: params.queryParams,
      })
      .pipe(
        filter(ev => ev.type === HttpEventType.DownloadProgress),
        filter(
          ({ partialText }: HttpDownloadProgressEvent) => !!partialText?.trim(),
        ),
        switchMap(({ partialText: text }) =>
          from(
            parseJSONStream<WatchAPIResponse<T>>(text).reduce((acc, curr) => {
              if (!acc.length) {
                return [curr];
              }
              const latest = acc[acc.length - 1];
              if (
                latest.type === curr.type &&
                latest.object.metadata.name === curr.object.metadata.name
              ) {
                return acc.slice(0, -1).concat(curr);
              } else {
                return acc.concat(curr);
              }
            }, [] as Array<WatchAPIResponse<T>>),
          ),
        ),
        filter(v => !!v),
      );
  }

  // @internal
  private _getResourceList<T extends KubernetesResource>(
    params: ResourceListReadParams<R>,
  ) {
    // 包含 keyword 前缀需要加上前缀 'acp/v1/resources/search'
    // 参考 http://confluence.alauda.cn/pages/viewpage.action?pageId=48731058
    let url = this._getApiPath(params, true);
    const queryParams = { ...params.queryParams };
    if (queryParams.keyword) {
      url = url.replace(API_GATEWAY, `${API_GATEWAY}/acp/v1/resources/search`);
      if (!queryParams.field) {
        queryParams.field = 'metadata.name';
      }
    }
    return this.http
      .get<KubernetesResourceList<T>>(url, {
        params: queryParams,
        headers: this._getHeaders(params),
      })
      .pipe(map(this._getNormalizedList));
  }

  // @internal
  private _postResource<T extends KubernetesResource>(
    params: ResourceWriteParams<R, T>,
  ) {
    return this.http.post<T>(this._getApiPath(params, true), params.resource, {
      headers: this._getHeaders(params),
    });
  }

  // @internal
  private _putResource<T extends KubernetesResource>(
    params: ResourceWriteParams<R, T>,
  ) {
    return this.http.put<T>(this._getApiPath(params), params.resource, {
      headers: this._getHeaders(params),
    });
  }

  // @internal
  private _patchResource<T extends KubernetesResource>(
    params: ResourcePatchParams<R, T>,
  ) {
    return this.http.patch<T>(this._getApiPath(params), params.part, {
      headers: {
        ...this._getHeaders(params),
        'Content-Type': params.operation || PatchOperation.Merge,
      },
    });
  }

  // @internal
  private _deleteResource<T extends KubernetesResource>(
    params: ResourceDeleteParams<R, T>,
  ) {
    return this.http.request<T>('delete', this._getApiPath(params), {
      headers: this._getHeaders(params),
      body: params.body,
    });
  }

  // @internal
  private readonly _getHeaders = (params: ApiBaseParams<R>) => {
    switch (params.notifyOnError) {
      case true:
        return NOTIFY_ON_ERROR_HEADERS;
      case false:
        return NOT_NOTIFY_ON_ERROR_HEADERS;
    }
  };
}
