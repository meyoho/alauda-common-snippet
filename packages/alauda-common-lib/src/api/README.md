# API 相关功能

## API Gateway

### Services

#### `ApiGatewayService`

用于在产品初始化时，通过 alauda-console 查询 API Gateway 服务器地址。

#### `ApiGatewayInterceptor`

默认实现，使用`@alauda/common-snippet`的项目可选择使用此实现，或者在项目内提供自定义实现，默认实现会自动使用 API Gateway 服务器地址替换`{{API_GATEWAY}}`开头的 api 中的`{{API_GATEWAY}}`，同时会给 method 为 `PATCH` 的请求附加`'Content-Type': 'application/merge-patch+json'`的 header。

**因 Angular Interceptor 需要保持合理的顺序才能工作, 需要自行在项目中提供默认实现或自定义实现。**

## K8s 资源标准 API

### Services

#### `K8sApiResourceService`

`APIResourceList` 也是一种 K8s 资源，主要用于根据已有 K8s 资源对象获取其完整的 API 结构信息

- `#getDefinition`: 根据 K8s 资源定义类型或非完整定义结构返回完整的 K8s 资源定义结构（`K8sResourceDefinition`）
- `#getApiPath`: 根据集群、命名空间、资源类型等参数返回包含 `API_GATEWAY` 前缀的完整 API url
- `#getResourceList`: 根据已有的 K8s 资源对象和集群参数返回其对应的 `APIResourceItem` 数组，包含每个资源类型支持的操作类型 (`verbs`)
- `#getResourceType`: 根据已有的 K8s 资源对象和集群参数返回其完整的资源类型名称

#### `K8sApiService`

Global/业务集群增删改查 API，不满足标准 API 时请在项目实现相应的高级 API

对于写入 API 网络发生错误或者返回异常会自动通知提醒，可以传入 `notifyOnError: false` 针对性关闭通知提醒；只读 API 默认不会通知，可以传入 `notifyOnError: true` 针对性打开通知提醒。

### Helpers

#### `createResourceDefinitions`

用于创建 K8s 资源定义结构（`K8sResourceDefinition`）的辅助函数，提供更好地类型支持，使用方式参考：[icarus](https://bitbucket.org/mathildetech/icarus/src/master/src/app/typings/k8s-resource-definitions.ts)
