# acl-disabled-container

## 组件意义

解决 button 在 disabled 时 tooltip 无法生效的问题。

## 输入参数

### tooltip

自定义 tooltip 文案，默认为空

### tooltipPosition

tooltip 的位置参数，透传至 auiTooltipPosition

### isAllowed

权限控制参数，默认为 false，false 时组件的 tooltip 为 '无权限，请联系管理员'

### isDisabled

条件控制参数，默认为 false

## 本组件处理规则

isAllowed 为 true，isDisabled 为 false，tooltip 为 any， 组件正常显示

isAllowed 为 true，isDisabled 为 true，tooltip 为 any， 组件 tooltip 显示为 传入的 tooltip 的值

isAllowed 为 false，isDisabled 为 any，tooltip 为 any， 组件 tooltip 显示为 '无权限，请联系管理员'

总之：优先判断 isAllowed，若 isAllowed 为 false，则显示'无权限，请联系管理员'

## 使用 Demo

```html
<!-- template.html -->
<acl-disabled-container [isAllowed]="creatable$ | async">
  <button aui-button="primary" (click)="create()">
    {{ 'create' | translate }}
  </button>
</acl-disabled-container>
```

```html
<!-- template.html -->
<acl-disabled-container
  [isAllowed]="creatable$ | async"
  [isDisabled]="condition"
  [tooltip]="'custom-tooltip'"
>
  <button aui-button="primary" (click)="create()">
    {{ 'create' | translate }}
  </button>
</acl-disabled-container>
```

```html
<!-- template.html -->
<aui-menu #menu>
  <acl-disabled-container
    [isAllowed]="updated$ | async"
    tooltipPosition="start"
  >
    <aui-menu-item (click)="update()">
      {{ 'update' | translate }}
    </aui-menu-item>
  </acl-disabled-container>
  <acl-disabled-container
    [isAllowed]="deleted$ | async"
    tooltipPosition="start"
  >
    <aui-menu-item (click)="delete()">
      {{ 'delete' | translate }}
    </aui-menu-item>
  </acl-disabled-container>
</aui-menu>
```
