import { TooltipModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '../translate/public-api';

import { DisabledContainerComponent } from './disabled-container.component';

@NgModule({
  imports: [CommonModule, TranslateModule, TooltipModule],
  declarations: [DisabledContainerComponent],
  exports: [DisabledContainerComponent],
})
export class DisabledContainerModule {}
