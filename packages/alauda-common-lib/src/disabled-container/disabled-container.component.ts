import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { Observable, Subscription, combineLatest } from 'rxjs';

import { ObservableInput } from '../utils/public-api';

@Component({
  selector: 'acl-disabled-container',
  templateUrl: './disabled-container.component.html',
  styleUrls: ['disabled-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class DisabledContainerComponent implements AfterViewInit, OnDestroy {
  @Input() tooltip = '';
  @Input() tooltipPosition = 'top';
  @ObservableInput(true)
  private readonly isAllowed$: Observable<boolean>;

  @Input() isAllowed = false;
  @ObservableInput(true)
  private readonly isDisabled$: Observable<boolean>;

  @Input() isDisabled = false;

  private disabledSubscription: Subscription;

  constructor(private readonly elementRef: ElementRef) {}

  ngAfterViewInit() {
    this.disabledSubscription = combineLatest([
      this.isAllowed$,
      this.isDisabled$,
    ]).subscribe(([isAllowed, isDisabled]) => {
      this.setButtonStatus(isAllowed && !isDisabled);
    });
  }

  ngOnDestroy() {
    if (this.disabledSubscription) {
      this.disabledSubscription.unsubscribe();
    }
  }

  private setButtonStatus(isAllowed: boolean) {
    const container = this.elementRef.nativeElement.querySelector(
      '.acl-disabled-container',
    );
    if (!container) {
      return;
    }
    const button = container.querySelector('button');
    if (button) {
      isAllowed
        ? button.removeAttribute('disabled')
        : button.setAttribute('disabled', true);
    }
    const a = container.querySelector('a');
    if (a) {
      isAllowed
        ? (a.style['pointer-events'] = 'auto')
        : (a.style['pointer-events'] = 'none');
    }
  }
}
