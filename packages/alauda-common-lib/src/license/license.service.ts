import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, InjectionToken } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, shareReplay } from 'rxjs/operators';

import { FeatureGate } from '../types/k8s';
import { API_GATEWAY } from '../utils/constants';
import { K8sUtilService } from '../utils/public-api';

import { LicenseProductName, LicenseStatus } from './license.type';

// each platform inject this token to specify product name
export const LICENSE_PRODUCT_NAME_TOKEN = new InjectionToken<
  LicenseProductName
>('product_name');

export const LICENSE_ERROR_URL_TOKEN = new InjectionToken<string>(
  'license_error_url',
  { providedIn: 'root', factory: () => 'license-error' },
);

@Injectable({ providedIn: 'root' })
export class LicenseService {
  status$ = this.getStatus();

  constructor(
    @Inject(LICENSE_PRODUCT_NAME_TOKEN)
    private readonly productName: LicenseProductName,
    private readonly http: HttpClient,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  getStatus(): Observable<LicenseStatus> {
    return this.http
      .get<FeatureGate>(`${API_GATEWAY}/fg/v1/featuregates/license`, {
        params: { product: this.formatProductName(this.productName) },
      })
      .pipe(
        map(featureGate => {
          if (featureGate.status.enabled) {
            return LicenseStatus.AUTHORIZED;
          }
          const annotation = this.k8sUtil.getAnnotation(
            featureGate,
            'reason',
            'license',
          );
          return annotation === 'Expired'
            ? LicenseStatus.EXPIRED
            : annotation === 'Forbidden'
            ? LicenseStatus.NO_PERMISSION
            : LicenseStatus.UNAUTHORIZED;
        }),
        catchError(() => {
          return of(LicenseStatus.AUTHORIZED);
        }),
        shareReplay(1),
      );
  }

  // replace space to -, as Container-Platform
  private formatProductName(productName: string) {
    return productName.replace(/\s+/g, '-');
  }
}
