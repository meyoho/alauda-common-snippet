import { Inject, Injectable, Optional } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

import { AuthorizationGuardService } from '../authorization/public-api';

import { LICENSE_ERROR_URL_TOKEN, LicenseService } from './license.service';
import { LicenseStatus } from './license.type';

@Injectable({ providedIn: 'root' })
export class LicenseGuardService implements CanActivate, CanActivateChild {
  constructor(
    @Optional()
    @Inject(LICENSE_ERROR_URL_TOKEN)
    private readonly errorUrl: string,
    private readonly router: Router,
    private readonly licenseService: LicenseService,
    private readonly authGuard: AuthorizationGuardService,
  ) {
    this.statusHandler = this.statusHandler.bind(this);
  }

  canActivate() {
    return this.authGuard.canActivate().pipe(
      switchMap(_ => this.licenseService.status$),
      map(this.statusHandler),
    );
  }

  canActivateChild() {
    return this.canActivate();
  }

  // action mapper accord to status
  statusHandler(status: LicenseStatus) {
    if (status === LicenseStatus.AUTHORIZED) {
      return true;
    }
    return this.router.parseUrl(this.errorUrl);
  }
}
