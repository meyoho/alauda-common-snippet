export enum LicenseStatus {
  EXPIRED = 'expired',
  UNAUTHORIZED = 'unauthorized',
  AUTHORIZED = 'authorized',
  NO_PERMISSION = 'no_permission',
}

// 支持证书功能的产品名称
// reference: http://confluence.alauda.cn/pages/viewpage.action?pageId=61903707
export enum LicenseProductName {
  CONTAINER_PLATFORM = 'Container Platform',
  DEVOPS = 'DevOps',
  SERVICE_MESH = 'Service Mesh',
  MACHINE_LEARNING = 'Machine Learning',
  API_MANAGEMENT_PLATFORM = 'API Management Platform',
}
