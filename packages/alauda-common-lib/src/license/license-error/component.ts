import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';

import { TranslateService } from '../../translate/public-api';
import { LICENSE_PRODUCT_NAME_TOKEN, LicenseService } from '../license.service';
import { LicenseStatus } from '../license.type';

type LicenseErrorStatus = typeof LicenseStatus[Exclude<
  keyof typeof LicenseStatus,
  'AUTHORIZED'
>];

const LICENSE_ERROR_TITLE_MAPPER: Record<LicenseErrorStatus, string> = {
  expired: 'expired_license',
  unauthorized: 'no_available_license',
  no_permission: 'no_permission_license',
};

@Component({
  selector: 'acl-license-error',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LicenseErrorComponent {
  LicenseStatus = LicenseStatus;
  errorStatus$ = this.licenseService.status$.pipe(
    // in case user input license-error url
    tap(status => {
      if (status === LicenseStatus.AUTHORIZED) {
        this.router.navigateByUrl('/');
      }
    }),
  );

  constructor(
    private readonly router: Router,
    private readonly licenseService: LicenseService,
    public readonly translate: TranslateService,
    @Inject(LICENSE_PRODUCT_NAME_TOKEN) private readonly productName: string,
  ) {
    this.getErrorContent = this.getErrorContent.bind(this);
  }

  getErrorTitle(status: LicenseErrorStatus) {
    return LICENSE_ERROR_TITLE_MAPPER[status];
  }

  getErrorContent(status: LicenseErrorStatus) {
    return status === LicenseStatus.NO_PERMISSION
      ? this.translate.get('no_permission_license_hint')
      : this.translate.get('license_error_hint', { product: this.productName });
  }

  shouldShowImport(status: LicenseErrorStatus) {
    return status !== LicenseStatus.NO_PERMISSION;
  }
}
