import { ButtonModule, PageModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '../../translate/public-api';
import { UtilsModule } from '../../utils/public-api';

import { LicenseErrorComponent } from './component';

@NgModule({
  declarations: [LicenseErrorComponent],
  imports: [
    PageModule,
    CommonModule,
    ButtonModule,
    TranslateModule,
    UtilsModule,
  ],
  exports: [LicenseErrorComponent],
})
export class LicenseErrorModule {}
