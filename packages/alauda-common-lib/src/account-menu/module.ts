import { DropdownModule, IconModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '../translate/public-api';

import { AccountMenuComponent } from './component/component';

@NgModule({
  imports: [CommonModule, IconModule, TranslateModule, DropdownModule],
  declarations: [AccountMenuComponent],
  exports: [AccountMenuComponent],
})
export class AccountMenuModule {}
