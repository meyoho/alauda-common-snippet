import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { AuthorizationStateService } from '../../authorization/public-api';
import { TranslateService } from '../../translate/public-api';

export interface AccountInfo {
  iss?: string;
  sub?: string;
  aud?: string;
  exp?: number;
  iat?: number;
  azp?: string;
  at_hash?: string;
  email?: string;
  email_verified?: true;
  name?: string;
  token?: string;
  ext?: {
    is_admin?: boolean;
    conn_id?: boolean;
  };
}

@Component({
  selector: 'acl-account-menu',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountMenuComponent {
  @Input()
  platformPath = '/console-platform';

  displayName$ = this.auth
    .getTokenPayload<AccountInfo>()
    .pipe(map(account => account.email || account.name));

  constructor(
    private readonly auth: AuthorizationStateService,
    private readonly translate: TranslateService,
    private readonly router: Router,
  ) {}

  logout() {
    this.auth.logout();
  }

  changeLanguage() {
    this.translate.toggleLocale();
  }

  navigateToLink(href: string) {
    if (!this.platformPath) {
      this.router.navigate([href]);
    } else {
      window.open(`${this.platformPath}${href}`, '_parent');
    }
  }

  get icon() {
    return `basic:${
      this.translate.locale === 'zh' ? 'switch_to_english' : 'switch_to_chinese'
    }`;
  }
}
