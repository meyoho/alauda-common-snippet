import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, OnDestroy } from '@angular/core';
import jwtDecode from 'jwt-decode';
import { EMPTY, Observable, Subject, concat, of } from 'rxjs';
import {
  catchError,
  map,
  shareReplay,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { StringMap } from '../types/public-api';
import {
  CODE_KEY,
  ID_TOKEN_KEY,
  REFRESH_TOKEN_KEY,
  TOKEN_CALLBACK_API,
  TOKEN_INFO_API,
  TOKEN_LOGIN_API,
  TOKEN_REFRESH_API,
  publishRef,
} from '../utils/public-api';

export interface AuthorizationState {
  idToken: string;
  refreshToken: string;
  authUrl: string;
  state: string;
}

@Injectable({ providedIn: 'root' })
export class AuthorizationStateService implements OnDestroy {
  private readonly destroy$$ = new Subject<void>();
  private readonly refreshToken$$ = new Subject<{
    idToken: string;
    refreshToken: string;
  }>();

  private readonly state$ = this.getAuthConfiguration().pipe(
    switchMap(config =>
      concat(this.getExistedToken(config), this.refreshToken$$).pipe(
        map(token => ({ ...config, ...token })),
      ),
    ),
    tap(state => {
      this.stateSnapshot = state;
      if (!state.idToken) {
        this.logout();
      }
    }),
    takeUntil(this.destroy$$),
    shareReplay(1),
  );

  private checkTokenCache: Record<string, Observable<unknown>> = {};
  private refreshTokenCache$: Observable<string>;

  stateSnapshot: AuthorizationState = null;

  constructor(
    private readonly http: HttpClient,
    @Inject(ID_TOKEN_KEY) private readonly idTokenKey: string,
    @Inject(REFRESH_TOKEN_KEY) private readonly refreshTokenKey: string,
    @Inject(CODE_KEY) private readonly codeKey: string,
    @Inject(TOKEN_LOGIN_API) private readonly apiTokenLogin: string,
    @Inject(TOKEN_CALLBACK_API) private readonly apiTokenCallback: string,
    @Inject(TOKEN_REFRESH_API) private readonly apiRefreshToken: string,
    @Inject(TOKEN_INFO_API) private readonly apiTokenInfo: string,
  ) {}

  logout() {
    localStorage.removeItem(this.idTokenKey);
    localStorage.removeItem(this.refreshTokenKey);
    this.redirectToLogin();
  }

  redirectToLogin() {
    if (this.stateSnapshot) {
      location.href = this.stateSnapshot.authUrl;
    }
  }

  refreshToken() {
    const refreshToken = localStorage.getItem(this.refreshTokenKey);
    if (refreshToken) {
      if (!this.refreshTokenCache$) {
        this.refreshTokenCache$ = this.getTokenByRefreshToken(
          refreshToken,
        ).pipe(
          tap(token => {
            this.refreshTokenCache$ = null;
            this.refreshToken$$.next(token);
          }),
          map(({ idToken }) => idToken),
          catchError(() => {
            this.logout();
            return EMPTY;
          }),
          publishRef(),
        );
      }
      return this.refreshTokenCache$;
    } else {
      this.logout();
      return EMPTY;
    }
  }

  checkToken(token: string) {
    if (!this.checkTokenCache[token]) {
      this.checkTokenCache[token] = this.http
        .get(this.apiTokenInfo, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .pipe(
          tap(
            () => {
              this.checkTokenCache[token] = null;
            },
            () => {
              this.checkTokenCache[token] = null;
            },
          ),
          publishRef(),
        );
    }
    return this.checkTokenCache[token];
  }

  getToken() {
    return this.state$.pipe(map(state => state.idToken));
  }

  getTokenPayload<T>() {
    return this.state$.pipe(map(state => jwtDecode<T>(state.idToken)));
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  private getAuthConfiguration() {
    return this.http
      .get<{ auth_url: string; state: string }>(this.apiTokenLogin)
      .pipe(
        map(res => ({
          authUrl: res.auth_url,
          state: res.state,
        })),
      );
  }

  private getExistedToken(config: { authUrl: string; state: string }) {
    return this.getTokenFromLocal(config).pipe(
      catchError(error => {
        console.error(error);
        return of({ idToken: '', refreshToken: '' });
      }),
    );
  }

  private getTokenFromLocal(config: { authUrl: string; state: string }) {
    const { queryParams, hashParams } = this.getParams();

    const code = queryParams[this.codeKey] || hashParams[this.codeKey];
    /**
     * 加入 !localStorage.getItem(this.idTokenKey) 这个判断条件。
     * 原因是登录完成之后手动刷新页面时这个code 是失效的，如果继续使用code获取token api 会出错。然后重定向到登陆页面
     * 目前没有想到哪种场景在存在 token 的情况下还需要code 去获取 token
     */
    if (code && !localStorage.getItem(this.idTokenKey)) {
      return this.getTokenByCode(code, config.state);
    }

    const refreshToken =
      queryParams[this.refreshTokenKey] || hashParams[this.refreshTokenKey];
    if (refreshToken) {
      return this.getTokenByRefreshToken(refreshToken);
    }

    const idToken = queryParams[this.idTokenKey] || hashParams[this.idTokenKey];
    if (idToken) {
      localStorage.setItem(this.idTokenKey, idToken);
    }

    return of({
      idToken: idToken ?? localStorage.getItem(this.idTokenKey) ?? '',
      refreshToken: localStorage.getItem(this.refreshTokenKey) ?? '',
    });
  }

  private getTokenByRefreshToken(refreshToken: string) {
    return this.http
      .get<{ id_token: string; refresh_token: string }>(this.apiRefreshToken, {
        params: { refresh_token: refreshToken },
      })
      .pipe(
        tap(({ id_token, refresh_token }) => {
          localStorage.setItem(this.idTokenKey, id_token);
          localStorage.setItem(this.refreshTokenKey, refresh_token);
        }),
        map(({ id_token, refresh_token }) => ({
          idToken: id_token,
          refreshToken: refresh_token,
        })),
      );
  }

  private getTokenByCode(code: string, state: string) {
    return this.http
      .get<{ id_token: string; refresh_token: string }>(this.apiTokenCallback, {
        params: { code, state },
      })
      .pipe(
        tap(({ id_token, refresh_token }) => {
          localStorage.setItem(this.idTokenKey, id_token);
          localStorage.setItem(this.refreshTokenKey, refresh_token);
        }),
        map(({ id_token, refresh_token }) => ({
          idToken: id_token,
          refreshToken: refresh_token,
        })),
      );
  }

  private getParams() {
    const initUrl = sessionStorage.getItem('_init_url');

    if (!initUrl) {
      throw new Error(
        '_init_url must provide in session storage, make sure invoke recordInitUrl before angular bootstrap.',
      );
    }

    const initLocation = new URL(initUrl);

    const queryParams = parseParams(
      initLocation.search ? initLocation.search.slice(1) : '',
    );

    const hashParams = parseParams(
      initLocation.hash ? initLocation.hash.slice(1) : '',
    );

    return { queryParams, hashParams };
  }
}

function parseParams(query: string): StringMap {
  if (!query) {
    return {};
  }

  return query.split('&').reduce((acc, param) => {
    const [key, value] = param.split('=');

    return {
      ...acc,
      [key]: value,
    };
  }, {});
}
