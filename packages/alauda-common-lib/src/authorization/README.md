# 公共 Authorization 模块

## Services

### `AuthorizationStateService`

身份认证状态服务，认证过程：

1. 通过 alauda-console 获取身份认证相关配置 (`TOKEN_LOGIN_API`)。

2. 检查项目初始 url (需要在 Angular 应用 Bootstrap 之前调用 `recordInitUrl` 记录到 SessionStorage) 是否包含 `id_token` 或者 `code` (详情可参考 oidc 规范, `id_token`, `code` 在 url 中的名称可在项目中通过注入 `ID_TOKEN_KEY`, `CODE_KEY` 自定义). 包含的话，跳转到 6.

3. url 中如果包含的是 `id_token`, 跳转到 5.

4. url 中如果包含的是 `code`, 使用 `code` 通过 alauda-console 获取 `id_token` (`TOKEN_CALLBACK_API`).

5. 记录 `id_token` 到 LocalStorage 的 `token` 项.

6. 检查 LocalStorage 是否已经包含 `token`，如果不包含，转到 登录页. 否则继续流程.

7. 赋值 `stateSnapshot`， 提供 Auth state 快照.

服务实例方法:

- `logout(): void` 登出系统, 移除 LocalStorage 中的 token 项, 并重定向回登录页.

- `redirectToLogin(): void` 重定向回登录页.

- `getToken(): Observable<string>` 获取 token.

- `getTokenPayload<T>(): T` 获取 token 携带的数据.

### `AuthorizationInterceptorService`

自动给需要身份认证的 API 附加 Bearer Header，是否附加由以下条件决定。

- 不是 alauda console Api (`MODULES_API`, `TOKEN_LOGIN_API`, `TOKEN_CALLBACK_API`, `TOKEN_INFO_API`)

- 不以 api gateway 地址开头，且已经包含 Authorization Header.

- 不在匿名 Api 声明中，(`ANONYMOUS_APIS`).

**因 Angular Interceptor 需要保持合理的顺序才能工作, 需要自行在项目中提供默认实现或自定义实现。**

### `AuthorizationGuardService`

路由守卫，如果路由需要身份认证，则阻塞直到认证通过.

### `recordInitUrl(): void`

记录项目初始 url 到 SessionStorage, 避免 Angular 路由接管后, url 会被修改.
