export function recordInitUrl() {
  if (location?.href) {
    sessionStorage.setItem('_init_url', location.href);
  }
}
