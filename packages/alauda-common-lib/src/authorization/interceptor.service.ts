import {
  HttpErrorResponse,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Inject, Injectable, Injector } from '@angular/core';
import { combineLatest, of, throwError } from 'rxjs';
import { catchError, concatMap, map, switchMap, take } from 'rxjs/operators';

import { ApiGatewayService } from '../api/public-api';
import { ANONYMOUS_APIS } from '../utils/public-api';

import { AuthorizationStateService } from './state.service';

// Interceptor must provide with correct order, not provide in root here
// https://angular.cn/guide/http#interceptor-order
@Injectable()
export class AuthorizationInterceptorService implements HttpInterceptor {
  private auth: AuthorizationStateService;

  constructor(
    @Inject(ANONYMOUS_APIS)
    private readonly anonymousApis: Array<string | RegExp>,
    private readonly apiGateway: ApiGatewayService,
    private readonly injector: Injector,
  ) {}

  intercept(req: HttpRequest<unknown>, next: HttpHandler) {
    if (this.isConsoleRelative(req.url)) {
      return next.handle(req);
    }

    const auth = this.getAuth();

    return combineLatest([
      this.apiGateway.getApiAddress(),
      auth.getToken(),
    ]).pipe(
      take(1),
      concatMap(([apiAddress, idToken]) => {
        const authReq = this.needAuthorization(apiAddress, req)
          ? this.cloneReq(req, idToken)
          : req;
        return next.handle(authReq).pipe(
          catchError((error: HttpErrorResponse) => {
            if (!this.isUnauthorized(error)) {
              return throwError(error);
            }

            return auth.checkToken(idToken).pipe(
              map(() => false),
              catchError(() => of(true)),
              switchMap(expired =>
                expired ? this.retryAuth(next, req) : throwError(error),
              ),
            );
          }),
        );
      }),
    );
  }

  // console relative means url protocol+hostname+port same with console, for this case, just check url start with ```http(s)://``` or ```//```
  isConsoleRelative(url: string) {
    const lowerCaseUrl = (url || '').toLowerCase();

    return (
      !lowerCaseUrl.startsWith('http://') &&
      !lowerCaseUrl.startsWith('https://') &&
      !lowerCaseUrl.startsWith('//')
    );
  }

  private retryAuth(next: HttpHandler, req: HttpRequest<unknown>) {
    const auth = this.getAuth();
    return auth.refreshToken().pipe(
      concatMap(token =>
        next.handle(this.cloneReq(req, token)).pipe(
          catchError((error: HttpErrorResponse) => {
            if (this.isUnauthorized(error)) {
              auth.logout();
            }

            return throwError(error);
          }),
        ),
      ),
    );
  }

  private cloneReq(req: HttpRequest<unknown>, idToken: string) {
    return req.clone({
      setHeaders: {
        Authorization: `Bearer ${idToken}`,
      },
    });
  }

  private getAuth() {
    if (!this.auth) {
      this.auth = this.injector.get(AuthorizationStateService);
    }
    return this.auth;
  }

  private needAuthorization(
    apiAddress: string,
    req: HttpRequest<unknown>,
  ): boolean {
    if (!req.url.startsWith(apiAddress) || req.headers.get('Authorization')) {
      return false;
    }

    return !this.anonymousApis.some(api =>
      api instanceof RegExp ? api.test(req.url) : req.url.includes(api),
    );
  }

  private isUnauthorized(error: HttpErrorResponse) {
    return error?.status === 401;
  }
}
