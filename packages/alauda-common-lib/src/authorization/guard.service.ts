import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild } from '@angular/router';
import { map } from 'rxjs/operators';

import { AuthorizationStateService } from './state.service';

@Injectable({ providedIn: 'root' })
export class AuthorizationGuardService
  implements CanActivate, CanActivateChild {
  constructor(private readonly auth: AuthorizationStateService) {}

  canActivate() {
    return this.auth.getToken().pipe(map(token => !!token));
  }

  canActivateChild() {
    return this.canActivate();
  }
}
