export type StringMap = Record<string, string>;

export interface ResourceListParams extends StringMap {
  limit?: string;
  continue?: string;
  labelSelector?: string;
  fieldSelector?: string;
  field?: string;
  keyword?: string;
}
