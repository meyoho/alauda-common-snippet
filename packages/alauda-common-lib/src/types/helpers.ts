import { Observable } from 'rxjs';

export * from 'ts-essentials/dist/types';

export type Readonlyable<T> = T | Readonly<T>;

export type AnyArray<T> = Readonlyable<T[]>;

export type Arrayable<T, R extends boolean = false> = [R] extends [never]
  ? T | T[]
  : R extends true
  ? Readonly<T> | readonly T[]
  : R extends false
  ? Readonlyable<T> | AnyArray<T>
  : never;

export type Nullable<T> = T | null | undefined;

export type Callback<T extends any[] = unknown[], R = unknown> = (
  ...args: T
) => R;

/** Dark magic helper */
export type IfEqual<X, Y, A = X, B = never> = (<T>() => T extends X
  ? true
  : false) extends <T>() => T extends Y ? true : false
  ? A
  : B;

export type Keys<T> = { [Key in keyof T]: Key };

export type ObservableType<T> = T extends Observable<infer R> ? R : never;

export type UnaryObservableFunction<T, R> = (
  source: Observable<T>,
) => Observable<R>;

export { ValueOf as LooseValueOf } from 'ts-essentials';

export type ValueOf<T> = T extends {
  [key: string]: infer M;
}
  ? M
  : T extends {
      [key: number]: infer N;
    }
  ? N
  : never;
