import { StringMap } from './commons';

// TypeMeta describes an individual object in an API response or request
// with strings representing the type of the object and its API schema version.
// Structures that are versioned or persisted should inline TypeMeta.
export interface TypeMeta {
  kind?: string;
  apiVersion?: string;
}

export interface OwnerReference {
  apiVersion: string;
  kind: string;
  name: string;
  uid: string;
  controller?: boolean;
  blockOwnerDeletion?: boolean;
}

// ObjectMeta is metadata that all persisted resources must have, which includes all objects
export interface ObjectMeta {
  name?: string;
  namespace?: string;
  labels?: StringMap;
  annotations?: StringMap;
  finalizers?: string[];
  selfLink?: string;
  uid?: string;
  creationTimestamp?: string;
  deletionTimestamp?: string;
  ownerReferences?: OwnerReference[];
  resourceVersion?: string;
  generation?: number;
}

export interface KubernetesResource extends TypeMeta {
  metadata?: ObjectMeta;
  status?: unknown;
  spec?: unknown;
}

export interface KubernetesResourceList<
  T extends KubernetesResource = KubernetesResource
> extends TypeMeta {
  items: T[];
  metadata?: ObjectMeta & {
    continue?: string;
  };
}

export interface LabelSelector {
  matchLabels?: StringMap;
  matchExpressions?: LabelSelectorRequirement[];
}

export interface LabelSelectorRequirement {
  key: string;
  operator?: // tslint:disable-next-line: max-union-size
  '=' | '==' | '!=' | '!' | 'in' | 'notin' | 'exists' | 'doesnotexist';
  values?: string[];
}

export interface ScopedResourceSelectorRequirement {
  // tslint:disable-next-line: max-union-size
  operator: 'In' | 'NotIn' | 'Exists' | 'DoesNotExist';
  scopeName: string;
  values: string;
}

export interface ScopeSelector {
  matchExpressions: ScopedResourceSelectorRequirement[];
}

export enum K8sResourceAction {
  GET = 'get',
  LIST = 'list',
  WATCH = 'watch',
  CREATE = 'create',
  UPDATE = 'update',
  PATCH = 'patch',
  DELETE = 'delete',
  DELETE_COLLECTION = 'deletecollection',
  PROXY = 'proxy',
  REDIRECT = 'redirect',
  ALL = '*',
}

export interface APIResourceItem {
  name: string;
  singularName: string;
  namespaced: boolean;
  kind: string;
  verbs: K8sResourceAction[];
}

export interface APIResourceList extends KubernetesResource {
  kind: 'APIResourceList';
  apiVersion?: string;
  groupVersion: string;
  resources: APIResourceItem[];
}

export interface ResourceAttributes<T extends K8sResourceAction> {
  group: string;
  resource: string;
  verb: T;
  namespace?: string;
  name?: string;
}

export interface SubjectAccessReviewSpec<T extends K8sResourceAction> {
  resourceAttributes: ResourceAttributes<T>;
}

export interface SubjectAccessReviewStatus {
  allowed: boolean;
  denied?: boolean;
  evaluationError?: string;
  reason?: string;
}

export interface SubjectAccessReview<
  T extends K8sResourceAction = K8sResourceAction
> extends KubernetesResource {
  kind: 'SubjectAccessReview';
  spec: SubjectAccessReviewSpec<T>;
  status: SubjectAccessReviewStatus;
}

export interface SelfResourceAttributes<T extends K8sResourceAction>
  extends ResourceAttributes<T> {
  project?: string;
  cluster?: string;
}

export interface SelfSubjectAccessReviewSpec<T extends K8sResourceAction>
  extends SubjectAccessReviewSpec<T> {
  resourceAttributes: SelfResourceAttributes<T>;
}

export interface SelfSubjectAccessReview<
  T extends K8sResourceAction = K8sResourceAction
> extends KubernetesResource {
  kind: 'SelfSubjectAccessReview';
  spec: SelfSubjectAccessReviewSpec<T>;
  status: SubjectAccessReviewStatus;
}

export enum DependencyType {
  Any = 'any',
  All = 'all',
}

export enum FeatureStage {
  Alpha = 'Alpha',
  Beta = 'Beta',
  GA = 'GA',
  EOF = 'EOF',
}

export interface FeatureGate extends KubernetesResource {
  spec: {
    dependency: {
      type: DependencyType;
      featureGates: string[];
    };
    description: string;
    enabled: boolean;
    stage: FeatureStage;
  };
  status: {
    enabled: boolean;
  };
}

export interface Status extends TypeMeta {
  kind: 'Status';
  apiVersion: string;
  metadata: {
    continue?: string;
  };
  status: string;
  message: string;
  reason: string;
  code: number;
}
