import { BehaviorSubject } from 'rxjs';

import { ObservableType } from '../types/public-api';

const checkDescriptor = <T, K extends keyof T>(target: T, propertyKey: K) => {
  const oDescriptor = Object.getOwnPropertyDescriptor(target, propertyKey);

  if (oDescriptor && !oDescriptor.configurable) {
    throw new TypeError(`property ${propertyKey} is not configurable`);
  }

  return {
    oGetter: oDescriptor?.get,
    oSetter: oDescriptor?.set,
    oDescriptor,
  };
};

const strictCheckDescriptor = <T, K extends keyof T>(
  target: T,
  propertyKey: K,
) => {
  const { oGetter, oSetter, oDescriptor } = checkDescriptor(
    target,
    propertyKey,
  );

  if (oGetter || oSetter) {
    throw new TypeError(
      `property ${propertyKey} should not define getter or setter`,
    );
  }

  return oDescriptor;
};

export function ValueHook<T = any, K extends keyof T = any>(
  setter?: (this: T, value?: T[K]) => boolean | void,
  getter?: (this: T, value?: T[K]) => T[K],
) {
  return (target: T, propertyKey: K, _parameterIndex?: number) => {
    const { oGetter, oSetter } = checkDescriptor(target, propertyKey);

    const symbol = Symbol(propertyKey as string);

    type Mixed = T & {
      [symbol]: T[K];
    };

    Object.defineProperty(target, propertyKey, {
      enumerable: true,
      configurable: true,
      get(this: Mixed) {
        return getter
          ? getter.call(this, this[symbol])
          : oGetter
          ? oGetter.call(this)
          : this[symbol];
      },
      set(this: Mixed, value: T[K]) {
        if (
          value === this[propertyKey] ||
          setter?.call(this, value) === false
        ) {
          return;
        }
        if (oSetter) {
          oSetter.call(this, value);
        }
        this[symbol] = value;
      },
    });
  };
}

export function ObservableInput<
  T = any,
  OK extends keyof T = any,
  K extends keyof T = any
>(propertyKey?: K | boolean, initialValue?: ObservableType<T[OK]>) {
  return (target: T, oPropertyKey: OK) => {
    if (!(oPropertyKey as string).endsWith('$')) {
      throw new TypeError(
        `property ${oPropertyKey} should be an Observable and its name should end with $`,
      );
    }

    strictCheckDescriptor(target, oPropertyKey);

    const symbol = Symbol(oPropertyKey as string);

    type OT = ObservableType<T[OK]>;

    type Mixed = T & {
      [symbol]: BehaviorSubject<OT>;
    } & Record<OK, BehaviorSubject<OT>>;

    // eslint-disable-next-line prefer-const
    let oPropertyValue: OT;

    Object.defineProperty(target, oPropertyKey, {
      enumerable: true,
      configurable: true,
      get(this: Mixed) {
        return (
          this[symbol] ||
          (this[symbol] = new BehaviorSubject(
            // when no initialValue passed in, use the original property val
            initialValue === undefined ? oPropertyValue : initialValue,
          ))
        );
      },
      set(this: Mixed, value: OT) {
        this[oPropertyKey].next(value);
      },
    });

    if (!propertyKey) {
      return;
    }

    if (propertyKey === true) {
      propertyKey = (oPropertyKey as string).replace(/\$+$/, '') as K;
    }

    const oDescriptor = strictCheckDescriptor(target, propertyKey);
    oPropertyValue = oDescriptor ? oDescriptor.value : target[propertyKey];

    Object.defineProperty(target, propertyKey, {
      enumerable: true,
      configurable: true,
      get(this: Mixed) {
        return this[oPropertyKey].getValue();
      },
      set(this: Mixed, value: OT) {
        this[oPropertyKey].next(value);
      },
    });
  };
}
