import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { filter as _filter, get } from 'lodash-es';
import { Observable, Subject, identity, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  exhaustMap,
  filter,
  groupBy,
  map,
  mergeMap,
  retry,
  startWith,
  tap,
  timeout,
} from 'rxjs/operators';
import { Dictionary } from 'ts-essentials';

import { FeatureGate, KubernetesResourceList } from '../types/public-api';

import { API_GATEWAY } from './constants';
import { K8sUtilService } from './k8s-util.service';
import { publishRef } from './operators';

const GLOBAL_CLUSTER = '@global-cluster';
const RETRY_COUNT = 3;
const TIMEOUT = 10 * 1000;

@Injectable({ providedIn: 'root' })
export class FeatureGateService implements OnDestroy {
  constructor(
    private readonly http: HttpClient,
    private readonly k8sUtils: K8sUtilService,
  ) {}

  private settings: Dictionary<{
    cluster: string;
    error: any;
    enabled: Dictionary<boolean>;
  }> = {};

  private readonly requestFeatureGates$ = new Subject<string>();

  private readonly receivedFeatureGates$ = this.requestFeatureGates$.pipe(
    groupBy(identity),
    mergeMap(group$ =>
      group$.pipe(
        exhaustMap(cluster => {
          const url =
            cluster === GLOBAL_CLUSTER
              ? `${API_GATEWAY}/fg/v1/featuregates`
              : `${API_GATEWAY}/fg/v1/${cluster}/featuregates`;

          return this.http.get<KubernetesResourceList<FeatureGate>>(url).pipe(
            retry(RETRY_COUNT),
            timeout(TIMEOUT),
            map(list => ({
              cluster,
              items: list.items || ([] as FeatureGate[]),
              error: null,
            })),
            catchError(error =>
              of({ cluster, items: [] as FeatureGate[], error }),
            ),
          );
        }),
      ),
    ),
    tap(({ cluster, items, error }) => {
      this.settings = {
        ...this.settings,
        [cluster]: {
          cluster,
          error,
          enabled: items.reduce((accum, item) => {
            return {
              ...accum,
              [this.k8sUtils.getName(item)]: get(
                item,
                ['status', 'enabled'],
                false,
              ),
            };
          }, {} as Dictionary<boolean>),
        },
      };
    }),
    debounceTime(500), // debounce for short request interval
    publishRef(),
  );

  subscription = this.receivedFeatureGates$.subscribe();

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private isLoaded(cluster: string) {
    return !!this.settings[cluster];
  }

  private _isEnabled(gate: string, cluster: string): boolean {
    if (!gate) {
      return true;
    }

    const clusterSettings = this.settings[cluster];
    return !clusterSettings.error && clusterSettings.enabled[gate];
  }

  /**
   * check gate status
   * @param gate: specified feature gate
   * @param cluster: check gate for specified cluster
   */
  isEnabled(gate: string, cluster = ''): Observable<boolean> {
    const clusterKey = cluster || GLOBAL_CLUSTER;

    if (this.isLoaded(clusterKey)) {
      return of(this._isEnabled(gate, clusterKey));
    }

    this.requestFeatureGates$.next(clusterKey);

    return this.receivedFeatureGates$.pipe(
      filter(({ cluster: key }) => key === clusterKey),
      map(() => this._isEnabled(gate, clusterKey)),
      distinctUntilChanged(),
    );
  }

  /**
   * filter list with gate, send items without feature gate as begin push (can skip by passing `skipNoFeatureGatePush` argument with true)
   * @param items: list need filter
   * @param gateAccessor: how to get gate from item
   * @param cluster: check gate for specified cluster
   * @param filterMethod: filter method, default to list filter, can filter deep with createRecursiveFilter or totally custom method.
   * @param skipNoFeatureGatePush: skip begin push , default false.
   */
  filterEnabled<T>(
    items: T[],
    gateAccessor: (item: T) => string,
    cluster = '',
    filterMethod: (
      items: T[],
      condition: (item: T) => boolean,
    ) => T[] = _filter,
    skipNoFeatureGatePush = false,
  ): Observable<T[]> {
    const clusterKey = cluster || GLOBAL_CLUSTER;

    if (this.isLoaded(clusterKey)) {
      return of(
        filterMethod(items, item =>
          this._isEnabled(gateAccessor(item), clusterKey),
        ),
      );
    }

    this.requestFeatureGates$.next(clusterKey);

    const filterd = this.receivedFeatureGates$.pipe(
      filter(({ cluster: key }) => key === clusterKey),
      map(() =>
        filterMethod(items, item =>
          this._isEnabled(gateAccessor(item), clusterKey),
        ),
      ),
      distinctUntilChanged(),
    );

    if (skipNoFeatureGatePush) {
      return filterd;
    }

    return filterd.pipe(
      startWith(filterMethod(items, item => !gateAccessor(item))),
    );
  }

  /**
   * refetch cached feature gate settings
   * @param cluster: specified cluster, refetch all already cached settings when cluster is empty
   */
  refetchCache(cluster = '') {
    const expiredClusters = cluster ? [cluster] : Object.keys(this.settings);

    expiredClusters.forEach(item => {
      this.requestFeatureGates$.next(item);
    });
  }
}
