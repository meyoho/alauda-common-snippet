import {
  ChangeDetectorRef,
  Directive,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewContainerRef,
  ɵstringify as stringify,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import { FeatureGateService } from './feature-gate.service';

/**
 * @example <ng-container *aclFeatureGate="gate-name"></ng-container>
 */
@Directive({ selector: '[aclFeatureGate]' })
export class FeatureGateDirective implements OnInit, OnDestroy {
  @Input('aclFeatureGate')
  featureName: string = null;

  @Input('aclFeatureGateCluster')
  cluster: string = null;

  @Input('aclFeatureGateThen')
  set then(templateRef: TemplateRef<void>) {
    assertTemplate('aclFeatureGateThen', templateRef);
    this.thenTemplateRef = templateRef;
  }

  @Input('aclFeatureGateElse')
  set else(templateRef: TemplateRef<void>) {
    assertTemplate('aclVersionGateElse', templateRef);
    this.elseTemplateRef = templateRef;
  }

  private thenTemplateRef: TemplateRef<void> = null;
  private elseTemplateRef: TemplateRef<void> = null;

  private subscription: Subscription = null;

  constructor(
    private readonly container: ViewContainerRef,
    private readonly featureGate: FeatureGateService,
    private readonly crd: ChangeDetectorRef,
    templateRef: TemplateRef<void>,
  ) {
    this.thenTemplateRef = templateRef;
  }

  ngOnInit() {
    if (!this.featureName) {
      this.container.createEmbeddedView(this.thenTemplateRef);
      return;
    }

    this.subscription = this.featureGate
      .isEnabled(this.featureName, this.cluster)
      .pipe(distinctUntilChanged())
      .subscribe(enabled => {
        this.container.clear();
        if (enabled) {
          this.container.createEmbeddedView(this.thenTemplateRef);
        } else if (this.elseTemplateRef) {
          this.container.createEmbeddedView(this.elseTemplateRef);
        }
        this.crd.markForCheck();
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

function assertTemplate(
  property: string,
  templateRef: TemplateRef<any> | null,
): void {
  const isTemplateRefOrNull = !!(
    !templateRef || templateRef.createEmbeddedView
  );
  if (!isTemplateRefOrNull) {
    throw new Error(
      `${property} must be a TemplateRef, but received '${stringify(
        templateRef,
      )}'.`,
    );
  }
}
