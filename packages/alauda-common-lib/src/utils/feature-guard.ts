import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { FeatureGateService } from './feature-gate.service';
const DEFAULT_ERROR_GUARD_URL = 'feature-guard-error';

/**
 * ```ts
 * data: {
 *  path: 'home',
 *  canActivate: [ FeatureGuard ],
 *  data: { featureName: 'ace3', guard_url: 'xxx', logTitle: 'devops'  }
 * }
 * ```
 */
@Injectable({ providedIn: 'root' })
export class FeatureGuard implements CanActivate, CanActivateChild {
  constructor(
    private readonly featureGate: FeatureGateService,
    private readonly router: Router,
  ) {}

  canActivateChild(childRoute: ActivatedRouteSnapshot): Observable<boolean> {
    return this.canActivate(childRoute);
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const data = {
      featureName: '',
      guard_url: DEFAULT_ERROR_GUARD_URL,
      logTitle: '',
      ...route.data,
    };
    return this.featureGate.isEnabled(data.featureName).pipe(
      tap(enabled => {
        if (enabled) return;
        const logTitle = data.logTitle ? `/${data.logTitle}` : '';
        this.router.navigateByUrl(
          `${data.guard_url}/${data.featureName}${logTitle}`,
          { replaceUrl: true },
        );
      }),
    );
  }
}
