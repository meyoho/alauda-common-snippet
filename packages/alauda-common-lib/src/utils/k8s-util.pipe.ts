// tslint:disable: max-classes-per-file
import { Pipe, PipeTransform } from '@angular/core';

import { Readonlyable } from '../types/helpers';
import { KubernetesResource } from '../types/k8s';

import { BaseNormalizeTypeParams, K8sUtilService } from './k8s-util.service';

@Pipe({ name: 'aclAnnotation' })
export class K8sAnnotationPipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(
    resource: KubernetesResource,
    type: string | BaseNormalizeTypeParams,
    prefix?: string,
  ): string {
    return this.k8sUtil.getAnnotation(resource, type as string, prefix);
  }
}

@Pipe({ name: 'aclNamespace' })
export class K8sNamespacePipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(resource: KubernetesResource): string {
    return this.k8sUtil.getNamespace(resource);
  }
}

@Pipe({ name: 'aclName' })
export class K8sNamePipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(resource: KubernetesResource): string {
    return this.k8sUtil.getName(resource);
  }
}

@Pipe({ name: 'aclCreationTimestamp' })
export class K8sCreationTimestampPipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(resource: KubernetesResource): string {
    return this.k8sUtil.getCreationTimestamp(resource);
  }
}

@Pipe({ name: 'aclDisplayName' })
export class K8sDisplayNamePipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(resource: KubernetesResource, prefix?: string): string {
    return this.k8sUtil.getDisplayName(resource, prefix);
  }
}

@Pipe({ name: 'aclUnionDisplayName' })
export class K8sUnionDisplayNamePipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(
    resource: KubernetesResource,
    prefix?: string,
    wrapper?: Readonlyable<[string, string]>,
    fallback?: string,
  ): string {
    return this.k8sUtil.getUnionDisplayName(
      resource,
      prefix,
      wrapper,
      fallback,
    );
  }
}

@Pipe({ name: 'aclDescription' })
export class K8sDescriptionPipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(resource: KubernetesResource, prefix?: string): string {
    return this.k8sUtil.getDescription(resource, prefix);
  }
}

@Pipe({ name: 'aclCreator' })
export class K8sCreatorPipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(resource: KubernetesResource, prefix?: string): string {
    return this.k8sUtil.getCreator(resource, prefix);
  }
}

@Pipe({ name: 'aclUpdatedAt' })
export class K8sUpdatedAtPipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(resource: KubernetesResource, prefix?: string): string {
    return this.k8sUtil.getUpdatedAt(resource, prefix);
  }
}

@Pipe({ name: 'aclLabel' })
export class K8sLabelPipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(
    resource: KubernetesResource,
    type: string | BaseNormalizeTypeParams,
    prefix?: string,
  ): string {
    return this.k8sUtil.getLabel(resource, type as string, prefix);
  }
}

@Pipe({ name: 'aclProject' })
export class K8sProjectPipe implements PipeTransform {
  constructor(private readonly k8sUtil: K8sUtilService) {}

  transform(resource: KubernetesResource): string {
    return this.k8sUtil.getProject(resource);
  }
}
