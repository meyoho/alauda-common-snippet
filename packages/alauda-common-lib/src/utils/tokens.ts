import { InjectionToken } from '@angular/core';

import { K8sResourceDefinitions } from '../api/types';
import { StringMap } from '../types/commons';

import { getBaseHref } from './helpers';

export const idTokenKey = 'id_token';

export const refreshTokenKey = 'refresh_token';

export const codeKey = 'code';

export const appEntry = '/';

export const anonymousApis: Array<string | RegExp> = [];

export const modulesApi = 'api/v1/modules';

export const tokenLoginApi = 'api/v1/token/login';

export const tokenCallbackApi = 'api/v1/token/callback';

export const tokenRefreshApi = 'api/v1/token/refresh';

export const tokenInfoApi = 'api/v1/token/info';

export const tokenNonAdminRedirectPath = '/';

export const baseDomain = 'alauda.io';

export const globalNamespace = 'alauda-system';

export const resourceApiPrefixes: StringMap = {};

export const resourceDefinitions: K8sResourceDefinitions = {};

export const ANONYMOUS_APIS = new InjectionToken(
  'anonymous apis no need for authorization',
  {
    providedIn: 'root',
    factory: () => anonymousApis,
  },
);

export const ID_TOKEN_KEY = new InjectionToken(
  'id_token param name for authorization',
  {
    providedIn: 'root',
    factory: () => idTokenKey,
  },
);

export const REFRESH_TOKEN_KEY = new InjectionToken(
  'refresh_token param name for authorization',
  {
    providedIn: 'root',
    factory: () => refreshTokenKey,
  },
);

export const CODE_KEY = new InjectionToken(
  'code param name fro authorization',
  {
    providedIn: 'root',
    factory: () => codeKey,
  },
);

export const APP_ENTRY = new InjectionToken('app entry', {
  providedIn: 'root',
  factory: () => appEntry,
});

export const MODULES_API = new InjectionToken('api gateway configurations', {
  providedIn: 'root',
  factory: () => modulesApi,
});

export const TOKEN_LOGIN_API = new InjectionToken(
  'api for get authorization configurations',
  {
    providedIn: 'root',
    factory: () => tokenLoginApi,
  },
);

export const TOKEN_CALLBACK_API = new InjectionToken(
  'api for resolve id_token from code',
  {
    providedIn: 'root',
    factory: () => tokenCallbackApi,
  },
);

export const TOKEN_REFRESH_API = new InjectionToken(
  'api for refresh id_token by refresh_token',
  {
    providedIn: 'root',
    factory: () => tokenRefreshApi,
  },
);

export const TOKEN_INFO_API = new InjectionToken('api for validate id_token', {
  providedIn: 'root',
  factory: () => tokenInfoApi,
});

export const TOKEN_NON_ADMIN_REDIRECT_PATH = new InjectionToken(
  'default redirect path for admin guard',
  {
    providedIn: 'root',
    factory: () => tokenNonAdminRedirectPath,
  },
);

export const TOKEN_BASE_DOMAIN = new InjectionToken(
  'default base domain string for annotations and labels of kubernetes resource',
  {
    providedIn: 'root',
    factory: () => baseDomain,
  },
);

export const TOKEN_GLOBAL_NAMESPACE = new InjectionToken(
  'default global namespace for namespaced global resource',
  {
    providedIn: 'root',
    factory: () => globalNamespace,
  },
);

/**
 * @deprecated Please use `TOKEN_RESOURCE_DEFINITIONS` instead
 */
export const TOKEN_RESOURCE_API_PREFIXES = new InjectionToken(
  'custom resource api prefixes for getting resource group name',
  {
    providedIn: 'root',
    factory: () => resourceApiPrefixes,
  },
);

export const TOKEN_RESOURCE_DEFINITIONS = new InjectionToken(
  'custom resource definitions',
  {
    providedIn: 'root',
    factory: () => resourceDefinitions,
  },
);

export const TOKEN_BASE_HREF = new InjectionToken<string>(
  'base href from DOM',
  {
    providedIn: 'root',
    factory: getBaseHref,
  },
);
