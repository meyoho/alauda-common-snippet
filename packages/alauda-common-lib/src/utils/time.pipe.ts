import {
  ChangeDetectorRef,
  Injectable,
  OnDestroy,
  Pipe,
  PipeTransform,
} from '@angular/core';
import dayjs, { ConfigType } from 'dayjs';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { TranslateService } from '../translate/public-api';

import { FIELD_NOT_AVAILABLE_PLACEHOLDER } from './field-not-available.pipe';
import { DATE_FORMAT, DATE_TIME_FORMAT, TimeService } from './time.service';

const MILLISECONDS_SECOND = 1000;
const MILLISECONDS_MINUTE = MILLISECONDS_SECOND * 60;
const MILLISECONDS_HOUR = MILLISECONDS_MINUTE * 60;
const MILLISECONDS_DAY = MILLISECONDS_HOUR * 24;
const MILLISECONDS_WEEK = MILLISECONDS_DAY * 7;

@Pipe({ name: 'aclRelativeTime', pure: false })
export class RelativeTimePipe implements PipeTransform, OnDestroy {
  private readonly destroy$$ = new Subject<void>();

  private timerId: number | NodeJS.Timeout;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly time: TimeService,
    private readonly translate: TranslateService,
  ) {
    this.translate.locale$
      .pipe(takeUntil(this.destroy$$))
      .subscribe(() => this.cdr.markForCheck());
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();

    if (this.timerId) {
      clearTimeout(this.timerId as number);
    }
  }

  transform(value: ConfigType) {
    if (!value || !value.toString().trim()) {
      return FIELD_NOT_AVAILABLE_PLACEHOLDER;
    }

    const currentTime = Date.now();
    const givenTime = dayjs(value).valueOf();

    const distance = currentTime - givenTime;

    if (distance >= MILLISECONDS_WEEK) {
      return this.time.format(value, DATE_FORMAT);
    }

    this.checkDistance(distance);

    return this.time.distance(givenTime, currentTime);
  }

  private checkDistance(distance: number) {
    if (distance < MILLISECONDS_MINUTE) {
      return this.startTimer(MILLISECONDS_SECOND);
    }

    if (distance < MILLISECONDS_HOUR) {
      return this.startTimer(MILLISECONDS_MINUTE);
    }

    if (distance < MILLISECONDS_DAY) {
      return this.startTimer(MILLISECONDS_HOUR);
    }

    return this.startTimer(MILLISECONDS_DAY);
  }

  private startTimer(timeout: number) {
    clearTimeout(this.timerId as number);
    this.timerId = setTimeout(() => this.cdr.markForCheck(), timeout);
  }
}

@Injectable()
// tslint:disable-next-line: use-pipe-decorator
export abstract class DateTimePipe implements PipeTransform {
  abstract format: string;

  constructor(private readonly time: TimeService) {}

  transform(value: ConfigType) {
    if (!value || !value.toString().trim()) {
      return FIELD_NOT_AVAILABLE_PLACEHOLDER;
    }

    return this.time.format(value, this.format);
  }
}

@Pipe({ name: 'aclStandardTime' })
export class StandardTimePipe extends DateTimePipe implements PipeTransform {
  format = DATE_TIME_FORMAT;
}

@Pipe({ name: 'aclStandardDate' })
export class StandardDatePipe extends DateTimePipe implements PipeTransform {
  format = DATE_FORMAT;
}
