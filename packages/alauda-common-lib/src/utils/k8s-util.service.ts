import { DialogService, MessageService } from '@alauda/ui';
import { Injectable, Injector, Type } from '@angular/core';
import { get } from 'lodash-es';
import { EMPTY, Observable, from, throwError } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';

import { K8sApiService } from '../api/k8s-api.service';
import { ResourceWriteParams } from '../api/types';
import { TranslateService } from '../translate/translate.service';
import { Readonlyable } from '../types/helpers';
import { KubernetesResource, Status } from '../types/k8s';

import { SPACE } from './constants';
import { ifExist, wrapText } from './helpers';
import { TOKEN_BASE_DOMAIN } from './tokens';

export const METADATA = 'metadata';
export const NAMESPACE = 'namespace';
export const NAME = 'name';
export const CREATION_TIMESTAMP = 'creationTimestamp';
export const ANNOTATIONS = 'annotations';
export const LABELS = 'labels';
export const DISPLAY_NAME = 'display-name';
export const DESCRIPTION = 'description';
export const CREATOR = 'creator';
export const UPDATED_AT = 'updated-at';
export const PROJECT = 'project';
export const CLUSTER = 'cluster';

export interface BaseNormalizeTypeParams {
  type: string;
  baseDomain: string;
  prefix?: string;
}

@Injectable({
  providedIn: 'root',
})
export class K8sUtilService<R extends string = string> {
  protected dialog: DialogService;
  protected message: MessageService;
  protected k8sApi: K8sApiService<R>;
  protected translate: TranslateService;
  protected baseDomain: string;

  constructor(protected injector: Injector) {
    this.dialog = injector.get(DialogService as Type<DialogService>);
    this.message = injector.get(MessageService as Type<MessageService>);
    this.k8sApi = injector.get(K8sApiService as Type<K8sApiService>);
    this.translate = injector.get(TranslateService as Type<TranslateService>);
    this.baseDomain = injector.get(TOKEN_BASE_DOMAIN);
  }

  /**
   * @param type 对应资源的翻译 key
   */
  deleteResource<T extends KubernetesResource>(
    params: ResourceWriteParams<R, T>,
    type: string,
  ) {
    return from(
      this.dialog.confirm({
        title: this.translate.get(`delete_${type}`),
        content:
          this.translate.get('delete_confirm_resource', {
            type: this.translate.get(type).toLowerCase(),
            name: this.getName(params.resource),
          }) + this.translate.get('delete_not_recoverable'),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      }),
    ).pipe(
      catchError(() => EMPTY),
      switchMap(() =>
        // @ts-ignore
        (this.k8sApi[
          'cluster' in params && params.cluster
            ? 'deleteResource'
            : 'deleteGlobalResource'
        ](params) as Observable<T>).pipe(
          tap(() => this.message.success(this.translate.get('delete_success'))),
          catchError(() => EMPTY),
        ),
      ),
    );
  }

  updateResource<T extends KubernetesResource>({
    type,
    cluster,
    resource,
  }: {
    type: R;
    cluster: string;
    resource: T;
  }): Observable<T> {
    const put = (res: T) =>
      this.k8sApi.putResource({
        type,
        cluster,
        resource: res,
      });
    const get = () =>
      this.k8sApi.getResource<T>({
        type,
        cluster,
        namespace: resource.metadata.namespace,
        name: resource.metadata.name,
      });

    return this.retryUpdate(put, get, resource);
  }

  updateGlobalResource<T extends KubernetesResource>({
    type,
    namespaced,
    resource,
  }: {
    type: R;
    namespaced: boolean;
    resource: T;
  }): Observable<T> {
    const put = (res: T) =>
      this.k8sApi.putGlobalResource({
        type: type,
        namespaced,
        resource: res,
      });
    const get = () =>
      this.k8sApi.getGlobalResource<T>({
        type: type,
        namespaced,
        name: resource.metadata.name,
      });

    return this.retryUpdate(put, get, resource);
  }

  // reference: http://confluence.alaudatech.com/pages/viewpage.action?pageId=27166946
  normalizeType(type: string, prefix?: string): string;
  normalizeType(params: BaseNormalizeTypeParams): string;
  normalizeType(
    typeOrParams: string | BaseNormalizeTypeParams,
    prefix?: string,
  ): string {
    if (typeof typeOrParams === 'string') {
      return `${ifExist(prefix, prefix + '.')}${
        this.baseDomain
      }/${typeOrParams}`;
    }

    return `${ifExist(typeOrParams.prefix, typeOrParams.prefix + '.')}${
      typeOrParams.baseDomain
    }/${typeOrParams.type}`;
  }

  getAnnotation<T extends KubernetesResource>(
    resource: T,
    type: string,
    prefix?: string,
  ): string;

  getAnnotation<T extends KubernetesResource>(
    resource: T,
    params: BaseNormalizeTypeParams,
  ): string;

  getAnnotation<T extends KubernetesResource>(
    resource: T,
    paramsOrType: string | BaseNormalizeTypeParams,
    prefix?: string,
  ): string {
    return get(resource, [
      METADATA,
      ANNOTATIONS,
      this.normalizeType(paramsOrType as string, prefix),
    ]);
  }

  getNamespace<T extends KubernetesResource>(resource: T) {
    return get(resource, [METADATA, NAMESPACE]);
  }

  getName<T extends KubernetesResource>(resource: T) {
    return get(resource, [METADATA, NAME]);
  }

  getCreationTimestamp<T extends KubernetesResource>(resource: T) {
    return get(resource, [METADATA, CREATION_TIMESTAMP]);
  }

  getDisplayName<T extends KubernetesResource>(resource: T, prefix?: string) {
    return this.getAnnotation(resource, DISPLAY_NAME, prefix);
  }

  getUnionDisplayName<T extends KubernetesResource>(
    resource: T,
    prefix?: string,
    wrapper?: Readonlyable<[string, string]>,
    fallback?: string,
  ) {
    return (
      resource &&
      this.getName(resource) +
        SPACE +
        wrapText(this.getDisplayName(resource, prefix), wrapper, fallback)
    );
  }

  getDescription<T extends KubernetesResource>(resource: T, prefix?: string) {
    return this.getAnnotation(resource, DESCRIPTION, prefix);
  }

  getCreator<T extends KubernetesResource>(resource: T, prefix?: string) {
    return this.getAnnotation(resource, CREATOR, prefix);
  }

  getUpdatedAt<T extends KubernetesResource>(resource: T, prefix?: string) {
    return this.getAnnotation(resource, UPDATED_AT, prefix);
  }

  getLabel<T extends KubernetesResource>(
    resource: T,
    type: string,
    prefix?: string,
  ): string;

  getLabel<T extends KubernetesResource>(
    resource: T,
    params: BaseNormalizeTypeParams,
  ): string;

  getLabel<T extends KubernetesResource>(
    resource: T,
    paramsOrType: string | BaseNormalizeTypeParams,
    prefix?: string,
  ): string {
    return get(resource, [
      METADATA,
      LABELS,
      this.normalizeType(paramsOrType as string, prefix),
    ]);
  }

  getProject<T extends KubernetesResource>(resource: T) {
    return this.getLabel(resource, PROJECT);
  }

  private retryUpdate<T extends KubernetesResource>(
    put: (resource: T) => Observable<T>,
    get: () => Observable<T>,
    resource: T,
  ): Observable<T> {
    const waitUserConfirmRetry = (err: Status) =>
      from(
        this.dialog.confirm({
          title: this.translate.get('resource_version_conflict'),
          content: this.translate.get('confirm_retry_with_latest_version'),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
        }),
      ).pipe(
        switchMap(() =>
          get().pipe(
            switchMap(newResource => {
              return put({
                ...resource,
                metadata: {
                  ...resource.metadata,
                  resourceVersion: newResource.metadata.resourceVersion,
                },
              });
            }),
            catchError(() => {
              return this.retryUpdate(put, get, resource);
            }),
          ),
        ),
        catchError(() => {
          return throwError(err);
        }),
      );

    return put(resource).pipe(
      catchError((err: Status) => {
        if (err.code !== 409) {
          return throwError(err);
        } else {
          return waitUserConfirmRetry(err);
        }
      }),
    );
  }
}
