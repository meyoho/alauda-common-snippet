# Utils 工具包

## Functions

### `createRecursiveFilter` 创建自定义深层过滤方法，此方法对数据结构不做假设，详见源码`create-recursive-filter.ts`

## Decorators

### `ValueHook`

快速自定义属性 `setter`/`getter` 拦截器

### `ObservableInput`

自动将 `Input` 转化为 `Observable`

参见：[TypeScript 实践：自定义装饰器拦截 Angular Input 转化为 Observable](http://confluence.alauda.cn/pages/viewpage.action?pageId=48728620)

## Operators

### `publishRef`

简单包装一下 `publishReplay` + `refCount` 减少样板代码

### `skipError`

包装 `catchError` 操作符，用于直接映射错误

### `catchPromise`

处理 `Promise` `rejected` 状态无法被捕获的情况

## Services

### `K8sUtilService`

简化 K8s 资源操作的公共服务

### `TimeService`

时间处理公共服务

### `FeatureGateService`

功能开关公共服务

[相关文档](http://confluence.alauda.cn/pages/viewpage.action?pageId=54853992)

- `isEnabled()` 检查功能开关是否启用

  参数:

  - `gate: string` 指定的功能开关

  - `cluster: string` 在指定集群检查功能开关，为空时在 global 集群检查功能开关

- `filterEnabled<T>()` 按功能开关过滤列表，可通过自定义过滤方法做深层过滤

  参数:

  - `items: T[]` 需要过滤的列表

  - `gateAccessor: (item: T) => string` 指定如何从列表元素中获取对应的功能开关

  - `cluster: string` 在指定集群检查功能开关，为空时在 global 集群检查功能开关

  - `filterMethod: (items: T[], condition: (item: T) => boolean) => T[]` 过滤方法，默认为列表过滤，可以自定义为深层过滤方法，比如通过`createRecursiveFilter`

## Directives

### `aclFeatureGate`

功能开关指令，用于简化模板中单一功能的开关

```html
<ng-container *aclFeatureGate="devops-app"></ng-container>
```

## Pipes

### `K8sUtilPipe`

基于 `K8sUtilService` 提供的各种 K8s 资源管道

### `TimePipe`

基于 `TimeService` 提供的各种时间处理管理

### `PurePipe`

使用 `pure` 管道包装 `mapper` 处理输入值

### `FeatureGuard`

功能开关路由守卫

[相关文档](http://confluence.alauda.cn/pages/viewpage.action?pageId=61928856#id-%E4%BA%A7%E5%93%81%E9%80%9A%E7%94%A8%E7%A9%BA%E9%A1%B5%E9%9D%A2%E8%A7%84%E5%88%99-5%E5%8A%9F%E8%83%BD%E5%BC%80%E5%85%B3%E6%9C%AA%E5%90%AF%E7%94%A8)

> 当功能开关未启用，直接输入 url 进入，重定向到空页面，提示未启用
> 整个模块的功能开关，当关闭时通过 url 访问重定向到空页面（整个模块需要和左导航保持一致）

业务中使用：

- 在功能开关模块路由添加 `guard`,`data` 中传入 `featureName` 功能开关名称。
- 默认跳转路径为 `feature-guard-error`,如需自定义跳转地址可传入 `guard_url: 'xxxxx'`
- 为解决在 `underlord` 不同平台展示对应名称，添加 `logTitle` 参数，对应路由 `feature-guard-error/:name/:logTitle`
- 各产品需要自己添加一个 `feature-guard-error` 组件，内部直接使用 `acl-page-guard` 展示空白页提示(路由需要添加参数用来展示功能开关名称 例：`path: 'feature-guard-error/:name'` )

```ts
// ...
import {FeatureGuard } from '@alauda/common-snippet';
@NgModule({
  imports: [
    RouterModule.forRoot([
      // ...
      {
        path: 'home',
        canActivate: [ FeatureGuard ],
        data: { featureName: 'ace3'},
        // ...
      },
      {
        path: 'feature-guard-error/:name',
        component: FeatureGuardErrorComponent,
      },
    ]),
  ],
})
// ...
```
