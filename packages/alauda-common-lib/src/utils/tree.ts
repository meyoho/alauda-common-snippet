/* eslint-disable unicorn/consistent-function-scoping */
// reduceTree :: String -> (a -> Tree -> [Tree] -> [Number] -> a) -> a -> [Tree] -> a
export const reduceTrees = (subtreeKey: string) => <T extends Tree, R>(
  fn: TreeReducer<T, R>,
) => (seed: R) => (trees: T[]) => {
  const reducer = (nodes: T[], parents: T[], path: number[], acc: R): R => {
    return nodes.reduce((prev, curr, index) => {
      const result = fn(prev, curr, parents, path.concat(index));
      if (curr[subtreeKey]) {
        return reducer(
          curr[subtreeKey] as T[],
          parents.concat(curr),
          path.concat(index),
          result,
        );
      } else {
        return result;
      }
    }, acc);
  };
  return reducer(trees, [], [], seed);
};

// mapTree :: String -> (Tree -> [Tree] -> [Number] -> Tree) -> [Tree] -> [Tree]
export const mapTrees = (subtreeKey: string) => <
  T extends Tree,
  R extends Tree
>(
  fn: TreeMapper<T, R>,
) => (trees: T[]) => {
  const mapper = (acc: R[], curr: T, parents: T[], path: number[]) => {
    const mapped = fn(curr, parents, path);
    return setNode<R>(subtreeKey, path, acc, mapped);
  };
  return reduceTrees(subtreeKey)<T, R[]>(mapper)([])(trees);
};

export const filterTrees = (subtreeKey: string) => <T extends Tree>(
  fn: TreeFilter<T>,
) => (trees: T[]) => {
  const filter = (nodes: T[], parents: T[], path: number[]): T[] => {
    return nodes
      .filter(node => fn(node, parents, path))
      .map((node, index) => {
        if (node[subtreeKey]) {
          return {
            ...node,
            [subtreeKey]: filter(
              node[subtreeKey] as T[],
              parents.concat(node),
              path.concat(index),
            ),
          };
        } else {
          return node;
        }
      });
  };
  return filter(trees, [], []);
};

export const findPath = (subtreeKey: string) => <T extends Tree>(
  fn: TreeFilter<T>,
) => (trees: T[]) => {
  const endPoint: T[] = [];
  const finder = (nodes: T[], parents: T[], path: number[]): boolean => {
    const result = nodes.find((node, index) => {
      if (fn(node, parents, path)) {
        return true;
      } else if (node[subtreeKey]) {
        return finder(
          node[subtreeKey] as T[],
          parents.concat(node),
          path.concat(index),
        );
      } else {
        return false;
      }
    });
    if (result) {
      endPoint.unshift(result);
    }
    return !!result;
  };

  finder(trees, [], []);
  return endPoint;
};

export const firstPath = (subtreeKey: string) => <T extends Tree>(
  trees: T[],
): T[] => {
  const head = trees[0];
  if (head) {
    return head.children
      ? [head, ...firstPath(subtreeKey)<T>(head[subtreeKey] as T[])]
      : [head];
  } else {
    return [];
  }
};

export function setNode<T extends Tree>(
  subtreeKey: string,
  path: number[],
  source: T[],
  target: T,
) {
  const iterator = ([head, ...tail]: number[], trees: T[]) => {
    if (tail.length > 0) {
      const point = trees[head] || ({} as T);
      const tree = {
        ...point,
        [subtreeKey]: iterator(tail, (point[subtreeKey] || []) as T[]),
      };
      trees[head] = tree;
      return trees;
    } else {
      trees[head] = target;
      return trees;
    }
  };
  return iterator(path, source);
}

export interface Tree {
  [key: string]: Tree[];
}

export type TreeReducer<T extends Tree, R> = (
  acc: R,
  currentNode: T,
  parents: T[],
  path: number[],
) => R;

export type TreeMapper<T extends Tree, R extends Tree> = (
  tree: T,
  parents: T[],
  path: number[],
) => R;

export type TreeFilter<T extends Tree> = (
  tree: T,
  parents: T[],
  path: number[],
) => boolean;
