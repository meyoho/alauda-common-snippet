export const API_GATEWAY = '{{API_GATEWAY}}';

export const TRUE = 'true';
export const FALSE = 'false';

export const EMPTY = '';
export const SPACE = ' ';

export const Bracket = {
  PARENTHESES: ['(', ')'],
  BRACES: ['{', '}'],
  SQUARE_BRACKETS: ['[', ']'],
  POINTY_BRACKETS: ['<', '>'],
} as const;

export const TEMPLATE_OPTIONS: import('lodash').TemplateOptions = Object.freeze(
  {
    interpolate: /{{([\S\s]+?)}}/g,
  },
);

export const DEFAULT_CODE_EDITOR_OPTIONS: import('@alauda/code-editor').MonacoEditorOptions = {
  fontSize: 12,
  folding: true,
  scrollBeyondLastLine: false,
  minimap: { enabled: false },
  find: {
    seedSearchStringFromSelection: false,
    autoFindInSelection: 'never',
  },
  mouseWheelZoom: true,
  scrollbar: {
    vertical: 'visible',
    horizontal: 'visible',
  },
  fixedOverflowWidgets: true,
};

export const NOTIFY_ON_ERROR_HEADER = '__NOTIFICATION_ON_ERROR__';

export const NOTIFY_ON_ERROR_HEADERS = {
  [NOTIFY_ON_ERROR_HEADER]: TRUE,
} as const;

export const NOT_NOTIFY_ON_ERROR_HEADERS = {
  [NOTIFY_ON_ERROR_HEADER]: FALSE,
} as const;
