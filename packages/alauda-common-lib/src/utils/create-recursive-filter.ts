/**
 * @description Recursive filter without presume data structure
 *
 * @example 1. Nested Tree
 *
 * ```
 * interface Node {
 *   ...,
 *   children: Node[]
 * }
 *
 * const filter = createRecursiveFilter(
 *   (node: Node) => node.children,
 *   (node: Node, children: Node[]) => ({ ...node, children }),
 *  );
 * ```
 *
 * @example 2. Normalized Tree
 *
 * ```
 * interface Node {
 *   id: string;
 *   children: string[];
 * }
 *
 * interface NodeEntities {
 *   [id: string]: Node;
 * }
 *
 * const nodes: NodeEntities;
 *
 * const filter = createRecursiveFilter(
 *   (node: Node) => node.children.map(id => nodes[id]),
 *   (node: Node, children: Node[]) => ({ ...node, children: children.map(child => child.id) }),
 * );
 * ```
 */
export const createRecursiveFilter = <T>(
  pickChildren: (item: T) => T[] = null,
  mergeChildren: (item: T, children: T[]) => T = null,
) => (items: T[], condition: (item: T) => boolean): T[] => {
  if (!pickChildren || !mergeChildren) {
    return items.filter(condition);
  }

  const filter = createRecursiveFilter(pickChildren, mergeChildren);

  return items.filter(condition).map(item => {
    const children = pickChildren(item);

    if (!children || children.length === 0) {
      return item;
    }

    return mergeChildren(item, filter(children, condition));
  });
};
