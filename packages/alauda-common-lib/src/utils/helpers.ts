import { last } from 'lodash-es';

import { Callback, Keys, Nullable, Readonlyable } from '../types/public-api';

import { Bracket } from './constants';

export const getBaseHref = () => {
  // The last base element has highest privilege
  const base = last(document.querySelectorAll('base'));
  // use `getAttribute` instead of `base.href` because the attribute could be void but results current whole location url
  return base?.getAttribute('href') || '/';
};

export const ifExist = <T>(
  toCheck: boolean | Nullable<T>,
  truthyValue: T,
  fallback?: T,
) => (toCheck ? truthyValue : ((fallback || '') as T));

export const isAbsoluteUrl = (url: string) => /^(https?:\/)?\//.test(url);

// eslint-disable-next-line @typescript-eslint/no-empty-function
export const noop: Callback = () => {};

export const toKeys = <T>(source: T): Keys<T> =>
  Object.keys(source).reduce((acc, k) => {
    const key = k as keyof T;
    acc[key] = key;
    return acc;
  }, {} as Keys<T>);

export const wrapText = (
  text?: string,
  wrapper: Readonlyable<[string, string]> = Bracket.PARENTHESES,
  fallback?: string,
) => (text ? wrapper.join(text) : fallback || '');

export function parseJSONStream<T>(text: string): T[] {
  return String.raw({ raw: text } as any)
    .split('\n')
    .map(v => {
      try {
        return JSON.parse(v);
      } catch {
        return null;
      }
    })
    .filter(v => !!v);
}
