import { Injectable, Injector } from '@angular/core';
import dayjs, { ConfigType } from 'dayjs';
import 'dayjs/plugin/relativeTime';

import { Locale, TranslateService } from '../translate/public-api';

export const DATE_FORMAT = 'YYYY-MM-DD';
export const DATE_TIME_FORMAT = DATE_FORMAT + ' HH:mm:ss';

@Injectable({
  providedIn: 'root',
})
export class TimeService {
  constructor(private readonly injector: Injector) {}

  format(date?: ConfigType, dateFormat = DATE_TIME_FORMAT) {
    return dayjs(date).format(dateFormat);
  }

  distance(date: ConfigType, baseDate?: ConfigType) {
    return dayjs(date)
      .locale(
        this.injector.get(TranslateService).locale === Locale.ZH
          ? 'zh-cn'
          : 'en',
      )
      .from(baseDate);
  }
}
