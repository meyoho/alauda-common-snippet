import { EMPTY, MonoTypeOperatorFunction, Observable, of, pipe } from 'rxjs';
import { catchError, publishReplay, refCount, switchMap } from 'rxjs/operators';

export const publishRef = <T>(
  bufferSize = 1,
  windowTime?: number,
): MonoTypeOperatorFunction<T> =>
  pipe(publishReplay(bufferSize, windowTime), refCount());

export const skipError = <T>(mapped: T | Observable<T> = EMPTY) =>
  catchError(() => (mapped instanceof Observable ? mapped : of(mapped)));

export const catchPromise = <T, R>(
  promise: Promise<T>,
  mapped?: R | Observable<R>,
) =>
  of(null).pipe(
    switchMap(() => promise),
    skipError(mapped),
  );
