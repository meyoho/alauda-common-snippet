import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FeatureGateDirective } from './feature-gate.directive';
import { FieldNotAvailablePipe } from './field-not-available.pipe';
import {
  K8sAnnotationPipe,
  K8sCreationTimestampPipe,
  K8sCreatorPipe,
  K8sDescriptionPipe,
  K8sDisplayNamePipe,
  K8sLabelPipe,
  K8sNamePipe,
  K8sNamespacePipe,
  K8sProjectPipe,
  K8sUnionDisplayNamePipe,
  K8sUpdatedAtPipe,
} from './k8s-util.pipe';
import { PurePipe } from './pure.pipe';
import {
  RelativeTimePipe,
  StandardDatePipe,
  StandardTimePipe,
} from './time.pipe';

const PIPES = [
  FieldNotAvailablePipe,
  PurePipe,
  // time pipes
  RelativeTimePipe,
  StandardTimePipe,
  StandardDatePipe,
  // k8s util pipes
  K8sAnnotationPipe,
  K8sCreationTimestampPipe,
  K8sCreatorPipe,
  K8sDescriptionPipe,
  K8sDisplayNamePipe,
  K8sLabelPipe,
  K8sNamePipe,
  K8sNamespacePipe,
  K8sProjectPipe,
  K8sUnionDisplayNamePipe,
  K8sUpdatedAtPipe,
];

const DIRECTIVES = [FeatureGateDirective];

const EXPORTABLES = [...PIPES, ...DIRECTIVES];

@NgModule({
  imports: [CommonModule],
  declarations: EXPORTABLES,
  exports: EXPORTABLES,
})
export class UtilsModule {}
