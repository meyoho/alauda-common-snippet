import { Observable, of } from 'rxjs';
import { catchError, map, startWith } from 'rxjs/operators';

export interface WithLoadState<T, E = unknown> {
  loading: boolean;
  data: T;
  error: E;
}

export function withLoadState<T>(
  input: Observable<T>,
  defaultValue: T = null,
): Observable<WithLoadState<T>> {
  return input.pipe(
    map(data => ({
      loading: false,
      data,
      error: null,
    })),
    startWith({
      loading: true,
      data: defaultValue,
      error: null,
    }),
    catchError(error =>
      of({
        loading: false,
        data: defaultValue,
        error,
      }),
    ),
  );
}
