import { Pipe, PipeTransform } from '@angular/core';

export const FIELD_NOT_AVAILABLE_PLACEHOLDER = '-';

/**
 * Pipe a title sentence to correct cases.
 */
@Pipe({ name: 'aclFieldNotAvailable' })
export class FieldNotAvailablePipe implements PipeTransform {
  transform(field: string | number): string | number {
    if (field == null || !field.toString().trim()) {
      return FIELD_NOT_AVAILABLE_PLACEHOLDER;
    }
    return field;
  }
}
