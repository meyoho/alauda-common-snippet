import { StringMap } from '../types/commons';
import { LabelSelectorRequirement } from '../types/k8s';

/**
 * convert selector.matchLabels to string
 * refers to: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#api
 */
export function matchLabelsToString(params: StringMap = {}) {
  return Object.keys(params)
    .filter(key => params[key])
    .map(key => `${key}=${params[key]}`)
    .join(',');
}

/**
 * convert string to selector.matchLabels
 */
export function stringToMatchLabels(str?: string): Record<string, string> {
  const labels: Record<string, string> = {};
  if (str) {
    return str.split(',').reduce((acc, item) => {
      const [key, value] = item.split('=');
      if (key && value) {
        acc[key] = value;
      }
      return acc;
    }, labels);
  }
  return labels;
}

export function matchExpressionsToString(params: LabelSelectorRequirement[]) {
  return params
    .map(param => {
      switch (param.operator.toLowerCase()) {
        case '=':
        case '==':
        case '!=':
          return `${param.key}${param.operator}${param.values[0]}`;
        case '!':
          return `!${param.key}`;
        case 'in':
        case 'notin':
          return `${param.key} ${param.operator} (${param.values
            .filter(Boolean)
            .join(',')})`;
        case 'exists':
        case 'doesnotexist':
          return `${param.key} ${param.operator}`;
        default:
          return param.key;
      }
    })
    .join(',');
}

export function normalizeParams<P>(params: P): P {
  return Object.entries(params).reduce((acc, [key, value]) => {
    value = typeof value === 'string' ? value.trim() : value;
    if (value) {
      acc[key as keyof P] = value;
    }
    return acc;
  }, {} as P);
}
