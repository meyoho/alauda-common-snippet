import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { WatchAPIResponse } from '../api/types';
import { ResourceListParams } from '../types/commons';
import { KubernetesResource, KubernetesResourceList } from '../types/k8s';

import { K8SResourceList } from './k8s-resource-list';

export type Watcher<R extends KubernetesResource> = (
  seedVersion: string,
) => Observable<WatchAPIResponse<R>>;
export type InsertFn<R> = (items: R[], res: R, hasMore: boolean) => R[];

export interface WatcherParams<R extends KubernetesResource> {
  list: K8SResourceList<R>;
  watcher: Watcher<R>;
  insertFn?: InsertFn<R>;
}

interface BaseConfig<R extends KubernetesResource> {
  watcher?: Watcher<R>;
  insertFn?: InsertFn<R>;
  limit?: number;
  polling?: number;
}

export type ItemsScanner<R> = (items: R[]) => R[];

export interface ConfigWithRoute<R> extends BaseConfig<R> {
  fetcher: (
    params: ResourceListParams,
  ) => Observable<KubernetesResourceList<R>>;
  activatedRoute: ActivatedRoute;
}

export interface ConfigWithParams<R, P> extends BaseConfig<R> {
  fetcher: (params: P) => Observable<KubernetesResourceList<R>>;
  fetchParams$: Observable<P>;
}

export enum LoadAction {
  Reload = 'Reload',
  LoadMore = 'LoadMore',
  Poll = 'Poll',
}
