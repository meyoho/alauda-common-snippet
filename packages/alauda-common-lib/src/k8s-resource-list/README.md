# K8SResourceList

## K8SResourceList

### 配置

| 名称           | 类型            | 必填 | 说明                                                               |
| -------------- | --------------- | ---- | ------------------------------------------------------------------ |
| fetcher        | function        | ✔︎   | 接受参数并返回 API 数据的函数，刷新数据或继续加载时调用            |
| activatedRoute | ActivatedRoute  | ✘    | 如果 fetcher 不需要特殊参数且参数都在 URL query 字段中可以传入此项 |
| fetchParams\$  | Observable<any> | ✘    | fetcher 所需参数                                                   |

### 接口

| 名称           | 类型                                      | 说明                                                     |
| -------------- | ----------------------------------------- | -------------------------------------------------------- |
| reload         | function                                  | 重新加载列表                                             |
| loadMore       | function                                  | 继续加载                                                 |
| scanItems      | function                                  | 接受一个函数作为参数，并用此函数处理整个列表             |
| create         | function                                  | 接受一个资源数据作为参数，并添加到列表顶部               |
| update         | function                                  | 接受一个资源数据作为参数，并替换相同列表中相同名称的资源 |
| delete         | function                                  | 接受一个资源数据作为参数，并删除列表中相同名称的资源     |
| rawResponse\$  | Observable<KubernetesResourceList>        | 当次请求返回的数据                                       |
| loading\$      | Observable<boolean>                       | 加载状态                                                 |
| loadingError\$ | Observable<HttpErrorResponse \| Status>   | 错误状态                                                 |
| hasMore\$      | Observable<boolean>                       | 是否还有更多数据                                         |
| items\$        | items\$: Observable<KubernetesResource[]> | 当前加载的数据数组                                       |

## K8SResourceListFooter

### Input

| 名称           | 类型            | 默认值 | 说明                                 |
| -------------- | --------------- | ------ | ------------------------------------ |
| list           | K8SResourceList | -      |                                      |
| autoLoad       | boolean         | true   | 滚动时是否自动加载                   |
| backTop        | boolean         | true   | 滚动后是否显示「返回顶部」按钮       |
| bottomDistance | number          | 60     | 距页面底部多少像素时开始加载剩余数据 |

## 工具函数

| 名称                     | 说明                                                                                                              |
| ------------------------ | ----------------------------------------------------------------------------------------------------------------- |
| matchLabelsToString      | 将 matchLabels 对象转换为字符串                                                                                   |
| stringToMatchLabels      | 将字符串转换为 matchLabels 对象                                                                                   |
| matchExpressionsToString | 将 [matchExpressions](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#api) 转换为字符串 |
