import { BackTopModule, ButtonModule, IconModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '../translate/public-api';

import { K8SResourceListFooterComponent } from './footer/component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    IconModule,
    BackTopModule,
    TranslateModule,
  ],
  declarations: [K8SResourceListFooterComponent],
  exports: [K8SResourceListFooterComponent],
})
export class K8SResourceListModule {}
