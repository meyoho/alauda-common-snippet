import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import {
  EMPTY,
  Observable,
  Subject,
  combineLatest,
  interval,
  merge,
  of,
} from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  exhaustMap,
  filter,
  map,
  mapTo,
  scan,
  startWith,
  switchMap,
  takeUntil,
  withLatestFrom,
} from 'rxjs/operators';

import {
  KubernetesResource,
  KubernetesResourceList,
  ResourceListParams,
  Status,
} from '../types/public-api';
import { publishRef } from '../utils/public-api';

import { K8SResourceWatcher } from './k8s-resource-watcher';
import {
  ConfigWithParams,
  ConfigWithRoute,
  ItemsScanner,
  LoadAction,
} from './types';
import { normalizeParams } from './utils';

export class K8SResourceList<
  R extends KubernetesResource = KubernetesResource,
  P extends ResourceListParams = ResourceListParams
> {
  private readonly reloadAction$$ = new Subject<void>();
  private readonly loadMoreAction$$ = new Subject<void>();
  private readonly itemsScanner$$ = new Subject<ItemsScanner<R>>();
  private readonly destroy$$ = new Subject<void>();

  private readonly watcher: K8SResourceWatcher<R>;

  loadState$: Observable<{
    action?: LoadAction;
    rawResponse?: KubernetesResourceList<R>;
    loading?: boolean;
    loadingError?: HttpErrorResponse | Status;
  }>;

  queryParams$: Observable<P>;
  rawResponse$: Observable<KubernetesResourceList<R>>;
  loading$: Observable<boolean>;
  loadingError$: Observable<HttpErrorResponse | Status>;
  hasMore$: Observable<boolean>;
  continueToken$: Observable<string>;

  items$: Observable<R[]>;

  constructor({ fetcher, activatedRoute }: ConfigWithRoute<R>);
  // eslint-disable-next-line @typescript-eslint/unified-signatures
  constructor({ fetcher, fetchParams$ }: ConfigWithParams<R, P>);
  constructor({
    fetcher,
    fetchParams$,
    activatedRoute,
    limit = 20,
    polling = 0,
    watcher,
    insertFn,
  }: ConfigWithRoute<R> & ConfigWithParams<R, P>) {
    this.queryParams$ =
      fetchParams$ ||
      (this.extractParamsFromRoute(activatedRoute) as Observable<P>);

    this.loadState$ = this.buildLoadStateStream({
      queryParams$: this.queryParams$,
      fetcher,
      limit,
      polling,
    });

    this.initStreams();

    if (watcher) {
      this.watcher = new K8SResourceWatcher({
        list: this,
        watcher,
        insertFn,
      });
    }
  }

  reload() {
    this.reloadAction$$.next();
  }

  loadMore() {
    this.loadMoreAction$$.next();
  }

  scanItems(scanner: ItemsScanner<R>) {
    this.itemsScanner$$.next(scanner);
  }

  destroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
    if (this.watcher) {
      this.watcher.destroy();
    }
  }

  create(resource: R) {
    this.scanItems(items => [resource].concat(items));
  }

  update(resource: R) {
    this.scanItems(items =>
      items.map(item =>
        item.metadata.name === resource.metadata.name ? resource : item,
      ),
    );
  }

  delete({ metadata: { name } }: R) {
    this.scanItems(items => items.filter(item => item.metadata.name !== name));
  }

  private buildLoadStateStream({
    queryParams$,
    fetcher,
    polling,
    limit,
  }: {
    queryParams$: Observable<P>;
    fetcher: (p: unknown) => Observable<KubernetesResourceList<R>>;
    polling: number;
    limit: number;
  }) {
    const load$ = combineLatest([
      queryParams$,
      this.reloadAction$$.pipe(startWith(null as void)),
    ]).pipe(
      map(([queryParams]) => ({
        action: LoadAction.Reload,
        queryParams,
      })),
      switchMap(params =>
        polling
          ? interval(polling).pipe(
              mapTo({ ...params, action: LoadAction.Poll }),
              startWith(params),
            )
          : of(params),
      ),
    );

    return load$.pipe(
      switchMap(({ action: startAction, queryParams }) =>
        this.loadMoreAction$$.pipe(
          withLatestFrom(this.continueToken$),
          map(([_, token]) => token),
          filter(token => !!token),
          startWith(''),
          withLatestFrom(this.items$.pipe(map(items => items.length))),
          exhaustMap(([token, currLength]) => {
            const action = token ? LoadAction.LoadMore : startAction;
            return fetcher(
              normalizeParams<P>({
                limit:
                  (action === LoadAction.Poll && !token ? currLength : limit) +
                  '',
                continue: token,
                ...queryParams,
              }),
            ).pipe(
              map(rawResponse => ({ rawResponse })),
              catchError((err: Status | HttpErrorResponse) =>
                of({
                  loadingError: action === LoadAction.Poll ? null : err,
                }),
              ),
              map(state => ({
                ...state,
                action,
              })),
              startWith({
                action,
                loading: true,
              }),
            );
          }),
          startWith({ action: startAction, loading: true }),
        ),
      ),
      map(state => ({
        action: LoadAction.LoadMore,
        rawResponse: null,
        loading: false,
        loadingError: null,
        ...state,
      })),
      takeUntil(this.destroy$$),
      publishRef(),
    );
  }

  private extractParamsFromRoute(
    route: ActivatedRoute,
  ): Observable<ResourceListParams> {
    return route.queryParamMap.pipe(
      map(paramMap => ({
        labelSelector: paramMap.get('labelSelector'),
        fieldSelector: paramMap.get('fieldSelector'),
        keyword: paramMap.get('keyword'),
        field: paramMap.get('field'),
      })),
    );
  }

  private initStreams() {
    this.rawResponse$ = this.loadState$.pipe(
      map(data => data.rawResponse),
      distinctUntilChanged(),
      publishRef(),
    );

    this.loading$ = this.loadState$.pipe(
      map(({ loading, action }) => loading && action !== LoadAction.Poll),
      distinctUntilChanged(),
      publishRef(),
    );

    this.loadingError$ = this.loadState$.pipe(
      map(state => state.loadingError),
      distinctUntilChanged(),
      publishRef(),
    );

    this.continueToken$ = this.loadState$.pipe(
      filter(({ loading }) => !loading),
      switchMap(({ rawResponse, loadingError: err }) => {
        // eslint-disable-next-line @typescript-eslint/no-magic-numbers
        if (rawResponse) {
          return of(rawResponse.metadata.continue ?? '');
        }
        if (err && 'code' in err && err.code === 410) {
          return of(err.metadata.continue);
        }
        return EMPTY;
      }),
      distinctUntilChanged(),
      publishRef(),
    );

    this.hasMore$ = this.continueToken$.pipe(
      map(token => !!token),
      distinctUntilChanged(),
      publishRef(),
    );

    this.items$ = merge(
      this.loadState$.pipe(
        map(state => (items: R[]) => {
          const newItems = state.rawResponse
            ? state.rawResponse.items ?? []
            : null;
          if (state.action === LoadAction.Reload) {
            return newItems || [];
          } else if (state.action === LoadAction.Poll) {
            return newItems || items;
          } else if (state.action === LoadAction.LoadMore) {
            return newItems ? items.concat(newItems) : items;
          } else {
            return items;
          }
        }),
      ),
      this.itemsScanner$$,
    ).pipe(
      scan<ItemsScanner<R>, R[]>((acc, scanner) => scanner(acc), []),
      publishRef(),
    );
  }
}
