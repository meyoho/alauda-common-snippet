import { Subject } from 'rxjs';
import {
  filter,
  map,
  switchMap,
  take,
  takeUntil,
  withLatestFrom,
} from 'rxjs/operators';

import { WatchAPIResponse, WatchEvent } from '../api/types';
import { KubernetesResource } from '../types/k8s';

import { K8SResourceList } from './k8s-resource-list';
import { InsertFn, WatcherParams } from './types';

export class K8SResourceWatcher<T extends KubernetesResource> {
  private readonly destroy$$ = new Subject<void>();

  private readonly list: K8SResourceList<T>;
  private readonly insertFn: InsertFn<T>;

  constructor({ list, watcher, insertFn = insertByName }: WatcherParams<T>) {
    this.list = list;
    this.insertFn = insertFn;

    list.rawResponse$
      .pipe(
        filter(res => !!res),
        take(1),
        switchMap(list => watcher(list.metadata.resourceVersion)),
        withLatestFrom(
          list.queryParams$.pipe(map(params => params.keyword ?? '')),
          list.hasMore$,
        ),
        takeUntil(this.destroy$$),
      )
      .subscribe(([res, keyword, hasMore]) => {
        this.watchResource(res, keyword, hasMore);
      });
  }

  destroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  private watchResource(
    { type, object: resource }: WatchAPIResponse<T>,
    keyword: string,
    hasMore: boolean,
  ) {
    switch (type) {
      case WatchEvent.Added:
        if (
          !resource.metadata.name.toLowerCase().includes(keyword.toLowerCase())
        ) {
          return;
        }
        this.list.scanItems(items => this.insertFn(items, resource, hasMore));
        break;
      case WatchEvent.Modified:
        this.list.update(resource);
        break;
      case WatchEvent.Deleted:
        this.list.delete(resource);
        break;
    }
  }
}

function insertByName<T extends KubernetesResource>(
  items: T[],
  res: T,
  hasMore: boolean,
) {
  let index = 0;

  for (const [i, element] of items.entries()) {
    const v = element.metadata.name.localeCompare(res.metadata.name);
    if (v < 0) {
      index = i + 1;
    } else if (v > 0) {
      break;
    } else {
      index = -1;
      break;
    }
  }

  return index < 0 || (index >= items.length && hasMore)
    ? items
    : items.slice(0, index).concat(res, items.slice(index));
}
