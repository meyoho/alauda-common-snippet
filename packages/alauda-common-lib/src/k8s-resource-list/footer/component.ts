import { CdkScrollable } from '@angular/cdk/overlay';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';
import {
  Observable,
  Subject,
  combineLatest,
  concat,
  fromEvent,
  merge,
} from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  pairwise,
  startWith,
  switchMapTo,
  take,
  takeUntil,
  takeWhile,
  withLatestFrom,
} from 'rxjs/operators';

import { TranslateService } from '../../translate/public-api';
import { publishRef } from '../../utils/public-api';
import { K8SResourceList } from '../k8s-resource-list';
import { LoadAction } from '../types';

@Component({
  selector: 'acl-k8s-resource-list-footer',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class K8SResourceListFooterComponent implements OnInit, OnDestroy {
  @Input() list: K8SResourceList;
  @Input() autoLoad = true;
  @Input() backTop = true;
  @Input() bottomDistance = 60;
  @Input() resourceName: string;

  isEmptyList$: Observable<boolean>;
  noDataHint$: Observable<string>;
  loadingErrorCode$: Observable<number>;

  private readonly scrollEl: HTMLElement | Window;
  private readonly destroy$$ = new Subject<void>();

  constructor(
    private readonly translate: TranslateService,
    @Optional() scrollable: CdkScrollable,
  ) {
    this.scrollEl = scrollable
      ? scrollable.getElementRef().nativeElement
      : window;
  }

  ngOnInit() {
    this.loadingErrorCode$ = this.list.loadingError$.pipe(
      map(
        loadingError =>
          loadingError && 'code' in loadingError && loadingError.code,
      ),
      distinctUntilChanged(),
      publishRef(),
    );

    this.isEmptyList$ = combineLatest([
      this.list.items$.pipe(map(items => items.length)),
      this.list.hasMore$,
      this.list.loading$,
      this.list.loadingError$,
    ]).pipe(
      map(
        ([length, hasMore, loading, loadingError]) =>
          !(length || hasMore || loading || loadingError),
      ),
      distinctUntilChanged(),
      publishRef(),
    );

    this.noDataHint$ = combineLatest([
      this.list.rawResponse$,
      this.translate.locale$,
    ]).pipe(
      filter(([res]) => !!res),
      map(([res, lang]) => {
        const kind = res.kind;
        if (kind.endsWith('List') && kind.length > 4) {
          const name = this.formatFooterHint(
            lang,
            this.translate.get(
              this.resourceName ||
                kind.slice(0, kind.lastIndexOf('List')).toLowerCase(),
            ),
          );
          return this.translate.get('no_named_data_hint', {
            name,
          });
        } else {
          return this.translate.get('no_data');
        }
      }),
    );

    if (this.autoLoad) {
      this.setupAutoLoad();
    }
  }

  ngOnDestroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  retry() {
    this.list.items$.pipe(take(1)).subscribe(items => {
      if (items.length > 0) {
        this.list.loadMore();
      } else {
        this.list.reload();
      }
    });
  }

  private setupAutoLoad() {
    const viewEl =
      this.scrollEl === window
        ? document.documentElement
        : (this.scrollEl as HTMLElement);

    // 当数据不够一屏时自动加载更多
    const autoLoadOneScreen$ = this.list.loadState$.pipe(
      map(state => state.rawResponse),
      filter(res => !!res),
      debounceTime(50),
      takeWhile(res => {
        return (
          !!res.metadata.continue &&
          viewEl.scrollHeight - viewEl.clientHeight < this.bottomDistance
        );
      }),
    );

    const autoLoadMore$ = merge(
      fromEvent(this.scrollEl, 'scroll').pipe(
        startWith(viewEl.scrollTop),
        debounceTime(50),
        map(() => {
          return viewEl.scrollTop;
        }),
        pairwise(),
        // 向下滚动
        filter(([prev, curr]) => curr > prev),
      ),
      fromEvent(window, 'resize').pipe(
        startWith(document.body.clientHeight),
        debounceTime(50),
        map(() => {
          return document.body.clientHeight;
        }),
        pairwise(),
        // 窗口扩大
        filter(([prev, curr]) => curr > prev),
      ),
    ).pipe(
      filter(() => {
        return (
          viewEl.scrollTop >
          viewEl.scrollHeight - viewEl.clientHeight - this.bottomDistance
        );
      }),
    );

    this.list.loadState$
      .pipe(
        filter(state => state.action === LoadAction.Reload),
        startWith({}),
        switchMapTo(concat(autoLoadOneScreen$, autoLoadMore$)),
        // list.rawResponse$ 会处理好 loading 等状态时的 loadMore 请求，但有错误时主要主动停止
        withLatestFrom(this.list.loadingError$),
        filter(([_, error]) => !error),
        takeUntil(this.destroy$$),
      )
      .subscribe(() => {
        this.list.loadMore();
      });
  }

  // should insert space between '无' and english name
  private formatFooterHint(lang: string, name: string) {
    if (lang === 'zh' && /^\w/.test(name)) {
      return ` ${name}`;
    }
    return name;
  }
}
