import { DIALOG_DATA } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import {
  IEditorConstructionOptions,
  readonlyOptions,
  viewActions,
} from '../code-editor-configs';

export interface CodeDisplayDialogData {
  language?: string;
  title?: string;
  code: string;
}

@Component({
  templateUrl: 'template.html',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeDisplayDialogComponent {
  codeEditorOptions: IEditorConstructionOptions;

  editorActions = viewActions;

  constructor(
    @Inject(DIALOG_DATA)
    public data: CodeDisplayDialogData,
  ) {
    this.codeEditorOptions = {
      ...readonlyOptions,
      language: data.language || 'yaml',
    };
  }
}
