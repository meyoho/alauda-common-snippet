import { CodeEditorModule } from '@alauda/code-editor';
import { DialogModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { TranslateModule } from '../translate/translate.module';

import { CodeDisplayDialogComponent } from './code-display-dialog/component';

const EXPORTABLE_MODULES = [
  CommonModule,
  CodeEditorModule,
  DialogModule,
  FormsModule,
  TranslateModule,
];

const ENTRY_COMPONENTS = [CodeDisplayDialogComponent];

const EXPORTABLE_COMPONENTS = ENTRY_COMPONENTS;

@NgModule({
  imports: EXPORTABLE_MODULES,
  declarations: EXPORTABLE_COMPONENTS,
  exports: [...EXPORTABLE_MODULES, ...EXPORTABLE_COMPONENTS],
  entryComponents: ENTRY_COMPONENTS,
})
export class CodeModule {}
