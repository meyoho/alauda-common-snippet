import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { get } from 'lodash-es';
import { Observable, combineLatest } from 'rxjs';
import { map, pluck } from 'rxjs/operators';

import { TranslateKey } from '../translate/types';
import {
  KubernetesResource,
  KubernetesResourceList,
  ObjectMeta,
} from '../types/k8s';
import { API_GATEWAY } from '../utils/constants';
import { K8sUtilService } from '../utils/k8s-util.service';
import { TOKEN_GLOBAL_NAMESPACE } from '../utils/tokens';

interface Project extends KubernetesResource {
  spec: {
    clusters: Array<{
      name: string;
      quota: string;
    }>;
    status?: {
      phase: string;
    };
  };
}

interface Cluster extends KubernetesResource {
  metadata?: ObjectMeta & {
    finalizers?: string[];
  };
  details?: {
    group: string;
    kind: string;
    name: string;
    uid: string;
  };
}

interface Namespace extends KubernetesResource {
  status?: {
    phase?: string;
  };
}

export interface Production extends KubernetesResource {
  kind: 'Production';
  spec: {
    group: 'production' | 'others';
    index: number;
    logoURL?: string;
    title: TranslateKey;
    version?: string;
    description?: string;
    openInBlank?: boolean;
    homepage: string;
  };
}

export interface NamespaceIdentity {
  cluster: string;
  name: string;
}

export interface Product {
  name: string;
  displayName: string;
  url: string;
  hide?: boolean;
  index: number;
}

@Injectable({ providedIn: 'root' })
export class CommonLayoutContextService {
  constructor(
    private readonly httpClient: HttpClient,
    private readonly k8sUtil: K8sUtilService,
    @Inject(TOKEN_GLOBAL_NAMESPACE) private readonly globalNamespace: string,
  ) {}

  getProjects(): Observable<KubernetesResource[]> {
    return this.httpClient
      .get<KubernetesResourceList<Project>>(`${API_GATEWAY}/auth/v1/projects`)
      .pipe(pluck('items'));
  }

  getProducts(): Observable<Product[]> {
    return this.httpClient
      .get<KubernetesResourceList<Production>>(
        `${API_GATEWAY}/apis/portal.alauda.io/v1alpha1/alaudaproducts`,
      )
      .pipe(
        map(res => res.items || []),
        map(items =>
          items.map(item => ({
            name: this.k8sUtil.getName(item),
            displayName: this.k8sUtil.getDisplayName(item),
            url: get(item, 'spec.homepage'),
            hide: get(item, 'spec.hide'),
            index: get(item, 'spec.index'),
          })),
        ),
      );
  }

  getProjectClusters(_projectName: string): Observable<KubernetesResource[]> {
    return combineLatest([
      this.httpClient.get<Project>(
        `${API_GATEWAY}/apis/auth.alauda.io/v1/projects/${_projectName}`,
      ),
      this.httpClient
        .get(
          `${API_GATEWAY}/apis/clusterregistry.k8s.io/v1alpha1/namespaces/${this.globalNamespace}/clusters`,
        )
        .pipe(pluck('items')),
    ]).pipe(
      map(([project, clusters]) => {
        const clusterRefs = project.spec.clusters;
        return (clusters as Cluster[]).filter(c =>
          clusterRefs.find(ref => ref.name === c.metadata.name),
        );
      }),
    );
  }

  getClusterNamespaces(
    _projectName: string,
    _clusterName: string,
  ): Observable<KubernetesResource[]> {
    return this.httpClient
      .get<KubernetesResourceList<Namespace>>(
        `${API_GATEWAY}/auth/v1/projects/${_projectName}/clusters/${_clusterName}/namespaces`,
      )
      .pipe(pluck('items'));
  }
}
