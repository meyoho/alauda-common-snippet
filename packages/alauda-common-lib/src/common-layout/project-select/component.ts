import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { head } from 'lodash-es';
import { Subject, combineLatest } from 'rxjs';
import { map, startWith, tap } from 'rxjs/operators';

import { KubernetesResource } from '../../types/k8s';
import { K8sUtilService } from '../../utils/k8s-util.service';
import { CommonLayoutStoreService } from '../store.service';

@Component({
  selector: 'acl-project-select',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ProjectSelectComponent implements OnInit {
  @Input() selected = '';

  @Output() selectedChange = new EventEmitter<string>();

  hideOnClick = false;

  keywords$$ = new Subject<string>();

  projects$ = combineLatest([
    this.keywords$$.pipe(startWith('')),
    this.store.selectAllProjects().pipe(
      tap((projects: KubernetesResource[]) => {
        if (this.selected) {
          return;
        }

        if (!projects || projects.length === 0) {
          return;
        }

        this.selectedChange.emit(this.name(head(projects)));
      }),
    ),
  ]).pipe(
    map(([keywords, projects]) => {
      return projects.filter(project => this.name(project).includes(keywords));
    }),
  );

  constructor(
    private readonly store: CommonLayoutStoreService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    this.store.refetch();
  }

  onSelect(item: KubernetesResource) {
    this.hideOnClick = true;

    if (this.selected === this.name(item)) {
      return;
    }

    this.selectedChange.emit(this.name(item));
  }

  name = (item: KubernetesResource) => this.k8sUtil.getName(item);

  displayName = (item: KubernetesResource) => this.k8sUtil.getDisplayName(item);

  actived = (item: KubernetesResource) => {
    if (!this.selected) {
      return false;
    }
    return this.name(item) === this.selected;
  };

  trackFn = (_: number, item: KubernetesResource) => this.name(item);
}
