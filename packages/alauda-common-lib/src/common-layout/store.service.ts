import { Injectable, OnDestroy } from '@angular/core';
import {
  Observable,
  Subject,
  Subscription,
  forkJoin,
  identity,
  of,
} from 'rxjs';
import { catchError, map, retry, startWith, switchMap } from 'rxjs/operators';

import { KubernetesResource } from '../types/k8s';
import { K8sUtilService } from '../utils/k8s-util.service';
import { publishRef } from '../utils/public-api';

import { CommonLayoutContextService, Product } from './context.service';

const RETRY_COUNT = 3;

export interface Group {
  cluster: KubernetesResource;
  namespaces: KubernetesResource[];
}

@Injectable({ providedIn: 'root' })
export class CommonLayoutStoreService implements OnDestroy {
  private readonly fetch$ = new Subject<void>();

  private readonly projects$: Observable<
    KubernetesResource[]
  > = this.fetch$.pipe(
    switchMap(() =>
      this.context.getProjects().pipe(
        retry(RETRY_COUNT),
        catchError(() => of([])),
      ),
    ),
    startWith([]),
    publishRef(),
  );

  private readonly products$: Observable<
    Product[]
  > = this.context.getProducts().pipe(
    retry(RETRY_COUNT),
    catchError(() => of<Product[]>([])),
    publishRef(),
  );

  private readonly subscriptions: Subscription[] = [];

  constructor(
    private readonly context: CommonLayoutContextService,
    private readonly k8sUtil: K8sUtilService,
  ) {
    this.subscriptions.push(
      this.projects$.subscribe(),
      this.products$.subscribe(),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  refetch() {
    this.fetch$.next();
  }

  selectAllProducts(withHide = false): Observable<Product[]> {
    return this.products$.pipe(
      map(products =>
        (products || [])
          .filter(product => withHide || !product.hide)
          .sort((a, b) => a.index - b.index),
      ),
    );
  }

  selectProductByName(name: string): Observable<Product> {
    return this.products$.pipe(
      map(products => products.find(item => item.name === name)),
    );
  }

  selectAllProjects() {
    return this.projects$.pipe(map(identity));
  }

  selectProjectByName(name: string) {
    return this.projects$.pipe(
      map(projects =>
        projects.find(
          (item: KubernetesResource) => this.k8sUtil.getName(item) === name,
        ),
      ),
    );
  }

  selectNamespacesByProjectName(
    name: string,
  ): Observable<Array<[KubernetesResource, KubernetesResource[]]>> {
    return this.context.getProjectClusters(name).pipe(
      switchMap(clusters =>
        clusters.length > 0
          ? forkJoin(
              clusters.map(cluster =>
                this.context
                  .getClusterNamespaces(name, this.k8sUtil.getName(cluster))
                  .pipe(
                    catchError(() => of([])),
                    map(namespaces => [cluster, namespaces]),
                  ),
              ),
            )
          : of([]),
      ),
    );
  }
}
