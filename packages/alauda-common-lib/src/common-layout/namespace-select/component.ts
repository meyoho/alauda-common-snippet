import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { head, isEqual } from 'lodash-es';
import { BehaviorSubject, Subject, combineLatest } from 'rxjs';
import { map, startWith, switchMap, tap } from 'rxjs/operators';

import { KubernetesResource } from '../../types/k8s';
import { K8sUtilService } from '../../utils/k8s-util.service';
import { publishRef } from '../../utils/public-api';
import { NamespaceIdentity } from '../context.service';
import { CommonLayoutStoreService, Group } from '../store.service';

@Component({
  selector: 'acl-namespace-select',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class NamespaceSelectComponent {
  private readonly projectName$$ = new BehaviorSubject('');

  @Input()
  set projectName(projectName: string) {
    this.projectName$$.next(projectName);
  }

  @Input() selected?: NamespaceIdentity;

  @Output() selectedChange = new EventEmitter<NamespaceIdentity | null>();

  hideOnClick = false;

  keywords$$ = new Subject<string>();

  availableGroups$ = this.projectName$$.pipe(
    switchMap(projectName =>
      this.store.selectNamespacesByProjectName(projectName),
    ),
    publishRef(),
  );

  hasNamespace$ = this.availableGroups$.pipe(
    map(groups =>
      head(
        groups
          .map(([cluster, namespaces]) => {
            const namespace = head(namespaces);

            if (!namespace) {
              return null;
            }

            return {
              cluster: this.name(cluster),
              name: this.name(namespace),
            };
          })
          .filter(item => !!item),
      ),
    ),
    tap(first => {
      // TODO: temp fix for asm
      if (!this.selected && !first) {
        this.selectedChange.emit(null);
      }

      if (!this.selected && first) {
        this.selectedChange.emit(first);
      }
    }),
    map(first => !!first),
    publishRef(),
  );

  groups$ = combineLatest([
    this.keywords$$.pipe(startWith('')),
    this.availableGroups$,
  ]).pipe(
    map(([keywords, groups]) =>
      groups.map(([cluster, namespaces]) => ({
        cluster,
        namespaces: (namespaces || []).filter(namespace =>
          this.name(namespace).includes(keywords),
        ),
      })),
    ),
  );

  constructor(
    private readonly store: CommonLayoutStoreService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  name = (item: KubernetesResource) => this.k8sUtil.getName(item);

  displayName = (item: KubernetesResource) =>
    this.k8sUtil.getDisplayName(item) || this.name(item);

  actived = (
    { cluster }: { cluster: KubernetesResource },
    item: KubernetesResource,
  ) => {
    if (!this.selected) {
      return false;
    }
    return (
      this.name(cluster) === this.selected.cluster &&
      this.name(item) === this.selected.name
    );
  };

  trackFn = (_: number, item: KubernetesResource) => {
    return this.name(item);
  };

  groupTrackFn = (_: number, { cluster }: { cluster: KubernetesResource }) =>
    this.name(cluster);

  hasResult(groups: Group[]) {
    return groups.some(group => !!group.namespaces.length);
  }

  onSelect(
    { cluster }: { cluster: KubernetesResource },
    namespace: KubernetesResource,
  ) {
    this.hideOnClick = true;

    if (
      isEqual(this.selected, {
        cluster: this.name(cluster),
        namespace: this.name(namespace),
      })
    ) {
      return;
    }

    this.selectedChange.emit({
      cluster: this.name(cluster),
      name: this.name(namespace),
    });
  }
}
