import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  ViewEncapsulation,
} from '@angular/core';

import { ID_TOKEN_KEY } from '../../utils/tokens';
import { Product } from '../context.service';
import { CommonLayoutStoreService } from '../store.service';

@Component({
  selector: 'acl-product-select',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ProductSelectComponent {
  @Input() current: string;

  products$ = this.store.selectAllProducts();

  constructor(
    private readonly store: CommonLayoutStoreService,
    @Inject(ID_TOKEN_KEY) private readonly idTokenKey: string,
  ) {}

  navigate(item: Product) {
    if (location) {
      const [productUrl, hash] = item.url.split('#');
      const [url, search] = productUrl.split('?');
      const redirectSearch = search ? `&${search}` : '';
      const redirectHash = hash ? `#${hash}` : '';
      const redirectUrl = `${url}?id_token=${localStorage.getItem(
        this.idTokenKey,
      )}${redirectSearch}${redirectHash}`;

      location.href = redirectUrl;
    }
  }

  trackFn = (_: number, item: Product) => {
    return item.name;
  };
}
