# Common Layout

## 简介

在 common-layout 中主要实现一些全局通用逻辑，获取和切换 namespace、获取和切换产品、获取和切换项目

## acl-namespace-select

### Input

| 名称        | 类型              |
| ----------- | ----------------- |
| projectName | string            |
| selected    | NamespaceIdentity |

### Output

| 名称           | 类型                                    |
| -------------- | --------------------------------------- |
| selectedChange | EventEmitter<NamespaceIdentity \| null> |

## acl-product-select

### Input

| 名称    | 类型   |
| ------- | ------ |
| current | string |

## acl-project-select

### Input

| 名称     | 类型   |
| -------- | ------ |
| selected | string |

### Output

| 名称           | 类型                 |
| -------------- | -------------------- |
| selectedChange | EventEmitter<String> |
