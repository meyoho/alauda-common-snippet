import { DropdownModule, IconModule, InputModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '../translate/translate.module';
import { UtilsModule } from '../utils/public-api';

import { NamespaceSelectComponent } from './namespace-select/component';
import { ProductSelectComponent } from './product-select/component';
import { ProjectSelectComponent } from './project-select/component';

@NgModule({
  declarations: [
    ProjectSelectComponent,
    ProductSelectComponent,
    NamespaceSelectComponent,
  ],
  exports: [
    ProjectSelectComponent,
    ProductSelectComponent,
    NamespaceSelectComponent,
  ],
  imports: [
    CommonModule,
    DropdownModule,
    InputModule,
    IconModule,
    UtilsModule,
    TranslateModule,
  ],
})
export class CommonLayoutModule {}
