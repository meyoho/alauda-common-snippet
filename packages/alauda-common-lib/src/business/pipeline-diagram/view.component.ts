// eslint-disable @typescript-eslint/no-magic-numbers
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';

import { stepLineWithRadius } from './line';
import { PipelineDiagram, PipelineLink, PipelineNode, Stage } from './models';

@Component({
  selector: 'acl-pipeline-view',
  templateUrl: 'view.component.html',
  styleUrls: ['view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class PipelineViewComponent {
  @Input() gridX = 104;

  @Input() gridY = 64;

  @Input() textMarginY = 20;

  @Input() textMarginX = 8;

  @Input() nodeRadius = 12;

  @Input() endNodeRadius = 6;

  @Input() lineRadius = 10;

  @Input() skipLineRadius = 10;

  @Input() margin = 20;

  @Input() endOffset = 36;

  @Input() model: PipelineDiagram<Stage>;

  @Input() selected: Stage;

  @Input() hideStatus = false;

  @Output() selectedChange = new EventEmitter<Stage>();

  get width() {
    return this.model.size[0];
  }

  get height() {
    return this.model.size[1];
  }

  get surfaceTransform() {
    return `translate(${this.margin}px, ${this.margin}px)`;
  }

  get svgTransform() {
    return `translate(${this.gridX / 2}, ${this.gridY / 2})`;
  }

  get textTransform() {
    return `translate(0, -${this.nodeRadius + this.textMarginY}px)`;
  }

  get parallelTextTransform() {
    return `translate(0, ${this.nodeRadius + this.textMarginY}px)`;
  }

  tracker(_: number, item: PipelineNode<Stage> | PipelineLink<Stage>) {
    return item.id;
  }

  clickNode(node: PipelineNode<Stage>) {
    if (['START', 'END'].includes(node.type)) {
      return;
    }

    this.selectedChange.emit(node.data);
  }

  private getStatus(node: PipelineNode<Stage>) {
    return ['FINISHED', 'NOT_BUILT'].includes(node.data.status)
      ? node.data.result
      : node.data.status;
  }

  nodeClass(node: PipelineNode<Stage>) {
    if (!node.data) {
      return '';
    }

    const prefix = 'acl-pipeline-diagram__node-icon--';

    switch (this.getStatus(node)) {
      case 'QUEUED':
        return `${prefix}queued`;
      case 'PENDING':
        return `${prefix}pending`;
      case 'RUNNING':
        return `${prefix}running`;
      case 'PAUSED':
        return `${prefix}paused`;
      case 'FAILURE':
        return `${prefix}failed`;
      case 'SUCCESS':
        return `${prefix}complete`;
      case 'CANCELLED':
        return `${prefix}cancelled`;
      case 'ABORTED':
        return `${prefix}aborted`;
      default:
        return '';
    }
  }

  nodeIcon(node: PipelineNode<Stage>) {
    if (!node.data) {
      return '';
    }

    switch (this.getStatus(node)) {
      case 'QUEUED':
        return 'basic:hourglass_half_circle_s';
      case 'PENDING':
        return 'basic:play_circle_s';
      case 'RUNNING':
        return 'basic:sync_circle_s';
      case 'PAUSED':
        return 'basic:paused_circle_s';
      case 'FAILURE':
        return 'basic:close_circle_s';
      case 'SUCCESS':
        return 'check_circle_s';
      case 'CANCELLED':
        return 'basic:stop_circle_s';
      case 'ABORTED':
        return 'basic:stop_circle_s';
      default:
        return '';
    }
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  linkPath(link: PipelineLink<Stage>) {
    const line = stepLineWithRadius(this.lineRadius);
    const skipLine = stepLineWithRadius(this.skipLineRadius);

    if (!link.byPass && link.from.y === link.to.y) {
      return `M${
        link.from.type === 'START'
          ? 0
          : link.from.x * this.gridX - this.endOffset
      } ${link.from.y * this.gridY} L${
        link.to.type === 'END'
          ? link.to.x * this.gridX - this.endOffset * 2
          : link.to.x * this.gridX
      } ${link.to.y * this.gridY}`;
    }

    if (link.from.y !== link.to.y) {
      const radiusOffset =
        link.from.y > link.to.y ? -this.lineRadius : +this.lineRadius;
      if (link.from.skipped) {
        return line([
          [
            ((link.from.x + link.to.x) * this.gridX) / 2 +
              radiusOffset -
              this.endOffset,
            link.from.y * this.gridY + this.skipLineRadius,
          ],
          [
            ((link.from.x + link.to.x) * this.gridX) / 2 +
              radiusOffset -
              this.endOffset,
            link.to.y * this.gridY,
          ],
          [
            link.to.type === 'END'
              ? link.to.x * this.gridX + radiusOffset - 2 * this.endOffset
              : link.to.x * this.gridX + radiusOffset - this.endOffset,
            link.to.y * this.gridY,
          ],
        ]);
      }

      if (link.to.skipped) {
        return line([
          [
            link.from.type === 'START'
              ? radiusOffset
              : link.from.x * this.gridX - this.endOffset + radiusOffset,
            link.from.y * this.gridY,
          ],
          [
            ((link.from.x + link.to.x) * this.gridX) / 2 +
              radiusOffset -
              this.endOffset,
            link.from.y * this.gridY,
          ],
          [
            ((link.from.x + link.to.x) * this.gridX) / 2 +
              radiusOffset -
              this.endOffset,
            link.to.y * this.gridY + this.skipLineRadius,
          ],
        ]);
      }

      return line([
        [
          link.from.type === 'START'
            ? radiusOffset
            : link.from.x * this.gridX + radiusOffset - this.endOffset,
          link.from.y * this.gridY,
        ],
        [
          ((link.from.x + link.to.x) * this.gridX) / 2 +
            radiusOffset -
            this.endOffset,
          link.from.y * this.gridY,
        ],
        [
          ((link.from.x + link.to.x) * this.gridX) / 2 +
            radiusOffset -
            this.endOffset,
          link.to.y * this.gridY,
        ],
        [
          link.to.type === 'END'
            ? link.to.x * this.gridX + radiusOffset - 2 * this.endOffset
            : link.to.x * this.gridX + radiusOffset - this.endOffset,
          link.to.y * this.gridY,
        ],
      ]);
    }

    if (link.byPass) {
      return skipLine([
        [
          link.from.type === 'START'
            ? 0
            : link.from.x * this.gridX - this.skipLineRadius - this.endOffset,
          link.from.y * this.gridY,
        ],
        [
          link.from.type === 'START'
            ? this.skipLineRadius
            : (link.from.x + 0.5) * this.gridX -
              this.skipLineRadius -
              this.endOffset,
          link.from.y * this.gridY,
        ],
        [
          link.from.type === 'START'
            ? this.skipLineRadius
            : (link.from.x + 0.5) * this.gridX -
              this.skipLineRadius -
              this.endOffset,
          (link.from.y + 0.5) * this.gridY,
        ],
        [
          link.to.type === 'END'
            ? link.to.x * this.gridX - this.endOffset * 2 - this.skipLineRadius
            : (link.to.x - 0.5) * this.gridX +
              this.skipLineRadius -
              this.endOffset,
          (link.to.y + 0.5) * this.gridY,
        ],
        [
          link.to.type === 'END'
            ? link.to.x * this.gridX - this.endOffset * 2 - this.skipLineRadius
            : (link.to.x - 0.5) * this.gridX +
              this.skipLineRadius -
              this.endOffset,
          link.to.y * this.gridY,
        ],
        [
          link.to.type === 'END'
            ? link.to.x * this.gridX - this.endOffset * 2
            : link.to.x * this.gridX + this.skipLineRadius - this.endOffset,
          link.to.y * this.gridY,
        ],
      ]);
    }
  }
}
