import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { PipelineDiagram, Stage, transformPreviewDiagram } from './models';

@Component({
  selector: 'acl-pipeline-preview-diagram',
  template: `
    <acl-pipeline-view
      *ngIf="!!stages"
      [model]="model"
      [endNodeRadius]="endNodeRadius"
      [nodeRadius]="nodeRadius"
      [gridX]="gridX"
      [endOffset]="endOffset"
      [hideStatus]="hideStatus"
    ></acl-pipeline-view>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelinePreviewDiagramComponent {
  @Input() stages: Stage[];

  @Input() endNodeRadius = 4;

  @Input() nodeRadius = 8;

  @Input() gridX = 64;

  @Input() endOffset = 16;

  @Input() hideStatus = false;

  get model(): PipelineDiagram<Stage> {
    return transformPreviewDiagram(this.stages || []);
  }
}
