export const stepLineWithRadius = (radius: number) => (
  points: Array<[number, number]>,
  // eslint-disable-next-line sonarjs/cognitive-complexity
) => {
  if (points.length < 2) {
    return 'M0,0';
  }

  const [head, ...rest] = points;

  const {
    path,
    pending: [, last],
  } = rest.reduce<{ path: string; pending: number[][] }>(
    (accum, point) => {
      const { path: accPath, pending } = accum;

      if (pending.length < 2) {
        return {
          ...accum,
          pending: [...pending, point],
        };
      }

      const [start, breaking] = pending;

      const [x1, y1] = start;
      const [bx, by] = breaking;
      const [x2, y2] = point;

      const startFlagX = bx === x1 ? 0 : bx > x1 ? 1 : -1;
      const startFlagY = by === y1 ? 0 : by > y1 ? 1 : -1;
      const endFlagX = bx === x2 ? 0 : bx < x2 ? 1 : -1;
      const endFlagY = by === y2 ? 0 : by < y2 ? 1 : -1;

      return {
        path: `${accPath}L ${bx - startFlagX * radius} ${
          by - startFlagY * radius
        } Q${bx} ${by} ${bx + endFlagX * radius} ${by + endFlagY * radius}`,
        pending: [breaking, point],
      };
    },
    {
      path: `M${head[0]} ${head[1]} `,
      pending: [head],
    },
  );

  return `${path} L${last[0]} ${last[1]}`;
};
