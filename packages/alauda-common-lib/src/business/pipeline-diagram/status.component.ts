import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { PipelineDiagram, Stage, transformStatusDiagram } from './models';

@Component({
  selector: 'acl-pipeline-status-diagram',
  template: `
    <acl-pipeline-view
      *ngIf="!!stages"
      [model]="model"
      [hideStatus]="hideStatus"
      [selected]="selected"
      (selectedChange)="selectedChange.emit($event)"
    ></acl-pipeline-view>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineStatusDiagramComponent {
  @Input() stages: Stage[];

  @Input() selected: Stage;

  @Input() hideStatus = false;

  @Output() selectedChange = new EventEmitter<Stage>();

  get model(): PipelineDiagram<Stage> {
    return transformStatusDiagram(this.stages || []);
  }
}
