/* eslint-disable require-atomic-updates */
export interface Edge {
  id: string;
  type: 'STAGE' | 'PARALLEL';
}

export interface Stage {
  id: string;
  name?: string;
  displayName?: string;
  type: 'STAGE' | 'PARALLEL';
  durationInMillis: number;
  startTime: string;
  result: string;
  status: string;
  edges?: Edge[];
}

export interface PipelineNode<T> {
  id: string;
  x: number;
  y: number;
  // tslint:disable-next-line: max-union-size
  type: 'STAGE' | 'PARALLEL' | 'START' | 'END' | 'STAGE-COLLAPSED';
  parallels: Array<PipelineNode<T>>;
  skipped: boolean;
  passed: boolean;
  data: T;
}

export interface PipelineLink<T> {
  id: string;
  from: PipelineNode<T>;
  to: PipelineNode<T>;
  byPass: boolean;
}

export interface PipelineDiagram<T> {
  nodes: Array<PipelineNode<T>>;
  links: Array<PipelineLink<T>>;
  size: [number, number];
}

// eslint-disable-next-line sonarjs/cognitive-complexity
export function* createNodeIteratorByStages(
  stages: Stage[],
): IterableIterator<PipelineNode<Stage>> {
  if (!stages || stages.length === 0) {
    return;
  }
  const byId = stages.reduce((accum, item) => {
    return {
      ...accum,
      [item.id]: item,
    };
  }, {} as { [name: string]: Stage });

  // tslint:disable-next-line: no-shadowed-variable
  function getParallels(edges: Edge[], x: number) {
    if (edges.length <= 0 || edges[0].type !== 'PARALLEL') {
      return [];
    }

    return edges.map(
      (edge, y) =>
        ({
          id: edge.id,
          x,
          y,
          type: 'PARALLEL',
          parallels: [],
          skipped: byId[edge.id].status === 'SKIPPED',
          passed:
            // eslint-disable-next-line @typescript-eslint/no-use-before-define
            next.status &&
            !['NOT_BUILT', 'SKIPPED', 'RUNNING'].includes(byId[edge.id].status),
          data: byId[edge.id],
        } as PipelineNode<Stage>),
    );
  }
  yield {
    id: '__start',
    x: 0,
    y: 0,
    type: 'START',
    parallels: [],
    skipped: false,
    passed: true,
    data: null,
  };

  let next = stages[0];
  let x = 1;

  while (next) {
    const { edges = [] } = next;
    const parallels = getParallels(edges, x);
    yield {
      id: next.id,
      x,
      y: 0,
      type: 'STAGE',
      parallels,
      skipped: next.status === 'SKIPPED',
      passed:
        next.status &&
        !['NOT_BUILT', 'SKIPPED', 'RUNNING'].includes(next.status),
      data: next,
    };

    if (edges.length >= 1 && edges[0].type === 'PARALLEL') {
      const childEdges = parallels[0].data.edges;
      if (childEdges.length > 0) {
        next = byId[childEdges[0].id];
      } else {
        next = null;
      }
    } else if (edges.length === 1) {
      next = byId[edges[0].id];
    } else {
      next = null;
    }

    x++;
  }

  yield {
    id: '__end',
    x,
    y: 0,
    type: 'END',
    parallels: [],
    skipped: false,
    passed: true,
    data: null,
  };
}

export const travel = <T, TResult>(
  nodes: IterableIterator<PipelineNode<T>> | Array<PipelineNode<T>>,
  reducer: (accum: TResult, item: PipelineNode<T>) => TResult,
): TResult => {
  let status: TResult = null;

  for (const node of nodes) {
    status = reducer(status, node);
  }

  return status;
};

export const nodesReducer = <T>(
  accum: Array<PipelineNode<T>>,
  item: PipelineNode<T>,
) => [...(accum || []), item];

export const nodesWithoutParallelsReducer = <T>(
  accum: Array<PipelineNode<T>>,
  item: PipelineNode<T>,
) => [
  ...(accum || []),
  item.parallels.length > 0
    ? ({ ...item, type: 'STAGE-COLLAPSED', parallels: [] } as PipelineNode<T>)
    : item,
];

export const sizeReducer = <T>(
  accum: [number, number],
  item: PipelineNode<T>,
): [number, number] => {
  const [x, y] = accum || [0, 1];

  return [x + 1, y > item.parallels.length ? y : item.parallels.length];
};

export const sizeWithoutParallelsReducer = <T>(
  accum: [number, number],
  _item: PipelineNode<T>,
): [number, number] => {
  const [x] = accum || [0, 1];

  return [x + 1, 1];
};

export const linksReducer = <T>(
  accum: { links: Array<PipelineLink<T>>; pending: PipelineNode<T> },
  item: PipelineNode<T>,
) => {
  const { links, pending } = accum || { links: [], pending: null };

  if (!pending) {
    return {
      links,
      pending: item,
    };
  }

  if (pending.parallels.length === 0 && item.parallels.length === 0) {
    return {
      links: [
        ...links,
        {
          id: `${pending.id}/${item.id}`,
          from: pending,
          to: item,
          byPass: false,
        },
      ],
      pending: item,
    };
  } else {
    const startForks = item.parallels
      .map(parallel => ({
        id: `${pending.id}/${parallel.id}`,
        from: pending,
        to: parallel,
        byPass: false,
      }))
      .reverse();
    const endForks = pending.parallels
      .map(parallel => ({
        id: `${parallel.id}/${item.id}`,
        from: parallel,
        to: item,
        byPass: false,
      }))
      .reverse();

    return {
      links: [...links, ...startForks, ...endForks],
      pending: item,
    };
  }
};

export const byPassLinksReducer = <T>(
  accum: {
    links: Array<PipelineLink<T>>;
    pending: PipelineNode<T>;
    foundSkip: boolean;
  },
  item: PipelineNode<T>,
) => {
  const { links, pending, foundSkip } = accum || {
    links: [],
    pending: null as PipelineNode<T>,
    foundSkip: false,
  };

  if (foundSkip && !item.skipped) {
    return {
      links: [
        ...links,
        {
          id: `${pending.id}/${item.id}`,
          from: pending,
          to: item,
          byPass: true,
        },
      ],
      pending: item,
      foundSkip: false,
    };
  }

  if (!foundSkip && item.skipped) {
    return {
      links,
      pending,
      foundSkip: true,
    };
  }

  if (!foundSkip && !item.skipped) {
    return {
      links,
      pending: item,
      foundSkip: false,
    };
  }

  return accum;
};

export const linksWithoutParallelsReducer = <T>(
  accum: { links: Array<PipelineLink<T>>; pending: PipelineNode<T> },
  item: PipelineNode<T>,
) => {
  const { links, pending } = accum || { links: [], pending: null };

  if (pending) {
    return {
      links: [
        ...links,
        {
          id: `${pending.id}/${item.id}`,
          from: pending,
          to: item,
          byPass: false,
        },
      ],
      pending: item,
    };
  } else {
    return {
      links,
      pending: item,
    };
  }
};

type ReducerMap<TResult, T> = {
  [p in keyof TResult]: (accum: TResult[p], item: T) => TResult[p];
};

function combineReducer<TResult, T>(
  reducers: ReducerMap<TResult, T>,
): (accum: TResult, item: T) => TResult {
  const reducerKeys = Object.keys(reducers) as Array<keyof TResult>;

  return (accum, item) =>
    reducerKeys.reduce(
      (prev, key) =>
        Object.assign({}, prev, {
          [key]: reducers[key](prev ? prev[key] : null, item),
        }),
      accum,
    );
}

interface PipelineStatus<T> {
  nodes: Array<PipelineNode<T>>;
  baseLinks: { links: Array<PipelineLink<T>>; pending: PipelineNode<T> };
  byPassLinks: {
    links: Array<PipelineLink<T>>;
    pending: PipelineNode<T>;
    foundSkip: boolean;
  };
  size: [number, number];
}

interface PipelinePreview<T> {
  nodes: Array<PipelineNode<T>>;
  stageLinks: { links: Array<PipelineLink<T>>; pending: PipelineNode<T> };
  size: [number, number];
}

export function transformStatusDiagram(
  stages: Stage[],
): PipelineDiagram<Stage> {
  const iterator = createNodeIteratorByStages(stages);
  const { nodes, baseLinks, byPassLinks, size } = travel(
    iterator,
    combineReducer<PipelineStatus<Stage>, PipelineNode<Stage>>({
      nodes: nodesReducer,
      baseLinks: linksReducer,
      byPassLinks: byPassLinksReducer,
      size: sizeReducer,
    }),
  );

  return {
    nodes,
    links: [...baseLinks.links, ...byPassLinks.links],
    size,
  };
}

export function transformPreviewDiagram(
  stages: Stage[],
): PipelineDiagram<Stage> {
  const iterator = createNodeIteratorByStages(stages);
  const { nodes, stageLinks, size } = travel(
    iterator,
    combineReducer<PipelinePreview<Stage>, PipelineNode<Stage>>({
      nodes: nodesWithoutParallelsReducer,
      stageLinks: linksWithoutParallelsReducer,
      size: sizeWithoutParallelsReducer,
    }),
  );

  return {
    nodes,
    links: stageLinks.links,
    size,
  };
}
