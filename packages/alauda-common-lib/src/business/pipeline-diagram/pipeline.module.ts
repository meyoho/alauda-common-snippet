import { IconModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PipelinePreviewDiagramComponent } from './preview.component';
import { PipelineStatusDiagramComponent } from './status.component';
import { PipelineViewComponent } from './view.component';

@NgModule({
  imports: [CommonModule, IconModule],
  declarations: [
    PipelineViewComponent,
    PipelineStatusDiagramComponent,
    PipelinePreviewDiagramComponent,
  ],
  exports: [
    PipelineViewComponent,
    PipelineStatusDiagramComponent,
    PipelinePreviewDiagramComponent,
  ],
})
export class PipelineDiagramModule {}
