import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { DynamicFormDefDirective } from './dynamic-form-def.directive';
import { DynamicFormOutletDirective } from './dynamic-form-outlet.directive';
import { DynamicFormComponent } from './dynamic-form.component';
import { DynamicFormService } from './dynamic-form.service';

@NgModule({
  declarations: [
    DynamicFormComponent,
    DynamicFormOutletDirective,
    DynamicFormDefDirective,
  ],
  exports: [DynamicFormComponent, DynamicFormDefDirective],
  imports: [CommonModule, ReactiveFormsModule],
  providers: [DynamicFormService],
})
export class DynamicFormsModule {}
