export type ReducerMap<TResult, T> = {
  [p in keyof TResult]: (accum: TResult[p], item: T) => TResult[p];
};

export function combineReducer<TResult, T>(
  reducers: ReducerMap<TResult, T>,
): (accum: TResult, item: T) => TResult {
  const reducerKeys = Object.keys(reducers) as Array<keyof TResult>;

  return (accum, item) => {
    return reducerKeys.reduce((prev, key) => {
      return Object.assign({}, prev, {
        [key]: reducers[key](prev ? prev[key] : null, item),
      });
    }, accum);
  };
}
