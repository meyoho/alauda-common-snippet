import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[aclDynamicFormDef]',
})
export class DynamicFormDefDirective {
  constructor(public template: TemplateRef<any>) {}
}
