import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[aclDynamicFormOutlet]',
})
export class DynamicFormOutletDirective {
  constructor(public viewContainer: ViewContainerRef) {}
}
