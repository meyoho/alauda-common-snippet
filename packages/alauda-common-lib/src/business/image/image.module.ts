import {
  AutocompleteModule,
  ButtonModule,
  DialogModule,
  FormModule,
  IconModule,
  InputModule,
  RadioModule,
  SelectModule,
  TagModule,
  TooltipModule,
} from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { DisabledContainerModule } from '../../disabled-container/public-api';
import { UtilsModule } from '../../utils/public-api';

import { IMAGE_INTL_PROVIDER } from './image.intl';
import {
  ImagePullControlComponent,
  ImagePullDialogComponent,
  ImagePullInputComponent,
  ImagePullSelectComponent,
} from './pull-control/public-api';
import {
  ImagePushControlComponent,
  ImagePushCreateComponent,
  ImagePushDialogComponent,
  ImagePushInputComponent,
  ImagePushSelectComponent,
  ImagePushTagsComponent,
} from './push-control/public-api';

export const EXPORTS = [ImagePullControlComponent, ImagePushControlComponent];

export const PRIVATES = [
  ImagePushDialogComponent,
  ImagePullDialogComponent,
  ImagePullSelectComponent,
  ImagePullInputComponent,
  ImagePushSelectComponent,
  ImagePushCreateComponent,
  ImagePushInputComponent,
  ImagePushTagsComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IconModule,
    DialogModule,
    FormModule,
    RadioModule,
    ButtonModule,
    SelectModule,
    InputModule,
    TooltipModule,
    TagModule,
    AutocompleteModule,
    UtilsModule,
    DisabledContainerModule,
  ],
  declarations: [EXPORTS, PRIVATES],
  exports: EXPORTS,
  entryComponents: [ImagePullDialogComponent, ImagePushDialogComponent],
  providers: [IMAGE_INTL_PROVIDER],
})
export class ImageModule {}
