import { Injectable, Optional, SkipSelf } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ImageIntl {
  changes = new BehaviorSubject('zh-CN');
  pipelineSelect = '选择';
  pipelineSelectImage = '选择镜像';
  pipelineSelectImagePlaceholder = '选择镜像仓库';
  pipelineMethod = '方式';
  pipelineInput = '输入';
  pipelineConfirm = '确定';
  cancel = '取消';
  pipelineImageRepository = '镜像仓库';
  pipelineSelectOrInputImageTag = '选择或输入版本';
  pipelineImageRepositoryAddress = '镜像地址';
  pipelineInputRegistryAddressTip = '请输入“镜像仓库:版本”，例如';
  pipelineDefaultRegistryTips = '如没有输入镜像版本，则版本默认为 latest。';
  pipelineSecret = '凭据';
  noData = '无数据';
  pipelineSecretAdd = '添加凭据';
  pipelineImage = '镜像';
  pipelineNew = '新建';
  pipelineRepositoryAddress = '仓库地址';
  pipelineTemplateVersionChange = '版本';
  pipelineAdd = '添加';
  pipelineChange = '修改';
}

export function IMAGE_INTL_PROVIDER_FACTORY(parentIntl: ImageIntl) {
  return parentIntl || new ImageIntl();
}

export const IMAGE_INTL_PROVIDER = {
  provide: ImageIntl,
  deps: [[new Optional(), new SkipSelf(), ImageIntl]],
  useFactory: IMAGE_INTL_PROVIDER_FACTORY,
};
