import { OptionComponent } from '@alauda/ui';
import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';

import { publishRef, withLoadState } from '../../../../utils/public-api';
import {
  ImageRepositoryOption,
  ImageSelectorDataContext,
} from '../../data-context';
import { ImageIntl } from '../../image.intl';

@Component({
  selector: 'acl-image-push-select',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
})
export class ImagePushSelectComponent {
  @Input()
  form: FormGroup;

  private _context: ImageSelectorDataContext;

  private readonly context$$ = new BehaviorSubject<ImageSelectorDataContext>(
    null,
  );

  @Input()
  get context() {
    return this._context;
  }

  set context(context: ImageSelectorDataContext) {
    this._context = context;
    this.context$$.next(context);
  }

  imageRepositories$ = this.context$$.pipe(
    filter(context => !!context),
    switchMap(context => withLoadState(context.getImageRepositories(), [])),
    publishRef(),
  );

  get tags() {
    return (
      this.form &&
      (this.form.controls.tags as import('@angular/forms').FormArray)
    );
  }

  constructor(public intl: ImageIntl) {}

  getImage = (name: string) => {
    return this.context?.getAssetsIconPath(name);
  };

  repositoryTracker(repository: Partial<ImageRepositoryOption>) {
    return repository?.repositoryPath || '';
  }

  filterFn(filter: string, option: Partial<OptionComponent>) {
    return (option.value as Partial<
      ImageRepositoryOption
    >).repositoryPath.includes(filter);
  }
}
