import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap } from 'rxjs/operators';

import { publishRef } from '../../../../utils/public-api';
import { ExportOption, ImageSelectorDataContext } from '../../data-context';
import { ImageIntl } from '../../image.intl';

@Component({
  selector: 'acl-image-push-tags',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImagePushTagsComponent {
  @Input() form: import('@angular/forms').FormArray;

  private readonly context$$ = new BehaviorSubject<ImageSelectorDataContext>(
    null,
  );

  @Input()
  set context(context: ImageSelectorDataContext) {
    this.context$$.next(context);
  }

  intlChangeSubscription = this.intl.changes.subscribe(() =>
    this.cdr.markForCheck(),
  );

  suggestions$: Observable<ExportOption[]> = this.context$$.pipe(
    filter(context => !!context),
    switchMap(context =>
      context.getExportOptions().pipe(
        map(items => items || []),
        catchError(() => of([])),
      ),
    ),
    publishRef(),
  );

  constructor(
    private readonly fb: FormBuilder,
    public intl: ImageIntl,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  addTag() {
    this.form.push(this.fb.control('', Validators.required));
  }

  removeTag(index: number) {
    this.form.removeAt(index);
  }

  description(
    tag: ExportOption,
    language: keyof ExportOption['description'] = 'zh-CN',
  ) {
    return tag.description[language] || '';
  }

  wrapper(name: string) {
    return `$\{${name}}`;
  }
}
