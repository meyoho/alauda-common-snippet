import { OptionComponent } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap } from 'rxjs/operators';

import { withLoadState } from '../../../../utils/public-api';
import {
  ExportOption,
  ImageRegistryOption,
  ImageRepositoryOption,
  ImageSelectorDataContext,
  groupRegistryOptions,
} from '../../data-context';
import { ImageIntl } from '../../image.intl';

@Component({
  selector: 'acl-image-push-create',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImagePushCreateComponent {
  @Input() form: FormGroup;

  private _context: ImageSelectorDataContext;

  private readonly context$$ = new BehaviorSubject<ImageSelectorDataContext>(
    null,
  );

  @Input()
  get context() {
    return this._context;
  }

  set context(context: ImageSelectorDataContext) {
    this._context = context;
    this.context$$.next(context);
  }

  get tags() {
    return (
      this.form &&
      (this.form.controls.tags as import('@angular/forms').FormArray)
    );
  }

  get selectedImageRegistry(): ImageRegistryOption {
    return this.form?.controls.registry.value;
  }

  imageRegistries$ = this.context$$.pipe(
    filter(context => !!context),
    switchMap(context =>
      withLoadState(context.getImageRepositories('create'), []),
    ),
    map(({ data, ...rest }) => ({
      ...rest,
      data: groupRegistryOptions(data),
    })),
  );

  tagSuggestions$: Observable<ExportOption[]> = this.context$$.pipe(
    filter(context => !!context),
    switchMap(context =>
      context.getExportOptions().pipe(
        map(items => items || []),
        catchError(() => of([])),
      ),
    ),
  );

  constructor(public intl: ImageIntl) {}

  registryTracker(registry: Partial<ImageRegistryOption>) {
    return registry?.projectPath || '';
  }

  getImage = (name: string) => {
    return this.context?.getAssetsIconPath(name);
  };

  getImageSuggestions(registry: ImageRegistryOption) {
    return registry?.images;
  }

  filterFn(filter: string, option: Partial<OptionComponent>) {
    return (option.value as Partial<
      ImageRepositoryOption
    >).projectPath.includes(filter);
  }
}
