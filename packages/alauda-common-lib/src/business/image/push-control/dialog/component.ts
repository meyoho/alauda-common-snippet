import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { dropRight, get, last, omit, takeRight, union } from 'lodash-es';
import { distinctUntilChanged, map, startWith } from 'rxjs/operators';

import {
  ImageRegistryOption,
  ImageRepositoryOption,
  ImageRepositoryValue,
  ImageSelectorDataContext,
  SecretOption,
} from '../../data-context';
import { ImageIntl } from '../../image.intl';

type FormModel = Pick<
  ImageRepositoryValue,
  Exclude<
    keyof ImageRepositoryValue,
    // tslint:disable-next-line: max-union-size
    'secretName' | 'secretNamespace' | 'credentialId' | 'tag' | 'repositoryPath'
  >
> & {
  secret: SecretOption;
  tags: string[];
  fullPath: string;
  repository: Partial<ImageRepositoryOption>;
  registry: Partial<ImageRegistryOption>;
  newRepositoryPath: string;
};

export interface ImagePushDialogData {
  value: ImageRepositoryValue;
  context: ImageSelectorDataContext;
}

@Component({
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImagePushDialogComponent implements OnDestroy {
  form = this.buildForm();

  type$ = this.form.valueChanges.pipe(
    map(value => value.type),
    distinctUntilChanged(),
    startWith(this.form.value.type),
  );

  intlChangeSubscription = this.intl.changes.subscribe(() =>
    this.cdr.markForCheck(),
  );

  constructor(
    private readonly fb: FormBuilder,
    private readonly dialogRef: DialogRef<ImagePushDialogComponent>,
    public intl: ImageIntl,
    private readonly cdr: ChangeDetectorRef,
    @Inject(DIALOG_DATA) public data: ImagePushDialogData,
  ) {}

  ngOnDestroy() {
    this.intlChangeSubscription.unsubscribe();
  }

  buildForm() {
    const { tags, ...rest } = fromValue(this.data.value);

    return this.fb.group(
      {
        ...rest,
        tags: this.fb.array(
          tags.length > 0
            ? tags.map(tag =>
                this.fb.control(tag, [
                  Validators.required,
                  Validators.pattern('^[^,]*[^ ,][^,]*$'),
                ]),
              )
            : [
                this.fb.control('latest', [
                  Validators.required,
                  Validators.pattern('^[^,]*[^ ,][^,]*$'),
                ]),
              ],
        ),
      },
      { validators: this.formValidator },
    );
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  formValidator(control: FormGroup): ValidationErrors {
    const type = control.get('type');
    const tags =
      (get(control.get('tags'), 'value') || []).length === 0
        ? { required: true }
        : null;

    switch (type?.value) {
      case 'select': {
        const repository = !get(control.get('repository'), [
          'value',
          'repositoryPath',
        ])
          ? { required: true }
          : null;
        return repository || tags ? { repository, tags } : null;
      }
      case 'create': {
        const registry = !get(control.get('registry'), ['value', 'projectPath'])
          ? { required: true }
          : null;
        const newRepositoryPath = Validators.required(
          control.get('newRepositoryPath'),
        );
        return registry || newRepositoryPath || tags
          ? { registry, tags, newRepositoryPath }
          : null;
      }
      case 'input': {
        const fullPathValidator = Validators.compose([
          Validators.required,
          imageUrlWithoutTagValidator,
        ]);
        const fullPath = fullPathValidator(control.get('fullPath'));

        return fullPath || tags ? { fullPath, tags } : null;
      }
      default:
        return null;
    }
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    this.dialogRef.close(toValue(this.form.value));
  }
}

function imageUrlWithoutTagValidator(
  control: AbstractControl,
): null | { [key: string]: boolean } {
  const value = control.value as string;
  if (last(value.split('/')).includes(':')) {
    return { no_tags: true };
  }
  return null;
}

function fromValue(value: ImageRepositoryValue): FormModel {
  const defaultValue: ImageRepositoryValue = {
    type: 'select',
    repositoryPath: '',
    secretName: null,
    secretNamespace: null,
    tag: '',
    credentialId: '',
  };

  const { secretName, secretNamespace, repositoryPath, tag, type, ...rest } =
    value || defaultValue;

  const secret =
    (secretName && {
      name: secretName,
      namespace: secretNamespace,
    }) ||
    null;

  const tags = tag ? tag.split(',') : [];
  const repository = type === 'select' ? { repositoryPath, secret } : null;
  const pathSegments = repositoryPath ? repositoryPath.split('/') : [];
  const projectPath =
    pathSegments.length > 0 ? dropRight(pathSegments).join('/') : '';
  const [newRepositoryPath] = takeRight(pathSegments);
  const registry = type === 'create' ? { projectPath, secret } : null;

  return {
    type,
    ...omit(rest, 'credentialId'),
    secret,
    repository,
    tags,
    fullPath: repositoryPath,
    registry,
    newRepositoryPath,
  };
}

function toValue({
  type,
  secret,
  fullPath,
  repository,
  tags,
  registry,
  newRepositoryPath,
}: FormModel) {
  const { name, namespace } = secret || {
    name: '',
    namespace: '',
  };

  if (type === 'input') {
    return {
      type,
      repositoryPath: fullPath,
      tag: tags.join(','),
      secretName: name,
      secretNamespace: namespace,
      credentialId: namespace ? `${namespace}-${name}` : name,
    };
  }

  const bindingSecret = get(
    type === 'create' ? registry : repository,
    'secret',
  );
  const secretNamespace = bindingSecret?.namespace;
  const secretName = bindingSecret?.name;
  const credentialId =
    secretNamespace && secretName
      ? `${secretNamespace}-${secretName}`
      : secretName || '';
  const repositoryPath =
    type === 'create'
      ? `${registry.projectPath}/${newRepositoryPath}`
      : repository.repositoryPath;

  return {
    type,
    repositoryPath,
    tag: union(tags).join(','),
    secretName,
    secretNamespace,
    credentialId,
  };
}
