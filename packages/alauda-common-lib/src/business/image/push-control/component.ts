import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnDestroy,
  Optional,
  forwardRef,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

import { noop } from '../../../utils/helpers';
import {
  ImageRepositoryValue,
  ImageSelectorDataContext,
  emptyImageSelectorDataContext,
} from '../data-context';
import { ImageIntl } from '../image.intl';

import { ImagePushDialogComponent } from './dialog/component';

@Component({
  selector: 'acl-image-push-control',
  templateUrl: './component.html',
  styleUrls: ['../styles/repository.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ImagePushControlComponent),
      multi: true,
    },
    ImagePushControlComponent,
  ],
})
export class ImagePushControlComponent
  implements ControlValueAccessor, OnDestroy {
  @Input() context: ImageSelectorDataContext = emptyImageSelectorDataContext;

  repository: ImageRepositoryValue;

  intlChangeSubscription = this.intl.changes.subscribe(() =>
    this.cdr.markForCheck(),
  );

  @HostBinding('class.error-state')
  get errorState(): boolean {
    return this.parentForm?.submitted && this.parentForm.invalid;
  }

  constructor(
    private readonly dialog: DialogService,
    private readonly cdr: ChangeDetectorRef,
    public intl: ImageIntl,
    @Optional() private readonly parentForm: FormGroupDirective,
  ) {}

  ngOnDestroy() {
    this.intlChangeSubscription.unsubscribe();
  }

  writeValue(value: ImageRepositoryValue) {
    this.repository = value;
  }

  valueChanged = noop;

  onTouched = noop;

  registerOnTouched(fn: () => {}) {
    this.onTouched = fn;
  }

  registerOnChange(fn: () => {}) {
    this.valueChanged = fn;
  }

  open() {
    this.dialog
      .open(ImagePushDialogComponent, {
        size: DialogSize.Large,
        data: {
          value: this.repository,
          context: this.context,
        },
      })
      .afterClosed()
      .subscribe(value => {
        if (value) {
          this.repository = value;
          this.valueChanged(value);
        }
        this.cdr.markForCheck();
      });
  }

  repositoryPath({ repositoryPath }: ImageRepositoryValue) {
    return repositoryPath;
  }

  splitTags(tag: string) {
    return !tag ? [] : tag.split(',');
  }
}
