import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject, Subject, merge } from 'rxjs';
import { concatMap, filter, map, switchMap, tap } from 'rxjs/operators';

import { withLoadState } from '../../../../utils/public-api';
import { ImageSelectorDataContext, SecretOption } from '../../data-context';
import { ImageIntl } from '../../image.intl';

@Component({
  selector: 'acl-image-pull-input',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
})
export class ImagePullInputComponent {
  @Input() form: FormGroup;

  private _context: ImageSelectorDataContext;

  private readonly context$$ = new BehaviorSubject<ImageSelectorDataContext>(
    null,
  );

  @Input()
  get context() {
    return this._context;
  }

  set context(context: ImageSelectorDataContext) {
    this._context = context;
    this.context$$.next(context);
  }

  demoRepositoryAddress = 'index.docker.io/library/ubuntu:latest';

  private readonly createSecret$$ = new Subject<void>();

  imageRepositories$ = this.context$$.pipe(
    switchMap(() => withLoadState(this.context.getImageRepositories(), [])),
  );

  secrets$ = merge(
    this.context$$.pipe(
      filter(context => !!context),
      map(() => null),
    ),
    this.createSecret$$.pipe(
      concatMap(() => this.context.createSecret()),
      filter(secret => !!secret),
    ),
  ).pipe(
    switchMap(created =>
      withLoadState(this.context.getSecrets(), []).pipe(
        map(secrets => ({ secrets, created })),
      ),
    ),
    tap(({ created }) => {
      if (created) {
        this.form.patchValue({ secret: created });
      }
    }),
    map(({ secrets }) => secrets),
  );

  canCreateSecret$ = this.context$$.pipe(
    filter(context => !!context),
    switchMap(() => withLoadState(this.context.canCreateSecret(), false)),
  );

  constructor(public intl: ImageIntl) {}

  addSecret() {
    this.createSecret$$.next();
  }

  secretTracker(secret: SecretOption) {
    return (secret && `${secret.namespace}/${secret.name}`) || '';
  }

  setDemoAddress() {
    this.form.get('fullPath').patchValue(this.demoRepositoryAddress);
  }
}
