import { OptionComponent } from '@alauda/ui';
import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject, Observable, combineLatest, merge, of } from 'rxjs';
import {
  concatMap,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';

import { publishRef, withLoadState } from '../../../../utils/public-api';
import {
  ImageRepositoryOption,
  ImageSelectorDataContext,
} from '../../data-context';
import { ImageIntl } from '../../image.intl';

export interface SelectedImageRepository {
  value: string;
  isChange: boolean;
}

@Component({
  selector: 'acl-image-pull-select',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
})
export class ImagePullSelectComponent {
  _form: FormGroup;

  private readonly form$$ = new BehaviorSubject<FormGroup>(null);

  @Input()
  get form() {
    return this._form;
  }

  set form(form: FormGroup) {
    this._form = form;
    this.form$$.next(form);
  }

  private _context: ImageSelectorDataContext;

  private readonly context$$ = new BehaviorSubject<ImageSelectorDataContext>(
    null,
  );

  @Input()
  get context() {
    return this._context;
  }

  set context(context: ImageSelectorDataContext) {
    this._context = context;
    this.context$$.next(context);
  }

  imageRepositories$ = this.context$$.pipe(
    filter(context => !!context),
    switchMap(() => withLoadState(this.context.getImageRepositories(), [])),
    publishRef(),
  );

  private readonly selectedImageRepository$: Observable<
    SelectedImageRepository
  > = merge(
    this.form$$.pipe(
      filter(form => !!form),
      concatMap(form => form.valueChanges),
      map(value => value.repository?.repositoryPath || ''),
      map((value: string) => ({ value, isChange: true })),
    ),
    this.form$$.pipe(
      filter(form => !!form),
      take(1),
      map(form => form.value.repository?.repositoryPath || ''),
      map((value: string) => ({ value, isChange: false })),
    ),
  ).pipe(
    distinctUntilChanged((a, b) => a.value === b.value),
    // clean selected tag when repository change
    tap(
      ({ value, isChange }) =>
        (isChange || !value) && this.form.patchValue({ tag: '' }),
    ),
  );

  tags$ = combineLatest([
    this.selectedImageRepository$,
    this.imageRepositories$,
  ]).pipe(
    switchMap(([{ value }, { loading, data }]) => {
      if (!data || data.length === 0) {
        return of({
          loading,
          data: [],
        });
      }

      const selected = data.find(item => item.repositoryPath === value);

      if (!selected) {
        return of({
          loading,
          data: [],
        });
      }

      return withLoadState(this.context.getTags(selected), []);
    }),
  );

  constructor(public intl: ImageIntl) {}

  getImage = (name: string) => {
    return this.context?.getAssetsIconPath(name);
  };

  repositoryTracker(repository: Partial<ImageRepositoryOption>) {
    return repository?.repositoryPath || '';
  }

  filterFn(filter: string, option: Partial<OptionComponent>) {
    return (option.value as Partial<
      ImageRepositoryOption
    >).repositoryPath.includes(filter);
  }
}
