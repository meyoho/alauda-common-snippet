import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { dropRight, get, omit, takeRight } from 'lodash-es';
import { distinctUntilChanged, map, startWith } from 'rxjs/operators';

import {
  ImageRepositoryOption,
  ImageRepositoryValue,
  ImageSelectorDataContext,
  SecretOption,
} from '../../data-context';
import { ImageIntl } from '../../image.intl';

type FormModel = Pick<ImageRepositoryValue, 'type' | 'tag'> & {
  secret: SecretOption;
  fullPath: string;
  repository: Partial<ImageRepositoryOption>;
};

export interface ImagePullDialogData {
  value: ImageRepositoryValue;
  context: ImageSelectorDataContext;
}

@Component({
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImagePullDialogComponent implements OnDestroy {
  form = this.buildForm();

  isSelect$ = this.form.valueChanges.pipe(
    map(value => value.type === 'select'),
    distinctUntilChanged(),
    startWith(this.form.value.type === 'select'),
  );

  intlChangeSubscription = this.intl.changes.subscribe(() =>
    this.cdr.markForCheck(),
  );

  constructor(
    private readonly fb: FormBuilder,
    private readonly dialogRef: DialogRef<ImagePullDialogComponent>,
    public intl: ImageIntl,
    private readonly cdr: ChangeDetectorRef,
    @Inject(DIALOG_DATA) public data: ImagePullDialogData,
  ) {}

  ngOnDestroy() {
    this.intlChangeSubscription.unsubscribe();
  }

  private buildForm() {
    return this.fb.group(fromValue(this.data.value), {
      validators: this.formValidator,
    });
  }

  formValidator(control: FormGroup): ValidationErrors {
    const type = control.get('type');

    switch (type?.value) {
      case 'select': {
        const repository = !get(control.get('repository'), [
          'value',
          'repositoryPath',
        ])
          ? { required: true }
          : null;
        const tag = Validators.required(control.get('tag'));
        return repository || tag ? { repository, tag } : null;
      }
      case 'input': {
        const fullPathValidator = Validators.compose([
          Validators.required,
          Validators.pattern('.*[:].*'),
        ]);
        const fullPath = fullPathValidator(control.get('fullPath'));
        return fullPath ? { fullPath } : null;
      }
      default:
        return null;
    }
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    this.dialogRef.close(toValue(this.form.value));
  }
}

function fromValue(value: ImageRepositoryValue): FormModel {
  const defaultValue: ImageRepositoryValue = {
    type: 'select',
    repositoryPath: '',
    secretName: null,
    secretNamespace: null,
    tag: '',
    credentialId: '',
  };

  const { secretName, secretNamespace, repositoryPath, tag, type, ...rest } =
    value || defaultValue;

  const secret =
    (secretName && {
      name: secretName,
      namespace: secretNamespace,
    }) ||
    null;

  const repository = type === 'select' ? { repositoryPath, secret } : null;

  return {
    type,
    ...omit(rest, 'credentialId'),
    secret,
    repository,
    tag,
    // tslint:disable-next-line: no-nested-template-literals
    fullPath: repositoryPath ? `${repositoryPath}${tag ? `:${tag}` : ''}` : '',
  };
}

function toValue({
  type,
  secret,
  fullPath,
  repository,
  ...rest
}: FormModel): ImageRepositoryValue {
  const { name, namespace } = secret || {
    name: '',
    namespace: '',
  };

  if (type === 'input') {
    const pathSegments = fullPath.split(':');
    const rightString = takeRight(pathSegments, 1).join();
    const info: { tag: string; repositoryPath: string } = {
      tag: rightString,
      repositoryPath: dropRight(pathSegments, 1).join(':'),
    };
    if (rightString.includes('/')) {
      info.tag = '';
      info.repositoryPath = fullPath;
    }

    return {
      ...rest,
      ...info,
      type,
      secretName: name,
      secretNamespace: namespace,
      credentialId: namespace ? `${namespace}-${name}` : name,
    };
  }

  const secretNamespace = repository.secret?.namespace;
  const secretName = repository.secret?.name;

  return {
    ...rest,
    type,
    repositoryPath: repository.repositoryPath,
    secretName,
    secretNamespace,
    credentialId: secretNamespace
      ? `${secretNamespace}-${secretName}`
      : secretName,
  };
}
