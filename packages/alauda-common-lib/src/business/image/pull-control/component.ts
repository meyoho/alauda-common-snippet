import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnDestroy,
  Optional,
  forwardRef,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

import { noop } from '../../../utils/helpers';
import {
  ImageRepositoryValue,
  ImageSelectorDataContext,
  emptyImageSelectorDataContext,
} from '../data-context';
import { ImageIntl } from '../image.intl';

import { ImagePullDialogComponent } from './dialog/component';

@Component({
  selector: 'acl-image-pull-control',
  templateUrl: './component.html',
  styleUrls: ['../styles/repository.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ImagePullControlComponent),
      multi: true,
    },
    ImagePullControlComponent,
  ],
})
export class ImagePullControlComponent
  implements ControlValueAccessor, OnDestroy {
  @Input() context: ImageSelectorDataContext = emptyImageSelectorDataContext;

  repository: ImageRepositoryValue;

  intlChangeSubscription = this.intl.changes.subscribe(() =>
    this.cdr.markForCheck(),
  );

  @HostBinding('class.error-state')
  get errorState(): boolean {
    return this.parentForm?.submitted && this.parentForm.invalid;
  }

  constructor(
    private readonly dialog: DialogService,
    public intl: ImageIntl,
    private readonly cdr: ChangeDetectorRef,
    @Optional() private readonly parentForm: FormGroupDirective,
  ) {}

  ngOnDestroy() {
    this.intlChangeSubscription.unsubscribe();
  }

  writeValue(value: ImageRepositoryValue) {
    this.repository = value;
  }

  valueChanged = noop;

  onTouched = noop;

  registerOnTouched(fn: () => {}) {
    this.onTouched = fn;
  }

  registerOnChange(fn: () => {}) {
    this.valueChanged = fn;
  }

  open() {
    this.dialog
      .open(ImagePullDialogComponent, {
        size: DialogSize.Large,
        data: { value: this.repository, context: this.context },
      })
      .afterClosed()
      .subscribe((value: ImageRepositoryValue) => {
        if (value) {
          this.repository = value;
          this.valueChanged(value);
          this.cdr.markForCheck();
        }
      });
  }

  fullPath({ tag, repositoryPath }: ImageRepositoryValue) {
    // tslint:disable-next-line: no-nested-template-literals
    return `${repositoryPath}${tag ? `:${tag}` : ''}`;
  }
}
