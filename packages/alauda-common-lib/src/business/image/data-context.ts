import { isDevMode } from '@angular/core';
import { groupBy, head, omit } from 'lodash-es';
import { Observable, of } from 'rxjs';

export interface ImageRepositoryValue {
  type: 'input' | 'select' | 'create';
  repositoryPath: string;
  credentialId: string;
  tag: string;
  secretName: string;
  secretNamespace: string;
}

export interface ImageRepositoryOption {
  name: string;
  type: string;
  endpoint: string; // only registry endpoint path
  image: string; // image name with project
  imageName: string; // image name without project
  repositoryPath: string; // absolute repository path
  projectPath: string; // absolute registry path with project
  tags: TagOption[];
  secret: SecretOption;
}

export interface TagOption {
  name: string;
  digest: string;
  createdAt: string;
  size?: string;
  level?: number;
  severity?: string;
  scanStatus?: ScanStatus;
  message?: string;
  summary?: Array<{
    count: number;
    severity: Severity;
  }>;
}

export enum Severity {
  Unknown = 'Unknown',
  Negligible = 'Negligible',
  Low = 'Low',
  Medium = 'Medium',
  High = 'High',
  Critical = 'Critical',
}

export enum ScanStatus {
  NotScan = 'Not Scanned',
  Pending = 'Pending',
  Running = 'Running',
  Error = 'Error',
  Stopped = 'Stopped',
  Success = 'Success',
  Scheduled = 'Scheduled',
}

export interface SecretOption {
  name: string;
  namespace: string;
}

export interface ExportOption {
  name: string;
  description: {
    'zh-CN': string;
    en: string;
  };
}

export type ImageRegistryOption = Pick<
  ImageRepositoryOption,
  Exclude<
    keyof ImageRepositoryOption,
    // tslint:disable-next-line: max-union-size
    'repositoryPath' | 'image' | 'tags' | 'imageName'
  >
> & {
  images: string[];
};

export function groupRegistryOptions(
  repositories: ImageRepositoryOption[],
): ImageRegistryOption[] {
  const registryMap = groupBy(repositories, item => item.projectPath);

  return Object.keys(registryMap).map(key => {
    const registry = omit(head(registryMap[key]), [
      'repositoryPath',
      'image',
      'tags',
      'imageName',
    ]);

    return {
      ...registry,
      images: registryMap[key].map(item => item.imageName),
    };
  });
}

export interface ImageSelectorDataContext {
  canCreateSecret: () => Observable<boolean>;
  createSecret: () => Observable<SecretOption>;
  getImageRepositories: (mode?: string) => Observable<ImageRepositoryOption[]>;
  getTags: (repository: ImageRepositoryOption) => Observable<TagOption[]>;
  getSecrets: () => Observable<SecretOption[]>;
  getAssetsIconPath: (name: string) => string;
  getExportOptions: () => Observable<ExportOption[]>;
}

export const emptyImageSelectorDataContext: ImageSelectorDataContext = {
  canCreateSecret(): Observable<boolean> {
    needImplement('canCreateSecret');
    return of(true);
  },

  createSecret(): Observable<SecretOption> {
    needImplement('createSecret');
    return of(null);
  },

  getImageRepositories(_mode?: string): Observable<ImageRepositoryOption[]> {
    needImplement('getImageRepositories');
    return of([]);
  },

  getTags(_repository: ImageRepositoryOption): Observable<TagOption[]> {
    needImplement('getTags');
    return of([]);
  },

  getSecrets(): Observable<SecretOption[]> {
    needImplement('getSecrets');
    return of([]);
  },

  getAssetsIconPath(_name: string): string {
    needImplement('getAssetsIconPath');
    return '';
  },
  getExportOptions(): Observable<ExportOption[]> {
    needImplement('getExportOptions');
    return of([]);
  },
};

function needImplement(text: string) {
  if (isDevMode()) {
    console.warn(`${text} not implement`);
  }
}
