export const DEFAULT_REMOTE_URL = 'custom/i18n.{locale}.yaml';

export const LOCALE_STORAGE = '__LOCALE__';

export const LOCALE_PLACEHOLDER_REGEX = /{+\s*locale\s*}+/;

export enum Locale {
  ZH = 'zh',
  EN = 'en',
}
