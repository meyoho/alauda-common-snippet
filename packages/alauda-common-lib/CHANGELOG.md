# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.3.6](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.3.6..v2.3.5#diff) (2020-05-25)


### Bug Fixes

* import license url change casue infrastructure change ([700ab95](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/700ab953e23dbdc78c2cf8f9ed1fb2a09737632e))

### [2.3.5](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.3.5..v2.3.4#diff) (2020-05-21)


### Features

* resource list support polling ([936922b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/936922bd1bf8c5f841959895677aa436d24a891a))


### Bug Fixes

* page guard support pending state ([849fa23](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/849fa237775e5fe1099c6445e48eb52b7cce2d3c))
* page-state component shouldn't show loading state when reload ([50910ba](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/50910bac946762b857fa3d65008743801dfe8a95))

### [2.3.4](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.3.4..v2.3.3#diff) (2020-05-11)


### Bug Fixes

* avoid lost resource change event ([b53756b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/b53756bf9d840954a81473cbccba52fe5fffdb6f))

### [2.3.3](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.3.3..v2.3.2#diff) (2020-05-11)


### Bug Fixes

* avoid lost data changes ([3b178c1](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/3b178c10f83dddccb7eca0bb1197783eae11ef04))

### [2.3.2](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.3.2..v2.3.1#diff) (2020-05-09)


### Bug Fixes

* disabled-container check disabledSubscription ([ebee5b9](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ebee5b9cb673b979daca359e51bd8ecb90e6c384))

### [2.3.1](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.3.1..v2.3.0#diff) (2020-05-08)


### Bug Fixes

* insert new item when resource list doesn't have more items ([be6513d](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/be6513dbe3b68fcf121f1d490e7026a9f0c2df12))

## [2.3.0](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.3.0..v2.2.25#diff) (2020-05-08)


### Features

* k8s resource list support polling ([1331b7f](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/1331b7f9006721320a6c31272f0f851cf045bcd2))
* support watch resource list ([a14a188](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/a14a188d79ae55ead7f6ea4d9d9b3dd23ac1bc29))


### Bug Fixes

* logined refresh page when has code and state redirect to login ([a8cfa97](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/a8cfa97d9624a4b3d3915439e7d3e7154b181c1b))

### [2.2.25](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.25..v2.2.24#diff) (2020-04-30)


### Bug Fixes

* normalize resource list while watching ([76be62d](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/76be62d25c7f0a0ce3842c8df8f0775c81eb7f36))

### [2.2.24](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.24..v2.2.23#diff) (2020-04-28)


### Bug Fixes

* page guard input allowed ([5dad532](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/5dad532cafb7409790e99bb5d882264f13d7874b))

### [2.2.23](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.23..v2.2.22#diff) (2020-04-27)


### Bug Fixes

* open underlord in self window ([0e72baf](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/0e72baf04317aa4d7f17ad04873fc6cc4be2ee77))
* time pipe should change with loclae ([d68336f](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d68336fad832036cfb83eb76024e9eddefc25f12))

### [2.2.22](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.22..v2.2.21#diff) (2020-04-21)


### Bug Fixes

* page guard style ([511c5f3](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/511c5f312bd2a83f1a07f90c455a480e17f89b3f))

### [2.2.21](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.21..v2.2.20#diff) (2020-04-20)


### Features

* page guard ([2d432bd](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/2d432bd28c0b3791db7f80725da4809dd44b305d))


### Bug Fixes

* remove template static  default false ([ccf1e78](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ccf1e78edff20e93c064210fb9ec8e1dd1e124bb))
* template static fakse ([b8f73d0](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/b8f73d003d59bde69e5cb57bfa6e0591a46a5a78))

### [2.2.20](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.20..v2.2.19#diff) (2020-04-20)


### Bug Fixes

* fix pipeline pause condition ([2ef5c16](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/2ef5c163960829fcd612feef72f6704984e06021))

### [2.2.19](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.19..v2.2.18#diff) (2020-04-20)


### Bug Fixes

* open platform center in self window ([556a2b5](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/556a2b540cbc3bd85dddf3a9863c3227c09da47f))

### [2.2.18](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.18..v2.2.17#diff) (2020-04-15)


### Features

* modigy image tag type DEVOPS-4188 ([bce0d22](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/bce0d225e34618f04b982c6fe9f8cf3e6e66c382))
* replace date-fns with dayjs ([56b1a12](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/56b1a12b589c4b4588a8bb154cfbdfbced6ca216))

### [2.2.17](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.17..v2.2.16#diff) (2020-04-14)


### Bug Fixes

* partial text is JSON stream ([c50d00d](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/c50d00d9287e3a1d76be7d2bc1cebeb7d23d6467))
* watch resource with list resourceVersion and chunk data ([840e356](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/840e3567322e7f28141d7451437a2663a93decc9))

### [2.2.16](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.16..v2.2.15#diff) (2020-04-08)


### Bug Fixes

* nav link error ([0668e7b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/0668e7b8711afa16c46bad07ce40941606f2d2a8))

### [2.2.15](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.15..v2.2.14#diff) (2020-04-02)


### Features

* add acl-platform-center-nav and acl-account-menu ([62bcf27](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/62bcf27e20ed85df3c4252b6ce8b4b93b131baf5))

### [2.2.14](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.14..v2.2.13#diff) (2020-04-01)


### Features

* feature guard component ([f637888](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/f6378883acbe7a4ecff0c58f5bc30b454470aa9b))

### [2.2.13](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.13..v2.2.12#diff) (2020-03-19)


### Features

* retry update resource ([b9fbfdd](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/b9fbfdd97177d56c183ef4520f5ffd0adfd77c4b))


### Bug Fixes

* empty token redirect to login page ([b128691](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/b1286913337e5384f361892b00ca81c6ab5a7bab))

### [2.2.12](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.12..v2.2.11#diff) (2020-03-18)


### Features

* update icon ([8696d30](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/8696d3006fdbf64fe10e36a37ad7c2b72ccab73a))

### [2.2.11](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.11..v2.2.10#diff) (2020-03-18)


### Bug Fixes

* add license error status-no-permission #ACP-1750 ([806c37f](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/806c37f27087d7f3594f78b98a6708b4b844ca57)), closes [#ACP-1750](http://jira.alaudate.cn/browse/ACP-1750)

### [2.2.10](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.10..v2.2.9#diff) (2020-03-17)

### [2.2.9](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.9..v2.2.8#diff) (2020-03-16)


### Features

* fast push items without gate when filter items by feature gate ([f48610f](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/f48610fa42cb3c2e641af59994881591db3bf3a8))


### Bug Fixes

* start with no feature gate setting items as default ([04d6d2b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/04d6d2bc15bbfcbb708458fea17e9356ae0a1b71))

### [2.2.8](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.8..v2.2.7#diff) (2020-03-16)


### Bug Fixes

* feature fate not run templateRef component ngOninit ([2c417bd](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/2c417bdd26af9b4e7dd568646047b1bc7dd6e5a1))

### [2.2.7](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.7..v2.2.6#diff) (2020-03-13)


### Features

* replace date-fns with dayjs ([82271c3](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/82271c3c08917faa7b0a13e8a21274f183fa4915))


### Bug Fixes

* add custom relative time plugin ([d2adce8](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d2adce80a53f82195eb60794f03b58259bfe0229))

### [2.2.6](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.6..v2.2.5#diff) (2020-03-13)


### Bug Fixes

* rxjs ajax response structure for translate ([1606e0b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/1606e0b4067a5bcf868c0a823ddaa23f0a6906ae))

### [2.2.5](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.5..v2.2.4#diff) (2020-03-13)


### Bug Fixes

* check token ([943a210](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/943a210c52c37325bddfc47e6124da58e11bd5e7))

### [2.2.4](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.4..v2.2.3#diff) (2020-03-12)


### Features

* refresh token ([22027fc](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/22027fcfd1677f734d83cebe3955ac5326300983))

### [2.2.3](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.3..v2.2.2#diff) (2020-03-10)


### Features

* add watchResource api ([e83e901](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/e83e9014504b45f2f3fd2094785c247e08975cd4))
* support delete resource by name directly ([45077c8](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/45077c864440331f47da6a10bbb258cf07d8910e))

### [2.2.2](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.2..v2.2.1#diff) (2020-03-09)


### Bug Fixes

* all console relative url bypass auth ([532cbf2](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/532cbf2f338e147a2317d5bfeb762cb2e4f48100))
* ApiGatewayService incompatible with Ivy ([9c5c6b3](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/9c5c6b3c3e1c13d83c6ad8b8195d5adbded8737d))

### [2.2.1](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.1..v2.2.0#diff) (2020-02-20)


### Bug Fixes

* license defer to auth and default error logic ([df00064](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/df00064d5272b75676d91aa31354592d4285dd26))

## [2.2.0](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.2.0..v2.1.0#diff) (2020-02-18)


### Features

* add basic icons to assets folder ([708a420](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/708a420ac8f9af17a72f8b68e194531e482c6920))
* async data support polling data ([cd2cab8](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/cd2cab8e352a16fa5e15b9b9987c86e977956f2b))


### Bug Fixes

* auth interceptor should lazy inject auth state service ([a80cb86](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/a80cb860ed01c0b2382dbbcf89c4ef284214caeb))

## [2.1.0](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.1.0..v2.0.0#diff) (2020-02-13)


### Features

* add license-manage #ACP-1596 ([62d18c4](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/62d18c4c65f96fbd8c081568e0390c96883b3f1c)), closes [#ACP-1596](http://jira.alaudate.cn/browse/ACP-1596)
* extract abstract DateTimePipe ([6277705](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/627770513e652bdc9ea5a8675799bb0a83448ab8))


### Bug Fixes

* import button url #ACP-1677 ([6d775f8](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/6d775f882dc153ab1eae2a1601944ed4efb8cd0d)), closes [#ACP-1677](http://jira.alaudate.cn/browse/ACP-1677)

## [2.0.0](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v2.0.0..v1.5.4#diff) (2020-02-10)


### ⚠ BREAKING CHANGES

* upgrade angular to 9.0

### Features

* upgrade angular to 9.0 ([3e77a91](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/3e77a910fa572e47b04132c5ea1255748b2cc60b))

### [1.5.4](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.5.4..v1.5.3#diff) (2020-02-07)


### Features

* allow user to customize patch strategy ([0ffb1ce](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/0ffb1ce8ddec7ece429eaf65a97a0c9edc09a4f6))


### Bug Fixes

* add secret permission DEVOPS-3461 ([56e216c](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/56e216c837166bed62e59e03bca7890ee9c62cdf))

### [1.5.3](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.5.3..v1.5.2#diff) (2020-02-05)


### Features

* supoort do not notify on error for writable methods ([d37ace3](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d37ace3f3fe9a2158f3baa123d638ab55eccdf7c))

### [1.5.2](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.5.2..v1.5.1#diff) (2020-02-04)


### Bug Fixes

* k8s resource list footer support custom resourceName for noDatahint ([5ac44a5](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/5ac44a583a28f0d62768cce81adc434b40b8658f))

### [1.5.1](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.5.1..v1.5.0#diff) (2020-01-07)


### Features

* add local develop common-lib document ([deb32e8](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/deb32e8c89ad88590edd8502d7e783eefe1591cd))


### Bug Fixes

* add space between name and display name ([96804c3](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/96804c36ff796197f8c1665feea0b28df0dd44fa))
* prevent undefined/null string union display name ([d053cc9](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d053cc9e0315e809338d60550e555decfe24e4bc))
* res err interceptor and translate translate multi-instance ([4999615](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/4999615c011316aaa8f74272000eebe8ed69aedc))

## [1.5.0](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.5.0..v1.4.22#diff) (2019-12-27)


### Features

* add feedback notification related modules - close [#12](http://jira.alaudate.cn/browse/12) ([de989b2](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/de989b2e16d883a8ac8a6228180ee3ce63d609b3))


### Bug Fixes

* namespace, project, product use translate ([e72a90e](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/e72a90eb6565941212c74876a21023a17d94c52f))

### [1.4.22](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.22..v1.4.21#diff) (2019-12-23)


### Features

* add ! label selector support, improve skipError operator ([0218d3b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/0218d3bf1476e1e3a19bc17d8cc43288dee8d3b3))


### Bug Fixes

* default bracket should be PARENTHESES ([2b0b2e6](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/2b0b2e615801ff0477fb03a532de4fc93b60edb4))

### [1.4.21](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.21..v1.4.20#diff) (2019-12-23)


### Bug Fixes

* image dialog filter not working ([40de52b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/40de52ba5c9e88149fb546ab1ea944110fa289d7))

### [1.4.20](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.20..v1.4.19#diff) (2019-12-20)


### Bug Fixes

* remove base href splicing when apigateway request ([b55cc25](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/b55cc25be38225b35abd08e804e8de063ba523e5))

### [1.4.19](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.19..v1.4.18#diff) (2019-12-19)


### Bug Fixes

* remote translate not via Interceptor ([e44387f](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/e44387febc6d57709df40b49aba78aea213395e5))

### [1.4.18](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.18..v1.4.17#diff) (2019-12-16)


### Bug Fixes

* diagram node style DEVOPS-3077 ([cc4380d](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/cc4380de99e405bc19a6cdf425507a19047ce192))
* diagram selected text style DEVOPS-3078 ([fb33a55](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/fb33a55e13b01aca1ab361870d5e056d24edba31))
* image option placeholder DEVOPS-2875 ([40886e9](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/40886e9af4ab57e9c7cec61c48ae2102b15eb9bb))
* image push dialog title DEVOPS-2856 ([721a26e](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/721a26ec8275f10ec9d790f126e3809eaed7603a))
* image select cursor DEVOPS-2837, image type DEVOPS-2627 ([55ebfff](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/55ebfff808a8e4ce31d000a31d41be758fb718c2))

### [1.4.17](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.17..v1.4.16#diff) (2019-12-11)


### Features

* add skipError and catchPromise operators ([ce7c18e](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ce7c18e429fb1cd836ca09da3fbae7734fa09ea5))


### Bug Fixes

* change enabled strategy, treat not existed gate as disabled ([1473b74](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/1473b7476bb8aa4b3f1bf7f17bbd66f86b3378fa))
* feature gate filterEnabled use wrong variable ([7c40f1d](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/7c40f1d0e17de99f3a4c06724f4b31bdfc5bfa3b))

### [1.4.16](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.16..v1.4.15#diff) (2019-12-09)


### Features

* add feature gate util service and directive ([7d94374](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/7d9437450d239650f08c2f57e64227e81b3069db))
* add K8sUtilService#getUnionDisplayName and related utils ([426b4cb](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/426b4cb8e7fde6aefd456ed66af1ac20ebfea79b))
* add refetch cache for feature gate manage ([6df78f6](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/6df78f66cad486fd8e62b0c8ac3c89d0ffe5f387))


### Bug Fixes

* add missing modules ([8237624](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/82376249ff21d11451480bc51173e8bdcfc52318))
* condition reverse ([2a66d5b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/2a66d5b8f7368e2db387cd18f2a1e97bc8c38947))
* directive prefix ([8e2dc95](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/8e2dc95acb87976b0abcec5ca0e0c9b0116d2d3f))
* feature gate enabled condition ([2ad5a95](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/2ad5a9503322bfefd9ba4ea4c81e843e40702603))

### [1.4.15](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.15..v1.4.14#diff) (2019-12-09)


### Bug Fixes

* add deletionTimestamp into ObjectMeta, support bool toCheck param ([d73377b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d73377b322cd0bb859e7730a7be3e9385ee22ea7))
* lint issues, use @alauda/custom-webpack for debugging ([45c6dbc](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/45c6dbced70e3a6be01bb83166098baceb5ed3b0))

### [1.4.14](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.14..v1.4.13#diff) (2019-12-03)

### [1.4.13](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.13..v1.4.12#diff) (2019-12-03)


### Bug Fixes

* bold ([33608db](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/33608dbfd9567ee4807f5d01bb2e535ea5b75bbd))
* list format ([a915df7](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/a915df70dea1b2db0d15114f504d4ba747e98818))

### [1.4.12](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.12..v1.4.11#diff) (2019-11-19)


### Bug Fixes

* remove / in  yamlApiVersion when api group is core ([bc23a7b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/bc23a7b))

### [1.4.11](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.11..v1.4.10#diff) (2019-11-11)


### Bug Fixes

* image button hover color DEVOPS-2814 ([0c863d6](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/0c863d6))
* image text DEVOPS-2670 ([be5bdfd](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/be5bdfd))
* pipeline diagram selected status DEVOPS-1934 ([78250a6](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/78250a6))

### [1.4.10](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.10..v1.4.9#diff) (2019-11-04)


### Bug Fixes

* highlight selected project and namespace. DEV-19415 ([b2af4d0](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/b2af4d0))

### [1.4.9](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.9..v1.4.8#diff) (2019-10-29)


### Features

* support k8sResourceDefinition for permission params type ([3602301](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/3602301))

### [1.4.8](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.8..v1.4.7#diff) (2019-10-29)


### Features

* add error-state redirect output ([c6a8165](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/c6a8165))

### [1.4.7](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.7..v1.4.6#diff) (2019-10-18)


### Bug Fixes

* disabled-container <a> style use property ([57d5185](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/57d5185))

### [1.4.6](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.6..v1.4.5#diff) (2019-10-18)


### Features

* disabled-container add support for <a> ([d8367a0](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d8367a0))

### [1.4.5](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.5..v1.4.4#diff) (2019-10-18)


### Bug Fixes

* diagram color ([1532355](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/1532355))
* image secret filterable DEVOPS-2461 ([0f2a8bc](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/0f2a8bc))
* image tag not allow comma DEVOPS-1663 ([f0c129e](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/f0c129e))

### [1.4.4](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.4..v1.4.3#diff) (2019-10-16)


### Bug Fixes

* remove Renderer2 for fix disabled-container on dropdown-button bug ([4a7444e](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/4a7444e))

### [1.4.3](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.3..v1.4.2#diff) (2019-09-29)


### Features

* add aclStandardTime pipe ([099c743](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/099c743))

### [1.4.2](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.2..v1.4.1#diff) (2019-09-29)


### Features

* add annotation key prefix ([d704606](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d704606))

### [1.4.1](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.1..v1.4.0#diff) (2019-09-27)


### Bug Fixes

* known issues on usage ([6015009](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/6015009))

## [1.4.0](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.4.0..v1.3.9#diff) (2019-09-27)


### Features

* add k8s util pipe and service, time pipe and service ([cfff2b3](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/cfff2b3))

### [1.3.9](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.3.9..v1.3.8#diff) (2019-09-24)


### Bug Fixes

* add adjacent disabled-container margin ([6758466](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/6758466))

### [1.3.8](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.3.8..v1.3.7#diff) (2019-09-24)


### Features

* add component acl-access ([8884563](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/8884563))
* change alaudatech.com => alauda.cn ([ab24fd6](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ab24fd6))
* page-state error page ([46b9ce2](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/46b9ce2))

### [1.3.7](https://bitbucket.org/mathildetech/alauda-common-snippet/compare/v1.3.6...v1.3.7) (2019-09-18)


### Bug Fixes

* angular json ([8754056](https://bitbucket.org/mathildetech/alauda-common-snippet/commit/8754056))
* gitignore ([1dff851](https://bitbucket.org/mathildetech/alauda-common-snippet/commit/1dff851))
* pipeline diagram style DEVOPS-2195 ([9da934c](https://bitbucket.org/mathildetech/alauda-common-snippet/commit/9da934c))
* push/pull modify style DEVOPS-1964 ([5c1c20f](https://bitbucket.org/mathildetech/alauda-common-snippet/commit/5c1c20f))


### Features

* add k8s util rfc, close [#3](https://bitbucket.org/mathildetech/alauda-common-snippet/issues/3) ([c47f1ce](https://bitbucket.org/mathildetech/alauda-common-snippet/commit/c47f1ce))



### [1.3.6](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.3.6..v1.3.5#diff) (2019-09-10)


### Bug Fixes

* page state error code ([232f9fa](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/232f9fa))

### [1.3.5](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.3.5..v1.3.4#diff) (2019-09-09)


### Bug Fixes

* get lastRemoteLoaded from translate service on pipe initialization ([ffd8dcf](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ffd8dcf))
* use ajax from rxjs directly to escape from http interceptor ([1f667ff](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/1f667ff))

### [1.3.4](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.3.4..v1.3.3#diff) (2019-09-06)


### Bug Fixes

* remove Object.freeze inside decorator which is broken on AOT ([9f1960d](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/9f1960d))
* translate options issue on AOT ([17e6984](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/17e6984))

### [1.3.3](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.3.3..v1.3.2#diff) (2019-09-06)


### Bug Fixes

* ReferenceError: Cannot access 'oPropertyValue' before initialization ([350d4cc](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/350d4cc))

### [1.3.2](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.3.2..v1.3.1#diff) (2019-09-06)


### Features

* add serve on production script ([a6a9d9c](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/a6a9d9c))
* enhance ObservableInput, get original value automatically ([0aa5a11](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/0aa5a11)), closes [/github.com/angular/angular/issues/18224#issuecomment-329939990](http://jira.alauda.cn/browse/issuecomment-329939990)

### [1.3.1](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.3.1..v1.3.0#diff) (2019-09-05)


### Bug Fixes

* translate module import error ([2c60fc5](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/2c60fc5))

## [1.3.0](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.3.0..v1.2.2#diff) (2019-09-05)


### Features

* async data loader ([bb585a9](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/bb585a9))
* k8s resource list ([680adfd](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/680adfd))
* navconfig loader ([bd72b66](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/bd72b66))

### [1.2.2](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.2.2..v1.2.1#diff) (2019-09-03)


### Features

* get core translations from existed projects ([34f2b4a](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/34f2b4a))

### [1.2.1](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.2.1..v1.1.8#diff) (2019-08-28)


### Bug Fixes

* add missing exports ([b42560e](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/b42560e))
* change translate TEMPLATE_OPTIONS ([1ef06b9](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/1ef06b9))
* do not checkout branch on beta ([23d1d38](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/23d1d38))
* tiny improvement for remoteLoaded checking ([e4f8e08](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/e4f8e08))


### Features

* add translate module, should just work ([ef3ce07](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ef3ce07))

### [1.1.6](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.1.6..v1.1.5#diff) (2019-08-08)


### Features

* add notify-on-error param for get/list apis ([a0f10dc](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/a0f10dc))

### [1.1.5](https://bitbucket.org/mathildetech/alauda-common-snippet/branches/compare/v1.1.5..v1.1.4#diff) (2019-08-01)


### Bug Fixes

* authorization use pre recorded init url instead location ([9a5646a](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/9a5646a))

### 1.1.4 (2019-07-25)


### Bug Fixes

* image component bugs fix ([2acb126](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/2acb126))



### 1.1.3 (2019-07-19)


### Bug Fixes

* fix: project should be allowed also ([f483685](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/f483685))



### 1.1.2 (2019-07-19)


### Bug Fixes

* fix: add support of advanced api for resource permission ([4cb4390](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/4cb4390))



### 1.1.1 (2019-07-19)


### Features

* modify image push dialog get image porject context ([bf27765](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/bf27765))



## 1.1.0 (2019-07-18)


### Bug Fixes

* add option placeholder ([309472e](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/309472e))
* add pipeline diagram status ([66d9d7e](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/66d9d7e))
* bugs and typo ([aebe388](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/aebe388))
* cache not enabled correctly ([a7543df](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/a7543df))
* change version ([745e237](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/745e237))
* change version ([7c28b7f](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/7c28b7f))
* color ([017f5cf](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/017f5cf))
* color ([95a4102](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/95a4102))
* correct image path ([45bdde9](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/45bdde9))
* display properties ([460c698](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/460c698))
* dropdown active styles ([259b3e5](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/259b3e5))
* empty forkJoin, styles ([75f8416](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/75f8416))
* ErebusResourceBaseParams definition issue ([be00f23](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/be00f23))
* example data ([91a996a](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/91a996a))
* form validation ([09fc276](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/09fc276))
* get namespaces error handling ([4787f72](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/4787f72))
* get resource definition from resource/definition/type ([47dde1f](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/47dde1f))
* get secret from selected repository ([e21790e](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/e21790e))
* ignore unauthorizated when token validated by console ([b472195](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/b472195))
* ignore unauthorized when cluster api failed with 401 ([fe6f5e9](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/fe6f5e9))
* image dialog selector styles ([eee039d](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/eee039d))
* image push layout ([6fb1c0c](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/6fb1c0c))
* incorrect API prefix ([714d733](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/714d733))
* isAllowed type definition issue ([b97c280](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/b97c280))
* isAllowed type definition issue again ([eb582fb](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/eb582fb))
* keep original auth headers ([9a85cde](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/9a85cde))
* keywords unexpected empty ([d76054d](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d76054d))
* missing GlobalResourceBaseParams ([616a3fd](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/616a3fd))
* no emit when selected not change ([ee8555d](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ee8555d))
* null exception ([bc20e21](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/bc20e21))
* only include common k8s types ([a8dfcff](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/a8dfcff))
* pipeline diagram selector ([1d0e574](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/1d0e574))
* project auto select first, has namespaces condition, styles ([df30daa](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/df30daa))
* publish stream ([fa10d27](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/fa10d27))
* readonly array should be acceptable ([e88ab93](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/e88ab93))
* remove redundant slash in selected image path ([7c00c2c](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/7c00c2c))
* remove unused import ([ce8c21a](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ce8c21a))
* reset image tag value when switch type ([e2e4d22](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/e2e4d22))
* resolve type from original definition/type ([9c442a3](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/9c442a3))
* scroll style, keep invalid selected (not component responsibility) ([30d071f](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/30d071f))
* should use BehaviorSubject instead of Subject ([0ab6730](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/0ab6730))
* should use UnionDisplayableInput ([67ea4ad](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/67ea4ad))
* story static resources ([121eaf9](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/121eaf9))
* styles ([ee55960](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ee55960))
* styles ([6d42cbb](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/6d42cbb))
* styles ([65e0bb9](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/65e0bb9))
* tag clean ([f345b58](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/f345b58))
* tag icon ([65e7991](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/65e7991))
* tag regex ([d91380f](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d91380f))
* types ddefinition incorrect for resource with action array ([589ea50](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/589ea50))
* typings ([01a191d](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/01a191d))
* typings ([b8b12bd](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/b8b12bd))
* typings ([7605054](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/7605054))
* typo ([28edca6](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/28edca6))
* use aui-icon for tag icon ([229f3c7](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/229f3c7))
* use realtime tags api for pull image control ([8a122bd](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/8a122bd))
* using typed product data for product-select ([71bd717](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/71bd717))
* version ([18165e2](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/18165e2))
* when image address with network port split incorrect ([31faa27](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/31faa27))


### Build System

* add debug useage ([ddac007](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ddac007))


### Features

* add admin related snippets ([80d3538](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/80d3538))
* add advanced API support ([af8d813](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/af8d813))
* add ALL_ACTIONS support ([3c10db7](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/3c10db7))
* add image dialog ([ea0373c](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/ea0373c))
* add K8sApiService, remove unused files, add publish beta script ([fc1e478](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/fc1e478))
* add more useful hhelpers ([481605f](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/481605f))
* add pipeline diagram ([d9b5ef0](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d9b5ef0))
* add Reactive and Async decorators ([dba5269](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/dba5269))
* add storybook ([a4bad49](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/a4bad49))
* add strict types ([f59f195](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/f59f195))
* dynamic forms component migration ([e0dd41b](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/e0dd41b))
* dynamic forms component migrattion ([121faf9](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/121faf9))
* exclude current product ([3752030](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/3752030))
* implement rcf VarDirective ([3e0ce2e](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/3e0ce2e))
* intl ([cdf5a0c](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/cdf5a0c))
* product select ([1cba619](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/1cba619))
* refactor, support more use cases ([d679c2a](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/d679c2a))
* rfc - add a var directive for temporary variable ([0edcf12](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/0edcf12))
* use union unionDisplayable by default ([65225db](https://bitbucket.org/mathildetech/alauda-common-snippet/commits/65225db))
